#!/usr/bin/env bash

# 2022, by Antonio Prates <hello@aprates.dev>
# see provided README.md for more information

function openBrowser() {
    echo 'Building book ...'
    sleep 12  # allow a few seconds for honkit build :P
    echo 'Opening browser ...'
    if which xdg-open > /dev/null
    then
        xdg-open 'http://localhost:4000/' &> /dev/null
    elif which gnome-open > /dev/null
    then
        gnome-open 'http://localhost:4000/' &> /dev/null
    fi
}

./update_map.fat

if hash yarn 2>/dev/null
then
    echo 'Starting HonKit via yarn ...'
    yarn
    openBrowser & yarn honkit serve
elif hash npm 2>/dev/null
then
    echo 'Starting HonKit via npm ...'
    npx honkit --version
    openBrowser & npx honkit serve
else
    echo 'yarn / npm not found'
fi
