# Contributing

We would be delighted to receive your input. If you wish to contribute, please start by opening an issue to discuss your proposed changes at the GitLab project: [lang - the FatScript docs](https://gitlab.com/fatscript/lang), before submitting a merge request.

If you are already contributing to [fry - the FatScript interpreter](https://gitlab.com/fatscript/fry), we encourage you to maintain this project as well, updating or completing the information here whenever necessary.

Spread the word: Help us build our community by sharing our project with others!

## Testing the Docs

To read these docs offline by serving them locally, ensure you have either [Yarn](https://yarnpkg.com) or [NPM](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) installed on your system. Then, run the following commands:

```
git clone https://gitlab.com/fatscript/lang.git
cd lang
./start.sh
```

## Contributors

Antonio Prates - hello@aprates.dev

(when submitting a merge request, please add your name to this list)
