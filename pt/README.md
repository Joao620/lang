![FatScript logo](../logo/fatscript-ascii-art.png)

## Olá Mundo

```
_ <- fat.std
console.log('Olá Mundo')
```

## Início Rápido

Vá diretamente para a documentação:

- [Visão geral](general/index.md)
- [Sintaxe da linguagem](syntax/index.md)
- [Bibliotecas padrão](libraries/index.md)

## Executando seu código

Você pode executar o FatScript usando o interpretador `fry` ou o playground na web.

### Interpretador Fry

Para execução local, utilize o interpretador `fry`. Para detalhes sobre sua instalação e uso, consulte a seção de [configuração](general/setup.md).

### Playground na Web (beta)

Para testes rápidos e convenientes, execute seu código diretamente no [FatScript Playground](https://fatscript.org/playground). O playground oferece um REPL e uma interface intuitiva que permite carregar scripts a partir de um arquivo.

## Download de PDF

- [FatScript v2.6.0 (atual)](../pdf/fatscript_v2_pt.pdf)
- [FatScript v1.3.5 (legado)](../pdf/fatscript_v1_pt.pdf)

## Tutoriais

Mergulhe em nossos tutoriais imersivos, insights por trás dos bastidores e tópicos relacionados no [canal do YouTube FatScript](https://www.youtube.com/@fatscript).

## Doações

Você achou o FatScript útil e gostaria de agradecer?

[Compre-me um café](https://www.buymeacoffee.com/aprates)

## Licença

[GPLv3](../LICENSE) © 2022-2024 Antonio Prates

[fatscript.org](https://fatscript.org)

---

Published on {{ honkit.time }}
