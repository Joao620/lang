{% if output.name != "ebook" %}

# Empacotamento

{% endif %}

O fry oferece uma ferramenta integrada de empacotamento para código FatScript.

## Utilização

Para agrupar seu projeto em um único arquivo a partir do ponto de entrada, execute:

```
fry -b sweet mySweetProject.fat
```

Este processo consolida todas as importações (exceto [caminhos literais](../syntax/imports.md#caminhos-literais)) e remove espaços desnecessários, melhorando os tempos de carregamento:

- Adiciona um [shebang](https://bash.cyberciti.biz/guide/Shebang) ao código empacotado
- Recebe o atributo de execução para o modo de arquivo

A seguir, você pode executar seu programa:

```
./sweet
```

> o empacotamento substituirá quaisquer instruções `$break` (ponto de interrupção do depurador) por `()`

## Ofuscação

Para uma ofuscação opcional, use `-o`:

```
fry -o sweet mySweetProject.fat  # cria o pacote ofuscado
./sweet                          # executa seu programa da mesma maneira
```

> Ao distribuir por meio de hosts públicos, considere [definir uma chave personalizada](../libraries/sdk.md#setkey) com um `.fryrc` local. Apenas o cliente deve ter acesso a esta chave para proteger o fonte.

A ofuscação usa o algoritmo [enigma](../libraries/enigma.md) para encriptação, garantindo uma decodificação rápida. Para um tempo de carregamento ótimo, prefira `-b` se a ofuscação não for essencial.

## Considerações

As importações são deduplicadas e incluídas com base na ordem de sua primeira aparição. Como resultado, a sequência em que você importa seus arquivos desempenha um papel crítico no resultado final agrupado. Embora essas considerações geralmente sejam inconsequentes para projetos pequenos, o empacotamento de projetos maiores pode exigir uma organização adicional. Sempre valide o seu código empacotado.
