{% if output.name != "ebook" %}

# Ferramentas

{% endif %}

Aqui estão algumas dicas que podem melhorar sua experiência de programação com FatScript.

## Análise estática

Use o modo de verificação para checar a sintaxe e receber dicas sobre o seu código:

```
fry -p mySweetProgram.fat
```

## Depurador

Um ponto de interrupção, indicado pelo comando `$break`, atua como uma ferramenta de depuração ao interromper temporariamente a execução do programa em um local designado e carregar o console de depuração integrado. Ele oferece um ambiente interativo para examinar o estado atual do programa, inspecionando valores no escopo, avaliando expressões e rastreando o fluxo do programa.

Para ativar os pontos de interrupção, é necessário executar o programa com modo interativo habilitado:

```
fry -i mySweetProgram.fat
```

No FatScript, `$break` retorna `null`, o que pode alterar um valor de retorno se colocado no final de um bloco, devido ao recurso de [auto-retorno](../syntax/types/method.md#auto-retorno). Tenha cuidado com o posicionamento de `$break` para evitar efeitos não intencionais na funcionalidade do programa.

## Formatação do código-fonte

### Suporte nativo

Você pode aplicar a indentação automática ao seu código fonte usando o seguinte comando:

```
fry -f mySweetProgram.fat
```

### Extensão do Visual Studio Code

Para adicionar suporte de formatação de código ao VS Code, você pode instalar a extensão [fatscript-formatter](https://marketplace.visualstudio.com/items?itemName=aprates.fatscript-formatter). Abra o Quick Open do VS Code (Ctrl+P), cole o seguinte comando e pressione enter:

```
ext install aprates.fatscript-formatter
```

> o `fry` precisa estar instalado em seu sistema para que essa extensão funcione

## Realce de sintaxe

### Extensão do Visual Studio Code

Para adicionar destaque de sintaxe do FatScript ao VS Code, você pode instalar a extensão [fatscript-syntax](https://marketplace.visualstudio.com/items?itemName=aprates.fatscript-syntax). Abra o Quick Open do VS Code (Ctrl+P), cole o seguinte comando e pressione enter:

```
ext install aprates.fatscript-syntax
```

Você também pode encontrar e instalar essas extensões no Marketplace de Extensões do VS Code.

### Plugin para Vim e Neovim

Para instalar o realce de sintaxe do FatScript para Vim e Neovim, confira o plugin [vim-syntax](https://gitlab.com/fatscript/vim-syntax).

Para usuários do Neovim, adicione a linha respectiva à sua configuração:

**Usando packer.nvim**:

```lua
use { 'https://gitlab.com/fatscript/vim-syntax', as = 'fatscript' }
```

**Usando lazy.nvim**:

```lua
{ 'https://gitlab.com/fatscript/vim-syntax', name = 'fatscript' }
```

### Arquivo de sintaxe do Nano

Para instalar o realce de sintaxe do FatScript no `nano`, siga estes passos:

1. Baixe o arquivo `fat.nanorc` [daqui](https://gitlab.com/fatscript/fry/-/raw/main/extras/fat.nanorc?inline=false).
2. Copie o arquivo `fat.nanorc` para o diretório de sistema do `nano`:

```bash
sudo cp fat.nanorc /usr/share/nano/
```

Se o realce de sintaxe não for habilitado automaticamente, talvez você precise habilitá-lo explicitamente em seu arquivo `.nanorc`. Consulte as instruções na [Wiki do Arch Linux](https://wiki.archlinux.org/title/Nano#Syntax_highlighting) para mais informações.

Após a instalação do destaque de sintaxe, você também pode usar o formatador de código no `nano` com a seguinte sequência de atalhos:

- Ctrl+T Executar; e em seguida...
- Ctrl+O Formatador

## Outras dicas

### Navegação de arquivos no console

Para navegar pelas pastas do seu projeto a partir do terminal, você pode experimentar usar um gerenciador de arquivos do console como o [ranger](https://ranger.github.io/), combinado com o `nano`, `vim` ou `nvim`. Defina-o como o editor padrão para o `ranger` adicionando a seguinte linha ao seu arquivo `~/.bashrc`:

```
export EDITOR="nano"
```
