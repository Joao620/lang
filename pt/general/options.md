{% if output.name != "ebook" %}

# Opções

{% endif %}

Com esta descrição dos modos e parâmetros disponíveis, você descobrirá que o `fry` tem várias especiarias guardadas na manga para você temperar a execução do seu código.

## Argumentos de linha de comando

A interface CLI oferece alguns modos de operação:

- `fry [OPÇÕES]` read-eval-print-loop (REPL)
- `fry [OPÇÕES] ARQUIVO [ARGS]` executar um arquivo FatScript
- `fry [OPÇÕES] -b/-o ENTRADA SAÍDA` criar um bundle
- `fry [OPÇÕES] -f ARQUIVO...` formatar arquivos de código-fonte do FatScript

Aqui estão os parâmetros de opção disponíveis:

- `-a, --ast` exibir apenas a árvore sintática abstrata
- `-b, --bundle` salvar bundle em arquivo de saída (implica em -p)
- `-c, --clock` registro de tempo e estatísticas (alternar)
- `-d, --debug` habilitar logs de debug (implica em -c)
- `-e, --error` continuar em caso de erro (alternar)
- `-f, --format` indentar arquivos de código-fonte do FatScript
- `-h, --help` exibir ajuda e sair
- `-i, --interactive` habilitar REPL com execução do arquivo
- `-j, --jail` restringir sistema de arquivos, rede e chamadas de sistema
- `-k, --stack #` definir a profundidade da pilha (contagem de frames)
- `-m, --meta` exibir informações sobre a build
- `-n, --nodes #` definir limite de memória (contagem de nós)
- `-o, --obfuscate` ofuscar o bundle (implica em -b)
- `-p, --probe` realizar análise estática (teste seco)
- `-s, --save` armazenar sessão do REPL em repl.fat
- `-v, --version` exibir número da versão e sair
- `-w, --warranty` exibir isenção de responsabilidade e sair
- `-z, --minify` minificar código-fonte (implica em -p)

> a opção `-e` é ativada automaticamente nos modos REPL e probe

> combinar `-p` com `-f` envia o resultado formatado para stdout

## Gerenciamento de memória

`fry` gerencia a memória automaticamente sem pré-reserva. Você pode limitar o uso da memória especificando o número de nós com as opções da CLI:

- `-n <count>` para uma contagem exata de nós
- `-n <count>k` para kilonós, contagem \* 1000
- `-n <count>m` para meganós, contagem \* 1000000

Por exemplo, `fry -n 5k meuPrograma.fat` restringe o aplicativo a 5000 nós.

O coletor de lixo (GC) é executado automaticamente quando restam 256 nós antes que o limite final de memória seja atingido (premonição do GC). Você também pode invocar o GC a qualquer momento chamando o método `runGC` da [biblioteca SDK](../libraries/sdk.md) desde a thread principal.

### Estimativa de bytes (x64)

Cada nó em uma plataforma de 64 bits usa aproximadamente ~200 bytes. O tamanho real do nó depende dos dados que ele contém. Por exemplo, o limite padrão é 10 milhões de nós, seu programa pode chegar a usar cerca de 2 GB de RAM ao atingir o limite padrão.

Use a opção `-c` ou `--clock` para imprimir as estatísticas de execução e ter uma melhor compreensão de como seu programa está se comportando na prática.

#### Verificação de tempo de execução

Existem dois [comandos embutidos](../libraries/embedded.md) para verificar o uso de memória em tempo de execução:

- `$nodesUsage` nós alocados no momento (O(1))
- `$bytesUsage` bytes alocados no momento (O(n))

> verificar os bytes alocados no momento é uma operação cara, pois precisa percorrer todos os nós para verificar o tamanho real de cada um

### Tamanho da pilha

A profundidade máxima da pilha é definida em `parameters.h`, no entanto, você pode personalizar o tamanho da pilha até certo ponto usando opções da linha de comando (CLI):

- `-k <count>` para um número exato de frames
- `-k <count>k` para kibiframes, count \* 1024

## Arquivo "run commands"

Na inicialização, `fry` procura um arquivo `.fryrc` no mesmo caminho do arquivo do programa e, se não encontrado, também no diretório de atual. Se encontrado, é executado como uma fase de "pré-cozimento" para configurar o ambiente para a execução do programa.

### Gerenciamento de memória com .fryrc

Você pode usar o arquivo `.fryrc` para definir o limite de memória para seu projeto sem precisar especificá-lo como argumento da CLI. Para fazer isso, você pode usar o método `setMem` fornecido pela [biblioteca SDK](../libraries/sdk.md), assim:

```
_ <- fat.system
setMem(64000)  # define 64k nós como limite de memória
```

### Detalhes de inicialização

As opções da linha de comando são aplicadas primeiro, exceto pelo limite de memória. Durante a fase de pré-cozimento, o `fry` usa o limite padrão de 10 milhões de nós, independentemente da opção da linha de comando. Se você definir um limite de memória no arquivo `.fryrc`, esse limite terá efeito a partir desse ponto e substituirá a opção da linha de comando para toda a execução. Se o arquivo `.fryrc` não definir um limite de memória, a opção da linha de comando terá efeito após a fase de pré-cozimento.

O escopo de pré-cozimento é invisível por padrão. Após a execução do arquivo `.fryrc`, um escopo zerado é fornecido para o seu programa, o que permite testar seu código com um limite muito baixo de nós ao usar um arquivo `.fryrc` sem afetar a contagem de nós. Isso também impede que o namespace `.fryrc` entre em conflito com o escopo global do seu programa. No entanto, se você quiser manter as entradas declaradas no `.fryrc` no escopo global para fins de configuração, pode chamar o comando embutido `$keepDotFry` em algum lugar do arquivo `.fryrc`.

Outro uso possível, além de configurar o limite de memória, é pré-carregar as importações comuns, por exemplo, os tipos padrão:

```
$keepDotFry
_ <- fat.type._
```

## Modo Sandbox

Use a opção `-j` ou `--jail` para inibir os seguintes comandos embutidos:

- `write`, `remove` e `mkDir` - Esses comandos modificam o sistema de arquivos.
- `request` - Este comando é usado para fazer requisições HTTP externas.
- `shell`, `capture`, `fork` e `kill` - Estes comandos estão envolvidos em iniciar ou parar processos arbitrários.

## Veja também

- [Comandos embutidos](../libraries/embedded.md)
- [Biblioteca SDK](../libraries/sdk.md)
