{% if output.name != "ebook" %}

# Visão Geral

{% endif %}

FatScript é uma linguagem de programação leve e interpretada projetada para criar aplicativos baseados em console. Ela enfatiza a simplicidade, facilidade de uso e conceitos de programação funcional.

## Livre e de código aberto

`fatscript/fry` é um projeto de código aberto que incentiva a colaboração e o compartilhamento de conhecimento. Nós convidamos os desenvolvedores a [contribuir](https://gitlab.com/fatscript/fry/blob/main/CONTRIBUTING.md) para o projeto e nos ajudar a melhorá-lo com o tempo.

## Conceitos chave

- Gerenciamento automático de memória por coleta de lixo (GC)
- Combinações simbólicas de caracteres para uma sintaxe minimalista
- REPL (Read-Eval-Print Loop) para testes rápidos de expressões
- Suporte para sistema de tipos, herança e subtipagem por meio de aliases
- Suporte para programação imutável e métodos passáveis ​(como valores)
- Manter se simples e intuitivo, sempre que possível

## Conteúdo desta seção

- [Configuração](setup.md): como instalar o interpretador de FatScript
- [Opções](options.md): como personalizar a execução
- [Empacotamento](bundling.md): como empacotar um aplicativo FatScript
- [Ferramentas](tooling.md): visão geral de algumas ferramentas e recursos extras

## Limitações e desafios

Embora o FatScript seja projetado para ser simples e intuitivo, ele ainda é uma linguagem relativamente nova e pode não ser adequado para todos os casos de uso. Por exemplo, pode ter desempenho inferior em comparação com linguagens de programação mais maduras ao lidar com cargas de trabalho complexas ou tarefas de computação de alto desempenho.
