{% if output.name != "ebook" %}

# Sintaxe

{% endif %}

Nas seguintes páginas, você encontrará informações sobre os aspectos centrais da escrita de código FatScript, utilizando tanto os recursos básicos da linguagem quanto os recursos avançados do sistema de tipos e bibliotecas padrão.

## Tópicos abordados

- [Formatação](formatting.md): como formatar corretamente o código FatScript

- [Importações](imports.md): como importar bibliotecas para o seu código

- [Entradas](entries.md): entendendo o conceito de entradas e escopos

- [Tipos](types/index.md): um guia para o sistema de tipos FatScript

- [Controle de fluxo](flow.md): controlando a execução do programa com condicionais

- [Loops](loops.md): utilizando intervalos, map-over e while loops
