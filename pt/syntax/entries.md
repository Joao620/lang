{% if output.name != "ebook" %}

# Entradas

{% endif %}

As entradas são pares chave-valor que existem no escopo onde são declaradas.

## Nomeação

Os nomes das entradas (chaves) **não** podem começar com uma letra maiúscula, que é a distinção com relação aos [tipos](types/index.md). Os identificadores são sensíveis a maiúsculas e minúsculas, portanto "batatasfritas" e "batatasFritas" seriam considerados entradas diferentes.

A convenção recomendada é usar `camelCase` para as entradas.

> você pode usar um nome arbitrário como chave usando [nomeação dinâmica](#entradas-dinâmicas)

## Declaração e atribuição

Em FatScript, você pode declarar entradas simplesmente atribuindo um valor:

```
isOnline: Boolean = true
age: Number       = 25
name: Text        = 'João'
```

Os tipos também podem ser inferidos a partir da atribuição:

```
isOnline = true    # Boolean
age      = 25      # Number
name     = 'João'  # Text
```

### Entradas imutáveis

No FatScript, ao declarar uma entrada, ela é por padrão imutável, o que significa que, uma vez atribuído, seu valor não pode ser alterado. Essa imutabilidade garante consistência ao longo da execução do programa:

```
fruta = 'banana'
fruta = 'abacate'  # gera um erro porque 'fruta' é imutável
```

#### Exceção à Regra

A imutabilidade no FatScript aplica-se à vinculação da entrada, não ao conteúdo de escopos. Embora uma entrada seja imutável, se ela contiver um escopo, o conteúdo desse escopo pode ser modificado, seja pela adição de novas entradas, ou pela alteração de entradas mutáveis contidas no escopo:

```
s = { a = 1, b = 2 }
s.c = 3  # mesmo que 's' seja imutável, ele aceita o novo valor de 'c'
s        # agora é { a = 1, b = 2, c = 3 }
```

Essa escolha de design oferece flexibilidade com modificações de escopo. Em contraste, [listas](types/list.md) impõem uma imutabilidade mais estrita, impedindo a adição de novas entradas a listas imutáveis.

Note também que escopos são sempre passados por referência. Para modificar o conteúdo de um escopo sem alterar sua referência original, use o método `copy` da [Extensão de Protótipo de Escopo](../libraries/type/scope.md) para criar uma duplicata.

### Entradas mutáveis

Sim, você pode declarar entradas mutáveis, também conhecidas como variáveis. Para declarar uma entrada mutável, use o operador til `~`:

```
~ fruta = 'banana'
fruta   = 'abacate'  # ok
```

Observe que mesmo uma entrada mutável não pode mudar imediatamente seu tipo, a menos que seja apagada do escopo. Para apagar uma entrada, atribua `null` a ela e, em seguida, redeclare-a com um novo tipo. Mudar tipos é desencorajado pela sintaxe e não é recomendado, mas é possível:

```
~ color = 32    # cria a entrada color como um número mutável
color = 'blue'  # gera um TypeError porque color é um número
color = null    # a entrada é apagada
color = 'blue'  # redefine color com um tipo diferente (Texto)
```

> você deve declarar a entrada como mutável novamente usando o til `~` ao redefini-la após a exclusão se quiser que o próximo valor seja mutável

### Entradas dinâmicas

Você pode criar entradas com nomes dinâmicos usando colchetes `[ ref ]`:

```
ref = 'pipoca'  # texto será o nome da entrada

opcoes = { [ref] = 'é saborosa' }

opcoes.[ref]   # sintaxe dinâmica:    'é saborosa', com acesso de leitura e gravação
opcoes(ref)    # sintaxe de obtenção: 'é saborosa', mas o valor é somente leitura
opcoes.pipoca  # sintaxe de ponto:    'é saborosa', mas deve seguir a nomenclatura
```

> todas as declarações dinâmicas são entradas mutáveis

Essa funcionalidade permite definir dinamicamente os nomes dentro de um escopo e criar entradas com nomes que, de outra forma, não seriam aceitos pelo FatScript.

As entradas dinâmicas também podem usar referências numéricas, mas a referência é convertida em texto automaticamente, e.g.:

```
[ 5 ] = 'texto armazenado na entrada 5'
self.['5']  # devolve 'texto armazenado na entrada 5'
self.[5]    # devolve 'texto armazenado na entrada 5'
```

> em um contexto diferente, não seguido por uma atribuição `=` ou precedido por notação de ponto `.`, a sintaxe dinâmica será interpretada como uma declaração de [lista](types/list.md)

### Entradas especiais (DESCONTINUADO)

Entradas com nomes que começam com o sublinhado `_` são completamente livres e dinâmicas, não requerem til `~` e também podem mudar de tipo sem a necessidade de apagamento, como variáveis no JavaScript ou Python.

> Para permitir uma otimização mais agressiva no interpretador, as "entradas especiais" serão descontinuadas a partir da versão `3.x.x`. Em vez disso, serão tratadas como imutáveis, a menos que sejam declaradas com `~`, indicando que são mutáveis, mas não poderão mudar de tipo sem a necessidade de apagamento. Ou seja, não haverá um tratamento especial para entradas com nomes que começam com o sublinhado.

### Atribuição por desestruturação

Você pode copiar os valores de um escopo em outro escopo assim:

```
_ <- fat.math
distancia = (posicao: Scope/Number): Number -> {
  { x, y } = posicao     # atribuição por desestruturação no escopo do método
  sqrt(x ** 2 + y ** 2)  # calcula a distância entre a origem e (x, y)
}
distancia({ x = 3, y = 5 })  # 5.83095189485
```

A mesma sintaxe funciona de forma semelhante para listas:

```
distancia = (posicao: List/Number): Number -> {
  { x, y } = posicao  # extrai os dois primeiros itens para 'x' e 'y'
  sqrt(x ** 2 + y ** 2)
}
distancia([ 3, 5 ])  # 5.83095189485
```

Você também pode usar a atribuição por desestruturação para expor um determinado método ou propriedade de uma [importação nomeada](imports.md#importação-nomeada):

```
console <- fat.console
{ log } = console
log('Olá Mundo')
```

> usando essa sintaxe com importações, você pode escolher trazer para o escopo atual apenas os elementos da biblioteca que você está interessado em usar, evitando assim a poluição do namespace com nomes que não teriam uso ou poderiam entrar em conflito com os de sua própria autoria

### Sintaxe semelhante a JSON

O FatScript também suporta sintaxe semelhante a JSON para declarar entradas:

```
"nada": null,                  # entrada Void - comportamento distinto, veja abaixo
"estaOnline": true,            # entrada Boolean
"idade": 25,                   # entrada Number
"nome": "João",                # entrada Text
"tags": [ "a", "b" ],          # entrada List
"opcoes": { "prop": "outra" }  # entrada Scope
```

Embora possa parecer que [declarar "nada"](types/void.md) cria um valor "nada" de `null`, é importante observar que a "entrada resultante" na verdade não existe no escopo. Quando você tenta acessar esse "nada", o FatScript retorna `null`, mas se você tentar mapear o escopo, o nome dessa entrada estará faltando, pois nunca foi realmente criado.

É importante observar que as declarações semelhantes a JSON sempre criam entradas imutáveis, portanto você não pode adicionar o caractere til `~` para torná-las mutáveis.
