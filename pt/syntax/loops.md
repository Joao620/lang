{% if output.name != "ebook" %}

# Loops

{% endif %}

Repetir, repetir, repetir, repetir, repetir...

## Sintaxe base

Todos os loops são criados com o sinal de arroba `@`, por exemplo:

```
<expressão> @ <corpoDoLoop>
```

### Loop tipo "enquanto"

O corpo do loop irá executar enquanto a expressão avaliar para:

- verdadeiro
- número não zero
- texto não vazio

A execução irá terminar quando a expressão avaliar para:

- falso
- nulo
- número zero
- texto vazio
- erro

Por exemplo, este loop imprime números de 0 a 3:

```
_ <- fat.console

~ i = 0

(i < 4) @ {
  log(i)
  i += 1
}
```

## Sintaxe de mapeamento

Você pode mapear intervalos, listas e escopos com um mapeador, assim:

```
<intervalo|coleção> @ <mapeador>
```

Uma nova lista é gerada com base nos valores de retorno do mapeador.

### Mapeando um intervalo

Utilizando o operador de intervalo `..` o mapeador receberá um número como entrada sequencialmente do limite esquerdo até o limite direito:

```
4..0 @ num -> num + 1  # retorna [ 5, 4, 3, 2, 1 ]
```

> a sintaxe de intervalo é inclusiva em ambos os lados, por exemplo, 0..2 retorna 0, 1, 2.

Há também o operador de intervalo semiaberto `..<`, exclusivo no lado direito.

> ressalva: o intervalo semiaberto não funciona com direção inversa, sempre precisa ser do mínimo para máximo

### Mapeando uma lista

O mapeador receberá os itens em ordem (da esquerda para a direita):

```
[ 3, 1, 2 ] @ item -> item + 1  # retorna [ 4, 2, 3 ]
```

### Mapeando um escopo

O mapeador receberá os nomes (chaves) das entradas armazenadas no escopo em ordem alfabética:

```
{ c = 3, a = 1, b = 2 } @ chave -> chave  # retorna [ 'a', 'b', 'c' ]
```

> nos exemplos, usamos literais de lista e escopo, mas uma entrada ou chamada que avalia para uma lista ou um escopo terá o mesmo efeito

Você pode acessar as entradas de um escopo referindo-se a ele pelo nome, mas neste caso precisa que ele esteja definido no escopo externo, por exemplo:

```
meuEscopo = { c = 3, a = 1, b = 2 }
meuEscopo @ chave -> meuEscopo(chave)  # retorna [ 1, 2, 3 ]
```

O FatScript utiliza um recurso de caching inteligente que faz com que esta sintaxe não gere um esforço adicional para buscar o elemento da vez no escopo durante o mapeamento.
