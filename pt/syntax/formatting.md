{% if output.name != "ebook" %}

# Formatação

{% endif %}

No FatScript, espaços em branco e indentação são irrelevantes, porém são muito bem-vindos para tornar o código mais legível e fácil de entender.

## Espaços em branco

- Um caractere de nova linha (`\n`) indica o final de uma expressão, exceto quando:
  - o último token na linha é um operador
  - o primeiro token da próxima linha é um operador não-unário
  - usando parênteses para agrupar expressões
- Expressões podem estar na mesma linha se separadas por vírgula (`,`) ou ponto-e-vírgula (`;`)

## Comentários

Comentários começam com `#` e são terminados por uma nova linha:

```
a = 5  # este é um comentário
```

### Nota

FatScript não suporta comentários multi-linhas no momento. Além disso, literais de texto podem acabar como um valor de retorno válido se deixados como a última linha restante, devido à funcionalidade de [auto-retorno](types/method.md#auto-retorno). Portanto, é recomendável se ater ao formato de comentário de linha única.

## Veja também

- [Autoformatador de código fonte](../general/tooling.md)
