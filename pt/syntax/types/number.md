{% if output.name != "ebook" %}

# Number

{% endif %}

Um conceito matemático usado para contar, medir e fazer outras coisas de [matemáticas](../../libraries/math.md).

## Declaração

O tipo `Number` é implementado como `double`. Veja como declarar um número:

```
a = 5              # declaração de número (imutável)
b: Number = 5      # mesmo efeito, com verificação de tipo
c: Number = a      # iniciando com o valor da entrada, também 5
d = 43.14          # com casas decimais
```

Para declarar uma entrada mutável, coloque o operador til antes:

```
~ a = 6  # entrada de número mutável
a += 1   # adiciona 1 a 'a', resultando em 7
```

## Operações com números

Números aceitam várias operações:

- `==` igual
- `!=` diferente
- `+` soma
- `-` subtração
- `*` multiplicação
- `/` divisão
- `%` módulo
- `**` potência
- `<` menor
- `<=` menor ou igual
- `>` maior
- `>=` maior ou igual
- `&` AND lógico
- `|` OR lógico

### Ressalvas

Para operações lógicas e controle de fluxo, lembre-se de que zero é considerado falso e um não-zero é considerado verdadeiro.

Para operadores de igualdade, embora `0` e `null` sejam avaliados como falsos, no FatScript eles não são iguais:

```
0 == null  # falso
```

## Precisão

Embora a precisão aritmética de um `IEEE 754 double` seja maior, o `fry` utiliza truques de arredondamento para melhorar a legibilidade humana ao imprimir sequências longas de noves ou zeros decimais como texto. Além disso, ele usa um epsilon de `1.0e-06` para comparações de 'igualdade' entre números.

Em 99,999% dos casos de uso, essa abordagem fornece tanto comparações mais convenientes quanto números mais naturais:

```
# Epsilon de igualdade
x = 1.0e-06
x: Number = 0.000001

# Diferenças menores são tratadas como o "mesmo" número pela comparação
x == 0.0000015
Boolean: true  # a diferença de 0,0000005 é ignorada
```

Os números de ponto flutuante não são distribuídos uniformemente na reta numérica. Eles são densos em torno de 0 e, à medida que a magnitude aumenta, o 'delta' entre dois valores expressivos aumenta:

```
_____________________________________0_____________________________________
+infinity    |     |    |   |  |  | ||| |  |  |   |    |     |    -infinity
```

> o maior inteiro contíguo é 9.007.199.254.740.992 ou 2^53

Ainda é possível ter números muito maiores, em torno de 10^308, que é:

```
100000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000000000000000000
```

Tenha em mente que se você somar 1 a 10^308, não importa quantas vezes você o fizer, sempre resultará no mesmo valor! Você precisa adicionar pelo menos algo próximo a 10^293 em uma única operação para que seja considerado, pois os números precisam ser de ordens de magnitude semelhantes. Para lidar de maneira discreta com números que excedem 2^53 considere utilizar o tipo [HugeInt](hugeint.md).

Além disso, a palavra-chave `infinity` fornece uma representação clara e inequívoca de valores que se elevam aos reinos além dos maiores números expressáveis, aproximando-se da infinitude teórica.

## Veja também

- [Extensões do protótipo Number](../../libraries/type/number.md)
- [Biblioteca math](../../libraries/math.md)
