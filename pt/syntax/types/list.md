{% if output.name != "ebook" %}

# List

{% endif %}

Listas são coleções ordenadas de itens do mesmo tipo, acessados por índice.

## Definição

As listas são definidas com colchetes `[]`, como no exemplo a seguir:

```
lista: List/Text = [ 'maçã', 'pizza', 'pêra' ]
```

Listas não permitem a mistura de tipos. O tipo de uma lista é determinado pelo primeiro item adicionado a ela; consequentemente, as listas vazias não têm tipo.

As listas pulam posições vazias, então um item que avalia para `null` é ignorado:

```
a = 1
c = 3
[ a, b, c ]  # retorna: [ 1, 3 ] (b é ignorado)
```

## Acesso

### Itens individuais

Os itens da lista podem ser acessados individualmente com chamada de índice baseado em zero:

```
lista(0)  # 'maçã'
lista(2)  # 'pêra'
```

Valores negativos indexam a lista de trás para frente, começando em -1 como o último item:

```
lista(-1)  # 'pêra'
```

O acesso a itens que estão fora dos índices válidos gera um erro:

```
         0        1        2       > 2
Erro   [ 'maçã', 'pizza', 'pêra' ] Erro
 < -3       -3       -2      -1
```

### Seleções

Os índices de início e fim funcionam exatamente da mesma maneira que ao acessar itens individuais; então, os valores negativos contam a partir do último item e podem ser regressivos. No entanto, ao usar intervalos, nenhum erro é gerado ao acessar índices fora dos limites; em vez disso, uma lista vazia é retornada.

```
lista(0..0)   # [ 'maçã' ]
lista(4..8)   # []
lista(1..-1)  # [ 'pizza', 'pêra' ]
```

Um índice pode ser deixado em branco, e o início a partir do primeiro item ou o fim no último item é assumido:

```
lista(..1)   # [ 'maçã', 'pizza' ]
lista(1..)   # [ 'pizza', 'pêra' ]
```

#### Aviso de descontinuação

A opção de passar um segundo argumento (usando uma vírgula e não ponto-ponto) para realizar uma seleção está sendo descontinuada à partir da versão 3.x.x:

```
lista(0, 0)   # [ 'maçã' ]           # sintaxe descontinuada
lista(4, 8)   # []                   # sintaxe descontinuada
lista(1, -1)  # [ 'pizza', 'pêra' ]  # sintaxe descontinuada
```

## Listas aninhadas

Uma matriz pode ser usada e acessada da seguinte maneira:

```
matriz = [
  [ 1, 2, 3 ]
  [ 4, 5, 6 ]
]

matriz(1)(0)  # retorna 4 (1: segunda linha, em seguida, 0: primeiro índice)
```

> para simplificar, o exemplo usa uma matriz 2D, mas poderia ser n-dimensional

## Operações

- `==` igual
- `!=` diferente
- `+` adição (efeito de concatenação)
- `-` subtração (efeito de diferença)
- `&` AND lógico
- `|` OR lógico

> AND/OR lógicos avaliam listas vazias como `false`, caso contrário `true`

### Adição de lista (concatenação)

A operação de adição de listas permite combinar duas listas em uma nova lista:

```
x = [ 1, 2, 2, 3 ]
y = [ 3, 3, 4, 4 ]

x + y  # resultado: [ 1, 2, 2, 3, 3, 3, 4, 4 ]
```

Nesse caso, ao usar o operador de adição `+` para unir as listas `x` e `y`, os elementos de ambas as listas são combinados em uma única lista. A ordem dos elementos na lista resultante é determinada pela ordem em que as listas foram adicionadas.

> não há remoção de elementos duplicados durante a concatenação

### Concatenação rápida

Para melhor desempenho, você pode aproveitar o operador `+=`, por exemplo:

```
~ lista += [ valor ]  # mais rápido

# mesmo efeito que
~ lista = []
lista = lista + [ valor ]  # concatenação (mais lenta)
```

Outro detalhe do operador `+=`, que se aplica também a outros tipos, é a inicialização automática por omissão, onde caso a entrada ainda não tenha sido declarada anteriormente, atua como uma simples atribuição.

### Subtração de lista (diferença)

A operação de subtração de listas, permite remover os elementos do segundo operando que estão presentes no primeiro operando, resultando em uma lista contendo apenas valores únicos:

```
x = [ 1, 2, 2, 3 ]
y = [ 3, 3, 4, 4 ]

x - y  # resultado: [ 1, 2 ]
y - x  # resultado: [ 4 ]
```

Nesse caso, ao subtrairmos a lista `y` da lista `x`, os elementos com valor 3 são removidos, já que estão presentes em ambas as listas. O resultado é a lista `[1, 2]`. Da mesma forma, ao subtrairmos a lista `x` da lista `y`, o único elemento restante é o valor 4.

> apenas valores exatamente idênticos são removidos durante a subtração

## Veja também

- [Extensões do protótipo List](../../libraries/type/list.md)
- [Mapeando uma lista](../loops.md#mapeando-uma-lista)
