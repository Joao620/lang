{% if output.name != "ebook" %}

# Method

{% endif %}

Métodos são receitas que podem receber argumentos para "preencher as lacunas em branco".

## Definição

Um método é definido anonimamente com uma seta fina `->`, assim:

```
<parâmetros> -> <receita>
```

Os parâmetros podem ser omitidos se nenhum for necessário:

```
-> <receita>  # aridade zero
```

Para registrar um método no escopo, atribua-o a um identificador:

```
<identificador> = <parâmetros> -> <receita>
```

Parâmetros dentro do escopo de execução de um método são imutáveis, garantindo que as operações do método não alterem seu estado original. Para um comportamento mutável, considere passar um escopo ou utilizar um [tipo personalizado](index.md#tipos-personalizados) capaz de encapsular múltiplos valores e estados.

## Parâmetros opcionais

Enquanto assinaturas de métodos tipicamente requerem um número fixo de parâmetros obrigatórios, FatScript suporta parâmetros opcionais através de valores padrão:

```
greet = (mensagem: Texto, nome: Texto = 'Mundo') -> {
  "Olá, {nome}, {mensagem}"
}
```

Neste exemplo, o parâmetro `nome` é opcional, assumindo 'Mundo' como valor padrão se não for fornecido. Esta característica permite invocações de métodos mais flexíveis.

## Tratamento de argumentos

Chamadas de métodos em FatScript são projetadas para aceitar o fornecimento de mais argumentos do que o requerido; argumentos extras são simplesmente ignorados. Este comportamento faz parte do design da linguagem para aumentar a flexibilidade e desempenho.

## Auto-retorno

FatScript usa o auto-retorno, ou seja, o último valor é retornado automaticamente:

```
resposta: Method = (aGrandeQuestao) -> {
  # TODO: explicar a vida, o universo e tudo mais
  42
}

resposta("6 x 7 = ?")  # retorna: 42
```

## Chamadas automáticas

O FatScript introduz um recurso único que simplifica as chamadas de métodos quando não envolvem argumentos. Esse recurso é conhecido como "truque de chamada automática" e oferece várias vantagens principais:

- **Redução de Código Desnecessário**: Reduz a necessidade de parênteses, tornando o código mais limpo e conciso, para métodos sem parâmetros que agem como propriedades.

- **Computação Dinâmica**: Permite a computação dinâmica com saídas que podem mudar com base no estado interno do objeto ou no estado global.

- **Execução Diferida**: Habilita a execução diferida, útil em programação assíncrona e padrões de inicialização complexos.

### Uso básico

No FatScript, um método definido sem parâmetros é executado "automágicamente" quando referenciado:

```
foo = {
  bar = -> 'Olá!'
}

# Ambas as linhas abaixo produzem 'Olá!'
foo.bar()  # chamada explícita
foo.bar    # chamada automática
```

### Referenciando

Para referenciar um método sem acionar o recurso de chamada automática, você pode usar a sintaxe de obtenção:

```
foo('bar')  # gera uma referência para foo.bar, sem chamá-lo
```

O FatScript também oferece as palavras-chave `self` e `root` para referenciar métodos nos níveis local e global, respectivamente:

```
self('meuMetodoLocal')
root('meuMetodoGlobal')
```

### Optando por não usar chamadas automáticas

O operador til `~` permite que você contorne o recurso de chamada automática, proporcionando flexibilidade no manuseio de métodos:

```
# Ambas as linhas abaixo buscam a referência do método, sem chamá-lo
foo.~bar
~ meuMetodo
```

Ou você pode simplesmente encapsular a chamada do método em outro método (anônimo):

```
-> foo.bar
```

### Aviso: passando métodos como argumentos

Há uma exceção importante com relação a passagem de métodos como argumentos, especificamente quando se trata de um método local:

```
outro(bar)  # passa `bar` como uma referência, sem executá-lo
```

> no entanto, isso não se aplica com encadeamento: `outro(foo.bar)` passa o resultado de `bar`, e não a referência

Para passar o valor resultante do método local `bar`, uma chamada explícita tem que ser feita:

```
outro(bar())
```

> este comportamento pode parecer contra-intuitivo, mas é extremamente útil em diversos casos de uso, como, por exemplo, ao passar métodos para [reduce](../../libraries/type/list.md#reduzindo), para uma [tarefa assíncrona](../../libraries/async.md), para uma operação de [mapeamento](../loops.md#sintaxe-de-mapeamento) etc.

## Argumento implícito

Uma conveniência oferecida pelo FatScript é a possibilidade de fazer referência a um valor passado para o método sem precisar especificar um nome a ele explicitamente. Neste caso, o argumento implícito é então representado pelo sublinhado `_`.

Aqui está um exemplo que ilustra o uso do argumento implícito:

```
dobro = -> _ * 2
dobro(3)  # saída: 6
```

Você pode usar um argumento implícito sempre que precisar realizar uma operação simples em um único parâmetro sem atribuir um nome específico a ele, mas note que o método deve ter aridade zero para ativá-lo.

## Veja também

- [Extensões do protótipo Method](../../libraries/type/method.md)
