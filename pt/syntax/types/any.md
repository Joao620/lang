{% if output.name != "ebook" %}

# Any

{% endif %}

Um tipo virtual que engloba todos os tipos e nenhum tipo ao mesmo tempo.

## Tipo padrão

`Any` é o tipo inferido e o tipo de retorno quando nenhum tipo é explicitamente anotado em um método. Por exemplo:

```
identity = _ -> _
```

é equivalente a:

```
identity = (_: Any): Any -> _
```

Usar `Any`, seja implicitamente ou explicitamente, desabilita a verificação de tipos para um parâmetro. A anotação explícita pode ser útil em casos em que você deseja deixar claro que está dando flexibilidade ao tipo que é aceito.

Ser muito liberal com `Any` pode tornar seu código menos previsível e mais difícil de manter. É geralmente recomendado ser mais específico com as anotações de tipo sempre que possível:

```
# Exemplo de uso de Any que pode levar a problemas

console <- fat.console

doubleIt = (arg: Any): Void -> console.log(arg * 2)

doubleIt(2)    # imprime: '4'
doubleIt('a')  # gera: Error: unsupported expression > Text <multiply> Number
```

Este exemplo mostra que, embora a anotação de tipo `Any` permita flexibilidade no tipo do parâmetro, também pode resultar em comportamento inesperado se um argumento de um tipo inesperado for passado. Ao ser mais específico com a anotação de tipo, como `Number`, você pode tornar seu código mais previsível e autoexplicativo.

```
# Exemplo de uso de uma anotação de tipo específica para maior previsibilidade

console <- fat.console

doubleIt = (num: Number): Void -> console.log(num * 2)

doubleIt(2)    # imprime: '4'
doubleIt('a')  # gera: TypeError: type mismatch > num
```

Ao usar `Number` como a anotação de tipo, o método `doubleIt` agora é mais específico e só aceita argumentos do tipo `Number`.

## Comparação

A única operação possível com `Any` é a comparação com ele, mas note que `Any` aceita todos os valores indistintamente, então não há uso prático para isso:

```
null      == Any  # verdadeiro
true      == Any  # verdadeiro
12345     == Any  # verdadeiro
'abcd'    == Any  # verdadeiro
[ 1, 2 ]  == Any  # verdadeiro
{ a = 8 } == Any  # verdadeiro
```

> as comparações com `Any` não podem ser usadas para verificar a presença de um valor em um escopo, pois até mesmo `null` é aceito
