{% if output.name != "ebook" %}

# Void

{% endif %}

Quando você olha para o 'Vazio', apenas 'nulo' pode ser visto.

## Tem alguém aí fora?

Uma entrada é avaliada como `null` se não estiver definida no escopo atual.

Você pode comparar com `null` usando igualdade `==` ou desigualdade `!=`, como:

```
a == null  # verdadeiro, se 'a' não estiver definida
0 != null  # verdadeiro, porque 0 é um valor definido
```

Tenha em mente que você não pode declarar uma entrada sem valor no FatScript.

Embora você possa atribuir `null` a uma entrada, isso causa comportamentos diferentes, dependendo se a entrada já existe no escopo e se é mutável ou não:

- Se uma entrada ainda não foi declarada, atribuir `null` não tem efeito.
- Se já existe e é imutável, atribuir `null` gera um erro.
- Se já existe e é mutável, atribuir `null` remove a entrada.

## Declaração de exclusão

Atribuir `null` a uma entrada mutável é o mesmo que excluir essa entrada do escopo. Se excluído, nada é lembrado sobre essa entrada no escopo, nem mesmo seu tipo original.

```
~ m = 4   # entrada de número mutável
m = null  # exclui m do escopo
```

> "valores" null são sempre mutáveis, pois na verdade nada é armazenado sobre eles e, portanto, são o único tipo de "valor" que pode fazer a transição de um estado mutável para um estado imutável quando "reatribuído"

## Comparações

Você também pode usar `Void` para verificar o valor de uma entrada, como:

```
()    == Void  # verdadeiro
null  == Void  # verdadeiro
false == Void  # falso
0     == Void  # falso
''    == Void  # falso
[]    == Void  # falso
{}    == Void  # falso
```

Observe que `Void` só aceita `()` e `null`.

## Formas de vazio

Em FatScript, o conceito de "vazio" ou a ausência de um valor pode ser representado de duas maneiras: usando `null` ou parênteses vazios `()`. Eles são efetivamente idênticos, em termos de comportamento no código:

```
null  == null  # verdadeiro
()    == null  # verdadeiro
()    == ()    # verdadeiro
```

### Usando null

A palavra-chave `null` denota explicitamente a ausência de um valor. É comumente usada em cenários onde um parâmetro ou valor de retorno pode não apontar para nenhum valor.

```
method(null, otherParam)

var = null
```

Também pode ser usado para tornar um parâmetro opcional, permitindo que métodos sejam chamados com diferentes números de argumentos:

```
method = (mandatory: Text, optional: Text = null) -> {
  ...
}
```

> `null` pode ser usado explicitamente em qualquer contexto onde uma ausência de valor precisa ser representada

### Usando parênteses vazios

Quando usado no contexto de retornos de métodos, `()` pode significar que o método não retorna nenhum valor significativo.

```
fn = -> {
  doSomething

  ()
}
```

Aqui, `fn` executa alguma ação e então usa `()` para indicar a ausência de um valor de retorno significativo, retornando efetivamente void.

A diferença reside no estilo de código, então isso é apenas uma sugestão, não uma regra rígida.

> nas versões modernas do interpretador, parênteses vazios `()` são tratados como `null`, garantindo um comportamento consistente, mas, versões anteriores exigiam o uso explícito de `null` para denotar a ausência de um valor de retorno

## Veja também

- [Extensões do protótipo Void](../../libraries/type/void.md)
