{% if output.name != "ebook" %}

# Scope

{% endif %}

Um escopo é semelhante a um dicionário, onde chaves estão associadas a valores.

## Definição

Escopos são definidos usando chaves `{}`, conforme mostrado abaixo:

```
meuEscopoBacana = {
  lugar = 'aqui'
  quando = 'agora'
}
```

Escopos armazenam entradas em ordem alfabética, uma característica que se torna aparente ao [mapear sobre um escopo](../loops.md#mapeando-um-escopo).

## Acesso

Há três maneiras de acessar diretamente as entradas dentro de um escopo.

### Sintaxe de ponto

```
meuEscopoBacana.lugar  # retorna: 'aqui'
```

### Sintaxe de obtenção

```
# assumindo que prop = 'lugar'
meuEscopoBacana(prop)  # retorna: 'aqui'
```

Em ambos os métodos, se a propriedade não estiver presente, `null` é retornado. Se o escopo externo não for encontrado, um erro é gerado.

### Sintaxe de encadeamento opcional

Use o operador interrogação-ponto `?.` para encadear com segurança escopos externos potencialmente inexistentes:

```
naoExistente?.prop  # retorna null
```

A sintaxe de encadeamento opcional não gera um erro quando o escopo externo é `null`.

## Operações

- `==` igual
- `!=` diferente
- `+` adição (efeito de mesclagem)
- `-` subtração (efeito de diferença)
- `&` AND lógico
- `|` OR lógico

> AND/OR lógico avaliam escopos vazios como `false`, caso contrário, `true`

### Adição de escopo (mesclagem)

O segundo operando age como um patch para o primeiro:

```
x = { a = 1, b = 3 }
y = { b = 2 }

x + y  # resulta em { a = 1, b = 2 }
y + x  # resulta em { a = 1, b = 3 }
```

> valores do segundo operando substituem os do primeiro

### Subtração de escopo (diferença)

A subtração remove elementos do primeiro operando que são idênticos aos do segundo:

```
x = { a = 1, b = 3 }
y = { a = 1 }

x - y  # resulta em { b = 3 }
```

> apenas valores exatamente idênticos são removidos

## Blocos Escopados

Blocos Escopados em FatScript permitem a execução de declarações dentro do contexto de um escopo específico:

```
objeto.{
  # Declarações executadas no contexto de 'objeto'
}
```

Aqui, `objeto` é o escopo alvo. Dentro do bloco, você pode acessar e modificar diretamente as propriedades de `objeto`.

### Características

- **Isolamento**: entradas declaradas dentro de um Bloco Escopado são locais para aquele escopo e não afetam o escopo externo.
- **Acesso ao Escopo Externo**: Blocos Escopados podem acessar entradas do escopo externo.

#### Exemplo

```
x = {}

x.{
  a = 5      # 'a' agora é uma propriedade de 'x'
  b = a + 3  # 'b' agora também é uma propriedade de 'x'
}
```

## Interações de escopos

FatScript utiliza mecanismos sofisticados para gerenciar variáveis ​​em diferentes escopos, aproveitando conceitos de escopo léxico e sombreamento para fornecer capacidades de programação poderosas. Esta seção explora esses mecanismos, incluindo nuances de atribuição, comportamentos de incremento/decremento e o uso inovador do operador `+=` para alternância de booleanos.

### Atribuição

O operador de atribuição (=) copia valores de escopos externos para o escopo atual, definindo um novo valor:

```
~ n = 1
x = {}
x.{ ~ n = n }  # agora x.n == 1, e x.n é independente de root.n
x.{ c = n }    # tem efeito semelhante, porém 'c' é imutável
```

> o mesmo conceito se aplica ao código executado em um escopo de método

#### Ressalva

Usar `~ n = n + 1` dentro de um bloco ou método adiciona um novo 'n' no escopo atual, inicializado com o valor de `n + 1` do escopo envolvente mais próximo, sem alterar o 'n' externo.

### Incrementando e decrementando

Operações de incremento (+=) e decremento (-=) interagem com o escopo de variáveis de uma maneira diferente. Essas operações buscam a instância mais próxima de uma variável, começando do escopo atual e movendo-se para fora recursivamente, e então modificam essa instância diretamente.

```
~ outerN = 1
fn = -> {
  outerN += 1  # alveja e incrementa 'outerN' no escopo externo
}
```

### Auto-inicialização com +=

FatScript também fornece um comportamento especial em relação ao operador de incremento (+=). Se não existir o incremento funciona como uma atribuição regular como se você tivesse escrito o seguinte para `n += 1`:

```
n == Void ? n = 1 : n += 1
```

O recurso de auto-inicialização pode ser particularmente útil quando usado em combinação com [entradas dinâmicas](../entries.md#dynamic-entries) para programação dinâmica.

> este recurso está disponível exclusivamente para o operador de incremento, decremento não pode inicializar valores não existentes

### Alternância de booleanos com +=

Geralmente, booleanos não permitem operações de adição. FatScript, no entanto, estende a funcionalidade do operador `+=` para tipos booleanos, permitindo um mecanismo de alternância intuitivo dentro de escopos internos.

A expressão `flag += !flag` alterna efetivamente o valor booleano, mesmo quando `flag` é definido em um escopo externo.

> no caso particular de booleanos, a única distinção entre `=` e `+=` é o escopo

### Outros operadores de atribuição compostos

Da mesma forma, outras operações de atribuição compostas, como `*=`, `/=`, `%=` e `**=`, são suportados por tipos numéricos e respeitam as mesmas regras de escopo que se aplicam às operações de incremento e decremento.

## Veja também

- [Entradas dinâmicas](../entries.md#entradas-dinâmicas)
- [Extensões do protótipo Scope](../../libraries/type/scope.md)
- [Mapeando um escopo](../loops.md#mapeando-um-escopo)
