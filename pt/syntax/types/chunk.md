{% if output.name != "ebook" %}

# Chunk

{% endif %}

Chunks são apenas blocos binários de dados.

## Declaração

Chunks não podem ser declarados explicitamente; você deve usar o [construtor de tipo](../../libraries/type/chunk.md) e aplicar uma das seguintes estratégias:

```
_ <- fat.type.Chunk

Chunk(null)            # Void -> (chunk vazio)
Chunk(true)            # Boolean -> '\001'
Chunk(65)              # Number -> 'A'
Chunk('ABC')           # Text -> 'ABC'
Chunk([ 65, 66, 67 ])  # List/Number -> 'ABC'
```

> espera-se que os números sejam valores de byte válidos (0-255), caso contrário, um erro será gerado

## Manipulando Chunks

### Concatenação

No FatScript, você pode concatenar, ou juntar, dois chunks usando o operador `+`. Por exemplo:

```
abCombinados = chunkA + chunkB
```

### Seleção de blocos

A seleção permite acesso a partes específicas de um chunk usando índices. O FatScript suporta índices positivos e negativos. Índices positivos começam a partir do início do chunk (com `0` como o primeiro byte), enquanto índices negativos começam a partir do final (`-1` é o último byte).

> para uma explicação detalhada sobre o sistema de indexação no FatScript, consulte a seção sobre acesso e seleção de itens em [Lista](list.md)

Selecionar com um índice recupera um único byte do chunk (como número). Usando um intervalo de bytes, seleciona um fragment incluindo os índices de início e fim, exceto quando se usa o operador de intervalo semiaberto `..<`, que é exclusivo no lado direito.

Acessar índices fora do intervalo válido gerará um erro para seleções individuais. Para seleções de intervalo, índices fora dos limites resultam em um chunk vazio.

```
x3 = Chunk('example')
x3(1)     # 120 (valor ASCII de 'x')
x3(..2)   # novo Chunk contendo 3 bytes (correspondendo a 'exa')
```

### Comparações

São suportadas comparações de igualdade `==` e desigualdade `!=` de chunks.

## Veja também

- [Extensões do protótipo Chunk](../../libraries/type/chunk.md)
