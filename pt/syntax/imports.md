{% if output.name != "ebook" %}

# Importações

{% endif %}

Vamos desvendar a arte de importar arquivos e bibliotecas no FatScript! Por quê? Bem, porque nesta linguagem você pode importar sempre que seu coração desejar, simplesmente usando uma seta para a esquerda `<-`.

## Sintaxe de ponto

Para usar importações com sintaxe de ponto, os nomes dos arquivos e pastas do projeto não devem começar com dígito nem conter símbolos.

> você pode forçar qualquer caminho que desejar usando [caminhos literais](#caminhos-literais)

### Importação nomeada

Para importar arquivos, use a extensão `.fat` para nomes de arquivo (ou nenhuma extensão), mas omita a extensão na declaração de importação. Aqui está um exemplo:

```
ref <- nomeDoArquivo
```

> se ambos os arquivos `x` e `x.fat` existirem, o último terá precedência

Para importar arquivos de pastas:

```
ref1 <- pasta.nomeDoArquivo
ref2 <- pasta.subPasta.nomeDoArquivo
```

Para importar todos os arquivos de uma pasta, use a sintaxe de "ponto-sublinhado":

```
lib <- pasta._
```

Observe: apenas os arquivos imediatamente dentro da pasta são incluídos usando a sintaxe acima. Para incluir arquivos de subpastas, mencione-os explicitamente. Além disso, um arquivo "\_.fat" (ou arquivo "\_") dentro de uma pasta pode substituir o comportamento de importação de "ponto-sublinhado".

#### Acesso de elementos

Uma vez importado, acesse os elementos usando a sintaxe de ponto:

```
ref1.elemento1
```

#### Extração de elementos

Para extrair elementos específicos de uma importação nomeada ou para evitar adicionar o nome do módulo todas as vezes (por exemplo, `lib.foo`), use [atribuição por desestruturação](entries.md#atribuição-por-desestruturação):

```
{ foo, bar } = lib
```

#### Visibilidade

Importações nomeadas são resolvidas no escopo global, independentemente de onde forem declaradas. Isso significa que mesmo se você declarar uma importação nomeada dentro de uma função ou escopo local, ela será globalmente acessível.

### Importação local

Para importar no escopo atual, use:

```
_ <- nomeDoArquivo
```

Importações locais, ao contrário das nomeadas, despejam o conteúdo do arquivo diretamente no escopo atual. Assim, um método importado pode ser invocado como `baz(arg)` em vez de `ref.baz(arg)`.

Embora as importações locais sejam bem apropriadas para importar [tipos](types/index.md) no escopo global, elas devem ser usadas com cautela ao importar conteúdos de biblioteca. O uso excessivo de importações locais pode levar a poluição do namespace, tornando mais desafiador seguir o código, porque fica menos aparente de onde vêm os métodos.

#### Importação local seletiva

Você também pode descartar elementos de uma importação local usando atribuição por desestruturação:

```
{ foo } = { _ <- lib }
```

> o ponte é evitar a poluição do namespace, pois todo o conteúdo será processado

## Caminhos literais

Com caminhos literais, você pode usar qualquer nome de arquivo ou extensão. No entanto, observe que essas importações não são avaliadas durante o [empacotamento](../general/bundling.md), mas em tempo de execução. Aqui está um exemplo:

```
ref <- '_pasta/fonte-2.outro'
```

Você também pode usar [textos inteligentes](types/text.md#textos-inteligentes) como caminhos literais:

```
base = 'pasta'
arquivo = 'fonte.xyz'
ref <- '{base}/{arquivo}'
```

Como o FatScript também aceita [sintaxe semelhante a JSON](entries.md#sintaxe-semelhante-a-json), você pode até mesmo carregar um arquivo JSON diretamente como uma importação:

```
json <- 'sample/data.json'
```

> embora possível, é mais aconselhável usar [file.read](../libraries/file.md#métodos) e em seguida [recode.fromJSON](../libraries/recode.md#funções-json)

Lembre-se de que caminhos literais podem tornar seu código mais complexo e essas importações só podem ser resolvidas dinamicamente, então use-os com moderação.

## Política de importação

O FatScript utiliza uma estratégia de "importar apenas uma vez" com um mecanismo de flags no escopo, evitando automaticamente arquivos que já foram importados.

De maneira geral, importações são eficientes em termos de recursos. No entanto, **importações locais dentro de corpos de métodos** devem ser evitados, pois são re-avaliados a cada invocação, podendo causar retenção de memória.

Esse comportamento não é classificado como um bug per se, mas sim uma consequência das escolhas de design no sistema de coleta de lixo (GC) do FatScript. As otimizações do GC excluem nós diretamente derivados do código-fonte, permitindo que eles sejam evitados nos procedimentos padrões de marcação-e-varredura. Como resultado, importações locais dentro de métodos não tem a oportunidade de deduplicação, fazendo com que seus nós permaneçam residentes até o final do programa:

```
meuMetodo = -> {
  _ <- lib  # potencial vazamento de memória
  ...
}
```

Aqui estão algumas estratégias para resolver este problema:

- Mova a declaração de importação para o escopo externo.
- Prefira usar uma importação nomeada como alternativa.
- Reorganize a estrutura de 'lib' para exportar um método.
