{% if output.name != "ebook" %}

# Bibliotecas

{% endif %}

Vamos falar sobre os doces recheios embutidos no FatScript: as bibliotecas!

## Bibliotecas padrão

### Essenciais

Estas são as bibliotecas fundamentais que você espera que estejam disponíveis em uma linguagem de programação, fornecendo funcionalidades essenciais:

- [async](async.md) - Trabalhadores e tarefas assíncronas
- [color](color.md) - Códigos de cores ANSI para console
- [console](console.md) - Operações de entrada e saída do console
- [curses](curses.md) - Interface de usuário baseada em terminal
- [enigma](enigma.md) - Métodos de criptografia, hash e UUID
- [failure](failure.md) - Tratamento de erros e gerenciamento de exceções
- [file](file.md) - Operações de entrada e saída de arquivos
- [http](http.md) - Framework de manipulação HTTP
- [math](math.md) - Operações e funções matemáticas
- [recode](recode.md) - Conversão de dados entre vários formatos
- [sdk](sdk.md) - Utilitários do kit de desenvolvimento de software do fry
- [system](system.md) - Operações e informações no nível do sistema
- [time](time.md) - Manipulação de data e hora

### Pacote de tipos

[Este pacote](type/index.md) estende os recursos dos [tipos nativos](../syntax/types/index.md#tipos-nativos) do FatScript:

- [Void](type/void.md)
- [Boolean](type/boolean.md)
- [Number](type/number.md)
- [HugeInt](type/hugeint.md)
- [Text](type/text.md)
- [Method](type/method.md)
- [List](type/list.md)
- [Scope](type/scope.md)
- [Error](type/error.md)
- [Chunk](type/chunk.md)

### Pacote Extra

[Tipos adicionais](extra/index.md) implementados em FatScript puro:

- [Date](extra/date.md) - Gerenciamento de calendário e datas
- [Duration](duration.md) - Construtor de duração em milissegundos
- [HashMap](extra/hmap.md) - Armazenamento rápido de chave-valor
- [Logger](extra/logger.md) - Suporte ao registro de logs
- [Memo](extra/memo.md) - Utilitário de memoização genérica
- [Option](extra/option.md) - Encapsulamento de valor opcional
- [Param](extra/param.md) - Verificação de presença e tipo de parâmetro
- [Sound](extra/sound.md) - Interface de reprodução de som
- [Storable](extra/storable.md) - Armazenamento de dados

## Atalho de importação

Se você deseja torná-los todos disponíveis de uma vez, pode simplesmente fazer o seguinte, e todas essas coisas boas estarão disponíveis para o seu código:

```
_ <- fat._
```

Embora esse recurso possa ser conveniente ao experimentar no REPL, esteja ciente de que ele traz todas as constantes e nomes de método da biblioteca, potencialmente poluindo seu namespace global.

### fat.std

Alternativamente, importe a biblioteca "standard" que importa todos os tipos (inclusive do pacote extra), bem como aplica importações nomeadas de todos os demais pacotes, assim:

```
_ <- fat.std
```

O que equivale a:

```
_       <- fat.type._
_       <- fat.extra._
async   <- fat.async
color   <- fat.color
console <- fat.console
curses  <- fat.curses
enigma  <- fat.enigma
failure <- fat.failure
http    <- fat.http
file    <- fat.file
math    <- fat.math
recode  <- fat.recode
sdk     <- fat.sdk
system  <- fat.system
time    <- fat.time
```

Observe que, a importação de tudo antecipadamente pode adicionar uma sobrecarga desnecessária ao tempo de inicialização do seu programa, mesmo que você precise usar apenas alguns métodos.

Como boa prática, considere importar apenas os módulos específicos de que você precisa, com [importações nomeadas](../syntax/imports.md#importação-nomeada). Dessa forma, você pode manter seu código limpo e conciso, minimizando o risco de conflitos de nome ou problemas de desempenho.

## Hacking e mais

Sob o capô, as bibliotecas são construídas usando comandos embutidos. Para obter uma compreensão mais profunda e explorar o funcionamento interno do interpretador, mergulhe [neste tópico mais avançado](embedded.md).
