{% if output.name != "ebook" %}

# file

{% endif %}

Operações de entrada e saída de arquivo

## Importação

```
_ <- fat.file
```

## Contribuições de Tipo

| Nome     | Assinatura                   | Breve descrição      |
| -------- | ---------------------------- | -------------------- |
| FileInfo | (modTime: Epoch, size: Text) | Metadados do arquivo |

## Métodos

| Nome     | Assinatura                  | Breve descrição                            |
| -------- | --------------------------- | ------------------------------------------ |
| basePath | (): Text                    | Extrair caminho onde o app foi chamado     |
| exists   | (path: Text): Boolean       | Verificar se existe arquivo no caminho     |
| read     | (path: Text): Text          | Ler arquivo do caminho (modo de texto)     |
| readBin  | (path: Text): Chunk         | Ler arquivo do caminho (modo de binário)   |
| write    | (path: Text, src): Boolean  | Escrever src no arquivo e retornar sucesso |
| append   | (path: Text, src): Boolean  | Acrescentar ao arquivo e retornar sucesso  |
| remove   | (path: Text): Boolean       | Apagar o arquivo e retornar sucesso        |
| isDir    | (path: Text): Boolean       | Verificar se o caminho é um diretório      |
| mkDir    | (path: Text, safe: Boolean) | Criar um diretório a directory             |
| lsDir    | (path: Text): List          | Obter lista de arquivos em um diretório    |
| stat     | (path: Text): FileInfo      | Obter metadados do arquivo                 |

## Notas de uso

### read

Na exceção:

- registra o erro no `stderr`
- retorna `null`

> `read` não pode ver "arquivos" embutidos, mas `readLib` da [biblioteca SDK](sdk.md) pode

### write/append

Exceções:

- registra o erro no `stderr`
- retorna `false`

### mkDir

Se `safe` estiver definido como `true`, o diretório receberá permissão 0700 em vez do padrão 0755, o que é menos protegido.

## Veja também

- [Biblioteca recode](recode.md)
