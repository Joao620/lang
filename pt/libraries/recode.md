{% if output.name != "ebook" %}

# recode

{% endif %}

Conversão de dados entre vários formatos

## Importação

```
_ <- fat.recode
```

> [pacote type](type/index.md) é automaticamente importado com esta importação

## Constantes

- numeric, definição regex usado por `inferType` (DESCONTINUADA)

> a constante `numeric` é redundante e será removida na versão `3.x.x`

## Variáveis

Estas configurações podem ser ajustadas para configurar o comportamento das funções de processamento:

- csvSeparator, o padrão é `,` (vírgula)
- csvReplacement, o padrão é vazio (apenas remove vírgulas do texto)
- xmlWarnings, o padrão é `true` - defina como `false` para suprimir avisos XML (DESCONTINUADA)

## Funções Base64

| Nome       | Assinatura          | Breve descrição                               |
| ---------- | ------------------- | --------------------------------------------- |
| toBase64   | (data: Chunk): Text | Codifica bloco binário para texto base64      |
| fromBase64 | (b64: Text): Chunk  | Decodifica texto base64 para formato original |

## Funções JSON

| Nome     | Assinatura        | Breve descrição                         |
| -------- | ----------------- | --------------------------------------- |
| toJSON   | (\_: Any): Text   | Codifica JSON a partir de tipos nativos |
| fromJSON | (json: Text): Any | Decodifica JSON para tipos nativos      |

## Funções URL

| Nome         | Assinatura          | Breve descrição                                     |
| ------------ | ------------------- | --------------------------------------------------- |
| toURL        | (text: Text): Text  | Codifica texto para texto escapado URL              |
| fromURL      | (url: Text): Text   | Decodifica texto escapado URL para formato original |
| toFormData   | (data: Scope): Text | Codifica dados de formulário URL a partir de escopo |
| fromFormData | (data: Text): Scope | Decodifica dados de formulário URL para escopo      |

## Funções CSV

| Nome    | Assinatura                                  | Breve descrição                 |
| ------- | ------------------------------------------- | ------------------------------- |
| toCSV   | (header: List/Text, rows: List/Scope): Text | Codifica CSV a partir de linhas |
| fromCSV | (csv: Text): List/Scope                     | Decodifica CSV para linhas      |

> `csvReplacement` é usado por `toCSV` como substituição em caso de um `csvSeparator` ser encontrado dentro de um texto sendo codificado

## Funções XML (DESCONTINUADAS)

Atributos XML e tags auto-fechadas não são suportados.

| Nome    | Assinatura        | Breve descrição                        |
| ------- | ----------------- | -------------------------------------- |
| toXML   | (node: Any): Text | Codifica XML a partir de tipos nativos |
| fromXML | (text: Text): Any | Decodifica XML para tipos nativos      |

> As funções XML serão removidas das bibliotecas padrão FatScript na versão `3.x.x`, use [XMLoaf](https://gitlab.com/aprates/xmloaf)

## Funções RLE

| Nome    | Assinatura            | Breve descrição            |
| ------- | --------------------- | -------------------------- |
| toRLE   | (chunk: Chunk): Chunk | Comprime para esquema RLE  |
| fromRLE | (chunk: Chunk): Chunk | Descomprime de esquema RLE |

## Outras funções

| Nome      | Assinatura        | Breve descrição                       |
| --------- | ----------------- | ------------------------------------- |
| inferType | (val: Text): Any  | Converte texto em void/boolean/number |
| minify    | (src: Text): Text | Minifica código fonte FatScript       |

> `minify` substituirá quaisquer instruções `$break` (ponto de interrupção do depurador) por `()`

## Uso

### JSON

Uma vez que FatScript aceita alternativamente [sintaxe semelhante a JSON](../syntax/entries.md#sintaxe-semelhante-a-json), `fromJSON` usa internamente o parser do FatScript, que é extremamente rápido, mas pode ou não produzir exatamente o que se espera de um analisador JSON.

Por exemplo, uma vez que o fragmento abaixo é analisado, já que `null` no FatScript é ausência de valor, não haverá declarações de entrada para "prop":

```
"prop": null
```

Portanto, ler com `fromJSON` e escrever de volta com `toJSON` não é necessariamente uma operação idempotente.

## Veja também

- [Pacote type](type/index.md)
- [Biblioteca SDK](sdk.md)
