{% if output.name != "ebook" %}

# color

{% endif %}

Códigos de cores ANSI para console

## Importação

```
_ <- fat.color
```

## Constantes

- black, 0
- red, 1
- green, 2
- yellow, 3
- blue, 4
- magenta, 5
- cyan, 6
- white, 7
- bright.black, 8
- bright.red, 9
- bright.green, 10
- bright.yellow, 11
- bright.blue, 12
- bright.magenta, 13
- bright.cyan, 14
- bright.white, 15

## Métodos

| Nome        | Assinatura                              | Breve descrição                 |
| ----------- | --------------------------------------- | ------------------------------- |
| detectDepth | (): Number                              | Obter suporte de cor do console |
| to8         | (xr: Any, g: Number = ø, b: Number = ø) | Converter RGB para 8-cores      |
| to16        | (xr: Any, g: Number = ø, b: Number = ø) | Converter RGB para 16-cores     |
| to256       | (xr: Any, g: Number = ø, b: Number = ø) | Converter RGB para 256-cores    |

## Notas de Uso

### to8, to16 e to256

O parâmetro `xr` pode ser um texto opcional que representa a cor no formato HTML. Por exemplo, pode ser fornecido como 'fae830' ou '#fae830' (amarelo):

```
color   <- fat.console
console <- fat.console

console.log('hey', color.to16('fae830'))
console.log('hey', color.to256('fae830'))
```

No entanto, se `xr` for um número entre 0 e 255 representando `r`, então os parâmetros `g` e `b` serão necessários:

```
console.log('hey', color.to256(250, 232, 48))  // mesmo resultado
```

> estes métodos podem produzir aproximações da cor original nas profundidades 8, 16 ou 256 e não a côr verdadeira exata

## Veja também

- [Biblioteca console](console.md)
- [Biblioteca curses](curses.md)
- [256 Cores](https://www.ditig.com/256-colors-cheat-sheet)
