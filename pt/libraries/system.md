{% if output.name != "ebook" %}

# system

{% endif %}

Operações e informações em nível de sistema

## Importação

```
_ <- fat.system
```

## Tipos

| Nome          | Assinatura                  | Breve descrição            |
| ------------- | --------------------------- | -------------------------- |
| CommandResult | (code: ExitCode, out: Text) | Tipo de retorno de capture |

## Constantes

- successCode, 0: ExitCode
- failureCode, 1: ExitCode

## Métodos

| Nome      | Assinatura                       | Breve descrição                                  |
| --------- | -------------------------------- | ------------------------------------------------ |
| args      | (): List/Text                    | Retorna lista de argumentos passados pelo shell  |
| exit      | (code: Number): \*               | Sair do programa com o código de saída fornecido |
| getEnv    | (var: Text): Text                | Obter o valor da variável env por nome           |
| shell     | (cmd: Text): ExitCode            | Executa cmd no shell, retorna o código de saída  |
| capture   | (cmd: Text): CommandResult       | Captura a saída da execução de cmd               |
| fork      | (args: List/Text, out: Text = ø) | Inicia um processo em segundo plano, retorna PID |
| kill      | (pid: Number): Void              | Envia SIGTERM para o processo pelo PID           |
| getLocale | (): Text                         | Obter configuração de localidade atual           |
| setLocale | (cmd: Text): Number              | Definir configuração de localidade atual         |
| getMacId  | (): Text                         | Obter identificador da máquina (endereço MAC)    |
| blockSig  | (enabled: Boolean): Void         | Bloqueia SIGINT, SIGHUP e SIGTERM                |

## Notas de uso

### Atenção!

É importante agir com cautela e responsabilidade ao utilizar os métodos `getEnv`, `shell`, `capture`, `fork` e `kill`. A biblioteca `system` oferece a capacidade de executar comandos diretamente do sistema operacional, o que pode introduzir riscos de segurança se não forem utilizadas com cuidado.

Para mitigar vulnerabilidades, evite utilizar a entrada do usuário diretamente na construção de comandos passados para esses métodos. A entrada do usuário deve ser validada para prevenir ataques de injeção de comandos e outras violações de segurança.

### Outras Limitações (multithreading)

Embora os métodos desta biblioteca suportem uma variedade de tarefas de programação, eles não são otimizados para uso intercalado dentro de [Workers](async.md) assíncronos. Ao iniciar processos de dentro de threads, opte pelos métodos `shell/capture`, ou use exclusivamente `fork/kill`. Misturar esses dois grupos de métodos em aplicações multithread pode resultar em comportamentos imprevisíveis.

> em cada chamada, `shell/capture` definirá `SIGCHLD` para seu comportamento padrão, enquanto `fork` ignorará esse sinal para tentar evitar processos zumbis

### get/set locale

O interpretador `fry` tentará inicializar o locale `LC_ALL` para `C.UTF-8` e, se esse locale não estiver disponível no sistema, tentará usar `en_US.UTF-8`, caso contrário, usará o locale padrão.

Veja mais sobre [nomes de localidade](https://www.gnu.org/software/libc/manual/html_node/Locale-Names.html).

> a configuração de localidade aplica-se apenas ao aplicativo e não é mantida após a saída do `fry`
