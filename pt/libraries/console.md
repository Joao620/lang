{% if output.name != "ebook" %}

# console

{% endif %}

Operações de entrada e saída do console

## Importação

```
_ <- fat.console
```

## Métodos

| Nome         | Assinatura                                       | Breve descrição                              |
| ------------ | ------------------------------------------------ | -------------------------------------------- |
| log          | (msg: Any, fg: Number = ø, bg: Number = ø): Void | Imprime msg para stdout, com quebra de linha |
| print        | (msg: Any, fg: Number = ø, bg: Number = ø): Void | Imprime msg para stdout, sem quebra de linha |
| stderr       | (msg: Any, fg: Number = ø, bg: Number = ø): Void | Imprime msg para stderr, com quebra de linha |
| input        | (msg: Any, mode: Text = ø): Text                 | Imprime msg e retornar entrada de stdin      |
| flush        | (): Void                                         | Esvazia buffer de saída padrão               |
| cls          | (): Void                                         | Limpa stdout usando códigos de escape ANSI   |
| moveTo       | (x: Number, y: Number): Void                     | Move o cursor usando códigos de escape ANSI  |
| isTTY        | (): Boolean                                      | Verifica se stdout é um terminal             |
| isTty        | (): Boolean                                      | DESCONTINUADA (será removida em `3.x.x`)     |
| showProgress | (label: Text, fraction: Number): Void            | Renderiza barra de progresso, fração 0 a 1   |

> os métodos `log`, `stderr` e `input` garantem segurança de threads em cenários assíncronos

## Notas de uso

### saída

Por padrão, `stdout` e `stderr` imprimem no console. Os parâmetros de cor de primeiro plano (fg) e cor de plano de fundo (bg) são opcionais.

> as cores são automaticamente suprimidas se o buffer de saída não for um TTY

### input

O parâmetro opcional `mode` aceita os seguintes valores:

- 'plain', entrada simples (sem cursor readline, sem histórico)
- 'quiet', como modo plain, porém sem feedback
- 'secret', modo especial para leitura de senha
- `null` (padrão), com readline e histórico de entrada

## Veja também

- [Biblioteca color](color.md)
- [Biblioteca curses](curses.md)
