{% if output.name != "ebook" %}

# Comandos embutidos

{% endif %}

Comandos embutidos são funções de baixo nível do FatScript que podem ser invocadas com palavras-chave precedidas por um cifrão `$`. Esses comandos estão sempre disponíveis, implementados como código compilado e não requerem importações.

Ao contrário dos métodos, eles não recebem argumentos explícitos, mas podem ler a partir de nomes de entrada específicos no escopo atual ou até mesmo do estado interno do interpretador.

## Os mais úteis

Aqui estão alguns comandos embutidos que podem valer a pena conhecer:

- `$break` pausa a execução e carrega o console de depuração
- `$debug` alterna os logs de depuração do interpretador
- `$exit` encerra o programa com o código fornecido
- `$keepDotFry` mantém a config (.fryrc) no escopo após a inicialização
- `$result` alterna a impressão do resultado no final da execução
- `$root` fornece uma referência ao escopo global
- `$self` fornece uma referência própria ao escopo do método/instância
- `$bytesUsage` retorna o total de bytes alocados no momento
- `$nodesUsage` retorna o total de nós alocados no momento
- `$isMain` verifica se o código está sendo executado como principal ou módulo

> as palavras-chave `root` e `self` são automaticamente convertidas em `$root` e `$self`

Você pode chamá-los diretamente no seu código, assim:

```
$exit  # encerra o programa
```

> para usar outros comandos embutidos você deve estudar a implementação C de `fry`, já que a lista completa não está documentada, consulte o arquivo [embedded.c](https://gitlab.com/fatscript/fry/blob/main/src/sdk/embedded.c)

## Bibliotecas por trás dos bastidores

As bibliotecas padrão embalam chamadas embutidas como métodos, fornecendo uma interface mais ergonômica. Você não precisa criar um escopo de execução ou carregar argumentos nesse escopo antes de delegar a execução a eles.

Por exemplo, veja como você pode usar o método `floor` da [biblioteca math](math.md):

```
_ <- fat.math
floor(2.53)
```

Este método é implementado como:

```
floor = (x: Number): Number -> $floor
```

Por trás dos bastidores, o método `floor` cria um escopo de execução e carrega um argumento como `x` nele. O método então delega a execução ao comando embutido `$floor`, que por sua vez, lê o valor de `x` do escopo atual e retorna o menor inteiro que não é maior que este número.

Você pode obter o mesmo resultado que o método acima fazendo o seguinte:

```
x = 2.53
$floor  # lê o valor de x do escopo atual
```

## Hackeando

Você pode ver qual comando embutido um método de biblioteca está chamando, olhando para a implementação da biblioteca através do método `readLib` da [biblioteca SDK](sdk.md). Tecnicamente, não há nada que o impeça de chamar comandos embutidos diretamente.

Por exemplo, você pode encerrar seu programa chamando `$exit` diretamente, que sairá com o código 0 (padrão) ou, se uma entrada numérica chamada `code` existir no escopo atual, o valor dessa entrada será usado como código de saída. No entanto, seria mais elegante importar a biblioteca `fat.system` e chamar o método `exit` com o código de saída desejado:

```
sys <- fat.system
sys.exit(0)  # exits with code 0
```

Essa abordagem torna seu código mais legível e menos propenso a erros, além de fornecer uma melhor separação de responsabilidades.

É importante ter em mente que os comandos embutidos são caixas pretas e não são destinados à escrita de código FatScript comum. Na maioria dos casos, você precisaria ler a [implementação em C subjacente](https://gitlab.com/fatscript/fry/tree/main/src/libs) para entender melhor o que um comando está realmente fazendo.

Embora seja possível usar comandos embutidos para obter desempenho adicional em tempo de execução, evitando importações e chamadas de método, isso não é recomendado devido à perda de legibilidade do código. Em geral, é melhor usar as bibliotecas padrão e seguir as melhores práticas para escrever um código claro, fácil de manter.
