{% if output.name != "ebook" %}

# Void

{% endif %}

Extensões do protótipo Void

## Importação

```
_ <- fat.type.Void
```

## Construtor

| Nome | Assinatura | Breve descrição                         |
| ---- | ---------- | --------------------------------------- |
| Void | (val: Any) | Retorna nulo, apenas ignora o argumento |

## Membros do protótipo

| Nome     | Assinatura  | Breve descrição            |
| -------- | ----------- | -------------------------- |
| isEmpty  | (): Boolean | Retorna verdadeiro, sempre |
| nonEmpty | (): Boolean | Retorno falso, sempre      |
| size     | (): Number  | Retorna 0, sempre          |
| toText   | (): Text    | Retorna 'null' como texto  |

### Exemplo

```
_ <- fat.type.Void
x.isEmpty  # true, já que x não foi declarado
```

## Veja também

- [Void (sintaxe)](../../syntax/types/void.md)
- [Pacote type](index.md)
