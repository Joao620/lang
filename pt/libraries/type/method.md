{% if output.name != "ebook" %}

# Method

{% endif %}

Extensões do protótipo Method

## Importação

```
_ <- fat.type.Method
```

## Construtor

| Nome   | Assinatura | Breve descrição          |
| ------ | ---------- | ------------------------ |
| Method | (val: Any) | Envolve val em um método |

## Membros do protótipo

| Nome     | Assinatura  | Breve descrição                     |
| -------- | ----------- | ----------------------------------- |
| isEmpty  | (): Boolean | Retorna falso, sempre               |
| nonEmpty | (): Boolean | Retorna verdadeiro, sempre          |
| size     | (): Number  | Retorna 1, sempre                   |
| toText   | (): Text    | Retorna o literal de texto 'Method' |
| arity    | (): Number  | Retorna a aridade do método         |

### Exemplo

```
_ <- fat.type.Method
x = (): Number -> 3
(~ x).toText  # retorna 'Method'
```

> note que é preciso [optar por não usar chamada automática](../../syntax/types/method.md#optando-por-não-usar-chamadas-automáticas) explicitamente para usar os membros do protótipo

## Veja também

- [Method (sintaxe)](../../syntax/types/method.md)
- [Pacote type](index.md)
