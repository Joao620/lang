{% if output.name != "ebook" %}

# Scope

{% endif %}

Extensões do protótipo Scope

## Importação

```
_ <- fat.type.Scope
```

## Construtor

| Nome  | Assinatura | Breve descrição           |
| ----- | ---------- | ------------------------- |
| Scope | (val: Any) | Envolver val em um escopo |

## Membros do protótipo

| Nome     | Assinatura          | Breve descrição                          |
| -------- | ------------------- | ---------------------------------------- |
| isEmpty  | (): Boolean         | Retorna verdadeiro se o tamanho for zero |
| nonEmpty | (): Boolean         | Retorna verdadeiro para tamanho não zero |
| size     | (): Number          | Retorna o número de entradas do escopo   |
| toText   | (): Text            | Retorna o literal de texto 'Scope'       |
| copy     | (): Scope           | Retorna cópia profunda do escopo         |
| keys     | (): List            | Retorna lista de chaves do escopo        |
| maybe    | (key: Text): Option | Retorna valor dentro de Option           |

### Exemplo

```
_ <- fat.type.Scope
x = { num = 12, prop = 'outra' }
x.size  # retorna 2
```

## Veja também

- [Scope (sintaxe)](../../syntax/types/scope.md)
- [Tipo Option](../extra/option.md)
- [Pacote type](index.md)
