{% if output.name != "ebook" %}

# Boolean

{% endif %}

Extensões do protótipo Boolean

## Importação

```
_ <- fat.type.Boolean
```

## Construtor

| Nome    | Assinatura | Breve descrição             |
| ------- | ---------- | --------------------------- |
| Boolean | (val: Any) | Coage o valor para booleano |

## Membros do protótipo

| Nome     | Assinatura  | Breve descrição                      |
| -------- | ----------- | ------------------------------------ |
| isEmpty  | (): Boolean | Retorna verdadeiro se falso          |
| nonEmpty | (): Boolean | Retorna falso se verdadeiro          |
| size     | (): Number  | Retorna 1 se verdadeiro, 0 se falso  |
| toText   | (): Text    | Retorna 'true' ou 'false' como texto |

### Exemplos

```
_ <- fat.type.Boolean

x = true
x.isEmpty  # falso, já que x é verdadeiro

Boolean('false')  # retorna true, porque o texto é não-vazio
Boolean('')       # retorna falso, porque é vazio
```

> note que o construtor não tenta converter o valor do texto, o que é consistente com as avaliações de controle de fluxo, e você pode usar um simples [case](../../syntax/flow.md#cases) se precisar fazer conversão de texto para booleano

## Veja também

- [Boolean (sintaxe)](../../syntax/types/boolean.md)
- [Pacote type](index.md)
