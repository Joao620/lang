{% if output.name != "ebook" %}

# pacote type

{% endif %}

Extensões do protótipo para [tipos nativos](../../syntax/types/index.md#tipos-nativos):

- [Void](void.md)
- [Boolean](boolean.md)
- [Number](number.md)
- [HugeInt](hugeint.md)
- [Text](text.md)
- [Method](method.md)
- [List](list.md)
- [Scope](scope.md)
- [Error](error.md)
- [Chunk](chunk.md)

> FatScript **não** carrega essas definições automaticamente no escopo global, portanto você deve **explicitamente** [importar](../../syntax/imports.md) quando necessário

## Importando

Se você quiser disponibilizar todos eles de uma só vez, basta escrever:

```
_ <- fat.type._
```

...ou importe um por um, conforme necessário, por exemplo:

```
_ <- fat.type.List
```

## características comum

Todos os tipos neste pacote suportam os seguintes métodos de protótipo:

- apply (construtor)
- isEmpty
- nonEmpty
- size
- toText

## Veja também

- [Tipos (sintaxe)](../../syntax/types/index.md)
