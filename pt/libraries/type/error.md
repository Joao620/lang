{% if output.name != "ebook" %}

# Error

{% endif %}

Extensões do protótipo Error

## Importação

```
_ <- fat.type.Error
```

## Aliases

- AssignError: atribuindo um novo valor a uma entrada imutável
- AsyncError: falha na operação assíncrona
- CallError: uma chamada é feita com argumentos insuficientes
- FileError: falha na operação de arquivo
- IndexError: o índice está fora dos limites da lista/texto
- KeyError: a chave (nome) não é encontrada no escopo
- SyntaxError: erro de sintaxe ou estrutura de código
- TypeError: inconsistência de tipo em chamada, retorno ou atribuição de método
- ValueError: tipo pode estar correto, mas conteúdo não é aceito

## Construtor

| Nome  | Assinatura | Breve descrição                           |
| ----- | ---------- | ----------------------------------------- |
| Error | (val: Any) | Retornar val coagido para texto como erro |

## Membros do protótipo

| Nome     | Assinatura  | Breve descrição            |
| -------- | ----------- | -------------------------- |
| isEmpty  | (): Boolean | Retorna verdadeiro, sempre |
| nonEmpty | (): Boolean | Retorna falso, sempre      |
| size     | (): Number  | Retorna 0, sempre          |
| toText   | (): Text    | Retorna o texto do erro    |

### Exemplo

```
_ <- fat.type.Error
x = Error('ops')
x.toText  # retorna "Error: ops"

# ...ou algo inesperado
e = undeclared.item  # gera erro
e.toText             # retorna "can't resolve scope of 'item'"
```

## Veja também

- [Biblioteca failure](../failure.md)
- [Error (sintaxe)](../../syntax/types/error.md)
- [Pacote type](index.md)
