{% if output.name != "ebook" %}

# Chunk

{% endif %}

Extensões do protótipo Chunk

## Importação

```
_ <- fat.type.Chunk
```

## Construtor

| Nome  | Assinatura | Breve descrição                     |
| ----- | ---------- | ----------------------------------- |
| Chunk | (val: Any) | Converte valor para bloco (binário) |

## Membros do protótipo

| Nome     | Assinatura                                 | Breve descrição                            |
| -------- | ------------------------------------------ | ------------------------------------------ |
| isEmpty  | (): Boolean                                | Retorna verdadeiro se o tamanho for zero   |
| nonEmpty | (): Boolean                                | Retorna verdadeiro se tamanho não-zero     |
| size     | (): Number                                 | Retorna o tamanho do bloco (em bytes)      |
| toText   | (): Text                                   | Converte o bloco para formato de texto     |
| toBytes  | (): List/Number                            | Converte o bloco para uma lista de bytes   |
| seekByte | (byte: Number, offset: Number = 0): Number | Retorna índice da primeira correspondência |
| patch    | (i, n, val: Chunk): Chunk                  | Insere val na posição i, removendo n bytes |

> `toText` substitui quaisquer sequências inválidas de UTF-8 por U+FFFD, representado como � em UTF-8

### Exemplo

```
_ <- fat.type.Chunk

x = Chunk('example')

x.size     # retorna 7
x.toText   # retorna 'example'
x.toBytes  # retorna [ 101, 120, 97, 109, 112, 108, 101 ]
```

## Veja também

- [Chunk (sintaxe)](../../syntax/types/chunk.md)
- [Pacote type](index.md)
