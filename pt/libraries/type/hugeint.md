{% if output.name != "ebook" %}

# HugeInt

{% endif %}

Extensões do protótipo HugeInt

## Importação

```
_ <- fat.type.HugeInt
```

## Construtor

| Nome    | Assinatura | Breve descrição                        |
| ------- | ---------- | -------------------------------------- |
| HugeInt | (val: Any) | Número ou texto convertido para HugInt |

> realiza a conversão de texto para número assumindo a representação hexadecimal

## Membros do protótipo

| Nome     | Assinatura                            | Breve descrição                                     |
| -------- | ------------------------------------- | --------------------------------------------------- |
| isEmpty  | (): Boolean                           | Retorna verdadeiro se zero                          |
| nonEmpty | (): Boolean                           | Retorna verdadeiro se não zero                      |
| size     | (): Number                            | Retorna número de bits necessários para representar |
| toText   | (): Text                              | Retorna número em texto hexadecimal                 |
| modExp   | (exp: HugeInt, mod: HugeInt): HugeInt | Retorna exponenciação modular                       |
| toNumber | (): Number                            | Converte para número (com perda de precisão)        |

### Notas de uso

Ao converter do tipo `Number` para `HugeInt`, o limite é 2^53, que é o valor máximo que pode ser representado com segurança como um inteiro sem perda de precisão. Tentar passar um valor acima deste limite resultará em um `ValueError`.

Por outro lado, ao converter de `HugeInt` para `Number`, valores de até 2^1023 - 1 podem ser convertidos com certo grau de perda de precisão. Tentar converter um valor acima disso resultará em `infinity` (infinito), o que pode ser verificado usando o método `isInf` fornecido pela [biblioteca de matemática](../math.md).

> a biblioteca de matemática também fornece o valor `maxInt`, que serve para avaliar a potencial perda de precisão; se um número é menor que `maxInt`, sua conversão de `HugeInt` é considerada segura sem perda de precisão

## Veja também

- [HugeInt (sintaxe)](../../syntax/types/hugeint.md)
- [Pacote type](index.md)
