{% if output.name != "ebook" %}

# List

{% endif %}

Extensões do protótipo List

## Importação

```
_ <- fat.type.List
```

## Construtor

| Nome | Assinatura | Breve descrição           |
| ---- | ---------- | ------------------------- |
| List | (val: Any) | Envolver val em uma lista |

## Membros do protótipo

| Nome       | Assinatura                 | Breve descrição                                |
| ---------- | -------------------------- | ---------------------------------------------- |
| isEmpty    | (): Boolean                | Retorna verdadeiro se o comprimento for zero   |
| nonEmpty   | (): Boolean                | Retorna verdadeiro para comprimento não zero   |
| size       | (): Number                 | Retorna o comprimento da lista                 |
| toText     | (): Text                   | Retorna o literal de texto 'List'              |
| join       | (sep: Text): Text          | Junta lista com separador em texto             |
| flatten    | (): List                   | Achata lista de listas em uma plana            |
| find       | (p: Method): Any           | Retorna a primeira correspondência ou nulo     |
| contains   | (p: Method): Boolean       | Checa se algum item corresponde ao predicado   |
| filter     | (p: Method): List          | Retorna sub-lista que corresponde ao predicado |
| reverse    | (): List                   | Retorna uma cópia invertida da lista           |
| shuffle    | (): List                   | Retorna uma cópia embaralhada da lista         |
| unique     | (): List                   | Retorna itens únicos da lista                  |
| sort       | (): List                   | Retorna uma cópia ordenada da lista            |
| sortBy     | (key: Any): List           | Retorna uma cópia ordenada da lista \*         |
| indexOf    | (item: Any): Number        | Retorna índice do item, -1 se ausente          |
| head       | (): Any                    | Retorna o primeiro item, nulo se vazio         |
| tail       | (): List                   | Retorna todos os itens, exceto o primeiro      |
| map        | (m: Method): List          | Utilitário funcional (permite encadeamento)    |
| reduce     | (m: Method, acc: Any): Any | Utilitário funcional                           |
| walk       | (m: Method): Void          | Aplicar efeitos colaterais para cada item      |
| patch      | (i, n, val: List): List    | Insere val na posição i, removendo n itens     |
| headOption | (): Option                 | Retorna o primeiro item, como Option           |
| itemOption | (index: Number): Option    | Retorna item por índice, como Option           |
| findOption | (p: Method): Option        | Busca item por predicado, como Option          |

### Exemplo

```
_ <- fat.type.List
x = [ 'a', 'b', 'c' ]
x.size  # retorna 3
```

### Ordenação

Os métodos `sort` e `sortBy` implementam o algoritmo de ordenação quicksort, aprimorado com a seleção aleatória de pivô. Essa abordagem é conhecida por sua eficiência, oferecendo uma complexidade de tempo médio de caso de O(n log n). Demonstra alto desempenho na maioria dos conjuntos de dados. Para conjuntos de dados que contêm valores ou chaves repetidas uma ordenação estável não pode ser garantida e seu desempenho pode degradar até O(n^2) no pior caso, onde todos os elementos são idênticos ou tem a mesma chave.

> `sortBy` aceita um parâmetro textual para `key` se for uma lista de `Scope` ou um parâmetro numérico caso seja uma lista de `List` (matriz), representando o índice

### Reduzindo

O método `reduce` no FatScript transforma uma lista em um único valor aplicando um redutor (`m: Method`) a cada elemento em sequência, começando de um valor acumulador inicial (`acc: Any`), ou do primeiro elemento caso nenhum valor seja provido. Este método é útil para operações que envolvem agregar dados de uma lista.

#### Características

- **Método Redutor:** O redutor deve receber o valor acumulador atual e o item atual da lista, retornando o valor acumulador atualizado.

- **Comportamento com Lista Vazia:** Quando o `reduce` é aplicado a uma lista vazia sem um valor acumulador inicial, ele retorna `null`.

#### Exemplo Prático

```
_ <- fat.type.List
sumReducer = (acc: Número, item: Número) -> acc + item
sum = [1, 2, 3].reduce(sumReducer)  # resulta em 6
```

> para transformações complexas de dados ou ao lidar com listas de escopos, estruture cuidadosamente o redutor para manipular os tipos de dados específicos e o resultado desejado

## Veja também

- [List (sintaxe)](../../syntax/types/list.md)
- [Tipo Option](../extra/option.md)
- [Pacote type](index.md)
