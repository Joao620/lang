{% if output.name != "ebook" %}

# sdk

{% endif %}

Utilitários do kit de desenvolvimento de software do fry

> uma biblioteca especial que expõe alguns dos elementos internos do interpretador fry

## Importação

```
_ <- fat.sdk
```

## Métodos

| Nome       | Assinatura            | Breve descrição                                   |
| ---------- | --------------------- | ------------------------------------------------- |
| ast        | (\_): Void            | Imprime árvore de sintaxe abstrata do nó          |
| stringify  | (\_): Text            | Converte nó em texto json                         |
| eval       | (\_): Any             | Interpreta texto como programa FatScript          |
| getVersion | (): Text              | Retorna versão do fry                             |
| printStack | (depth: Number): Void | Imprime pilha do contexto de execução             |
| readLib    | (ref: Text): Text     | Retorna o código-fonte da biblioteca fry          |
| typeOf     | (\_): Text            | Retorna o nome do tipo do nó                      |
| getTypes   | (): List              | Retorna info sobre tipos declarados               |
| getDef     | (name: Text): Any     | Retorna definição de tipo por nome                |
| getMeta    | (): Scope             | Retorna os metadados do interpretador             |
| setKey     | (key: Text): Void     | Definir chave para pacotes ofuscados              |
| setMem     | (n: Number): Void     | Definir limite de memória (contagem de nós)       |
| runGC      | (): Number            | Rodar o GC, retorna transcorrido em milissegundos |
| quickGC    | (): Number            | Roda uma única iteração do GC e retorna ms        |
| setAutoGC  | (n: Number): Void     | Configura GC para rodar a cada n novos nós        |

## Notas de uso

### readLib

```
_ <- fat.sdk
_ <- fat.console

print(readLib('fat.extra.Date'))  # imprime a implementação da biblioteca Date
```

> `readLib` não pode ver arquivos externos, mas `read` da [biblioteca file](file.md) pode

### setKey

Use preferencialmente no arquivo `.fryrc` assim:

```
_ <- fat.sdk
setKey('secret')  # irá codificar e decodificar pacotes com esta chave
```

Veja mais sobre [ofuscação](../general/bundling.md#ofuscação).

### setMem

Use preferencialmente no arquivo `.fryrc` assim:

```
_ <- fat.sdk
setMem(5000)  # ~2mb
```

### Escolhendo entre GC completo, rápido e automático

A maioria dos scripts simples em FatScript não precisará se preocupar com a gestão de memória, pois as configurações padrão são projetadas para fornecer aos desenvolvedores uma capacidade de memória razoavelmente grande e um comportamento automático sensato logo de início.

O método `quickGC` oferece uma limpeza rápida e menos exaustiva, tornando-o adequado para cenários onde alguma margem na alocação de memória é aceitável. Por outro lado, `runGC` garante uma coleta de lixo determinística e completa, embora ao custo de tempos de execução mais longos, dependendo de vários fatores como o tamanho e a complexidade do grafo de memória. No entanto, em certos cenários, `quickGC` pode levar ao acúmulo de memória não reclamada e pode não ser a opção mais eficaz.

Além da escolha manual entre `quickGC` e `runGC`, existe também um GC automático baseado em heurísticas. Ele vem desativado por padrão, mas pode ser habilitado chamando `setAutoGC` com um valor não zero. Essa heurística aplica `quickGC` quando há memória livre suficiente, garantindo uma interrupção mínima. Em contraste, sob alta pressão de memória, `fullGC` é executado para uma limpeza abrangente. Esta estratégia equilibra a eficiência da memória com o desempenho da aplicação, adaptando-se dinamicamente ao padrão de uso da memória.

Veja mais sobre [gerenciamento de memória](../general/options.md#gerenciamento-de-memória).
