{% if output.name != "ebook" %}

# curses

{% endif %}

Interface de usuário baseada em terminal

> embora a inspiração seja reconhecida, o FatScript tem seu próprio modo de abordar a interface de usuário no terminal, que difere em muitos aspectos da biblioteca curses original

## Importação

```
_ <- fat.curses
```

## Methods

| Nome      | Assinatura                                        | Breve descrição                   |
| --------- | ------------------------------------------------- | --------------------------------- |
| box       | (p1: Scope, p2: Scope): Void                      | Desenhar quadrado de pos1 a pos2  |
| fill      | (p1: Scope, p2: Scope, p: Text = ' '): Void       | Preencher de pos1 a pos2 com p    |
| clear     | (): Void                                          | Limpar buffer de tela             |
| refresh   | (): Void                                          | Renderizar buffer de tela         |
| getMax    | (): Scope                                         | Retorna tamanho da tela como x, y |
| printAt   | (pos: Scope, msg: Any, width: Number = ø): Void   | Imprimir msg em { x, y } pos      |
| makePair  | (fg: Number = ø, bg: Number = ø): Number          | Criar um par de cores             |
| usePair   | (pair: Number): Void                              | Aplicar par de cores              |
| frameTo   | (cols: Number, rows: Number)                      | Alinhar área ao centro da tela    |
| readKey   | (): Text                                          | Retorna tecla pressionada         |
| readText  | (pos: Scope, width: Number, prev: Text = ø): Text | Inicia caixa de texto             |
| flushKeys | (): Void                                          | Limpar buffer de entrada          |
| endCurses | (): Void                                          | Encerrar o modo curses            |

> as posições (pos) estão no formato { x: Number, y: Number }

> os métodos nesta biblioteca **não garantem** a segurança de threads em cenários assíncronos, ou utilize a thread principal **ou então** um único [worker](async.md) para renderizar atualizações no console

## Notas de uso

Qualquer método desta biblioteca, exceto `getMax` e `endCurses`, iniciará o modo curses se ainda não tiver iniciado. Note que métodos como `log`, `stderr` e `input` da biblioteca [console](console.md) chamarão `endCurses` implicitamente. No entanto, `moveTo`, `print` e `flush` não irão alterar o modo de saída e podem ser combinados com métodos curses, o que pode ser útil em algumas circunstâncias.

As letras `x` e `y` representam coluna e linha, respectivamente, ao chamar `printAt`, onde (0, 0) é o canto superior esquerdo e o resultado de `getMax` é apenas a primeira coordenada fora do canto inferior direito.

> caracteres especiais em curses só funcionam se um [locale](system.md) UTF-8 puder ser definido

### makePair

Você pode importar a biblioteca [color](color.md) para usar nomes de cores e criar uma combinação de primeiro plano e plano de fundo (par). Passe `null` para aplicar a cor padrão no parâmetro desejado.

### usePair

A entrada deste método deve ser um par de cores criado com o método `makePair`. Ele deixa este par habilitado até que você chame-o novamente com um par diferente.

### readKey

Este método não bloqueia e retorna `null` se `stdin` estiver vazio, caso contrário retornará um caractere por vez.

Chaves especiais podem ser detectadas e retornar palavras-chave como:

- teclas de seta:
  - up
  - down
  - left
  - right
- teclas de edição:
  - delete
  - backspace
  - enter
  - space
  - tab
  - backTab (shift+tab)
- teclas de controle:
  - pageUp
  - pageDown
  - home
  - end
  - insert
  - esc
- outras:
  - resize (janela do terminal foi redimensionada)

> a detecção correta das teclas pode depender do contexto ou da plataforma

### readText

Entra em modo captura de texto utilizando uma área demarcada por posição e largura da caixa de texto. Se o texto for maior que o espaço uma rolagem automática do texto é realizada. O texto completo é retornado ao pressionar `enter` ou `tab`, no entanto caso `esc` seja pressionado é retornado `null`.

## Veja também

- [Biblioteca color](color.md)
- [Biblioteca console](console.md)
