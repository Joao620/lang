{% if output.name != "ebook" %}

# enigma

{% endif %}

Métodos de criptografia, hash e UUID

## Importação

```
_ <- fat.enigma
```

## Métodos

| Nome    | Assinatura                        | Breve descrição                |
| ------- | --------------------------------- | ------------------------------ |
| getHash | (msg: Text): Number               | Obter hash de 32-bits do texto |
| genUUID | (): Text                          | Gerar um UUID (versão 4)       |
| genKey  | (len: Number): Text               | Gerar chave aleatória          |
| derive  | (secret: Text): Text              | Função de derivação de chave   |
| encrypt | (msg: Text, key: Text = ''): Text | Encriptar msg usando key       |
| decrypt | (msg: Text, key: Text = ''): Text | Decriptar msg usando key       |

> `derive` é determinística e usa o alfabeto Base64 para uma saída de 32 caracteres

## Notas de uso

Você pode omitir ou passar uma chave (key) em branco `''` para usar a chave padrão.

### Atenção!

Embora `enigma` torne o texto encriptado "não legível por humanos", esse esquema não é criptograficamente seguro! NÃO o utilize sozinho para proteger dados!

Se pareado com uma chave personalizada que não esteja armazenada junto com a mensagem, pode oferecer alguma proteção de dados.

### Conformidade do método UUID

Um UUID, ou Universally Unique Identifier, é um número de 128 bits usado para identificar objetos ou entidades em sistemas de computador. A implementação fornecida gera UUIDs aleatórios como texto que segue o formato da versão 4 da especificação RFC 4122, mas não adere estritamente à aleatoriedade criptograficamente segura necessária. Na prática, o risco de colisão tem uma probabilidade extremamente baixa e é muito improvável de ocorrer, e para a maioria das aplicações pode ser considerado bom o bastante.
