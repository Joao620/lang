{% if output.name != "ebook" %}

# math

{% endif %}

Operações e funções matemáticas

## Importação

```
_ <- fat.math
```

## Constantes

- e, logaritmo natural constante 2.71...
- maxInt, 9007199254740992
- minInt, -9007199254740992
- pi, razão do círculo para o seu diâmetro 3.14...

> leia mais sobre [precisão numérica](../syntax/types/number.md#precisão) no FatScript

## Funções básicas

| Nome   | Assinatura                            | Breve descrição                           |
| ------ | ------------------------------------- | ----------------------------------------- |
| abs    | (x: Number): Number                   | Retorna o valor absoluto de x             |
| ceil   | (x: Number): Number                   | Retorna o menor inteiro >= x              |
| floor  | (x: Number): Number                   | Retorna o maior inteiro <= x              |
| isInf  | (x: Number): Boolean                  | Retorna true se x for infinito            |
| isNaN  | (x: Any): Boolean                     | Retorna true se x não for um número       |
| logN   | (x: Number, base: Number = e): Number | Retorna o logaritmo                       |
| random | (): Number                            | Retorna pseudo-aleatório, onde 0 <= n < 1 |
| sqrt   | (x: Number): Number                   | Retorna a raiz quadrada de x              |
| round  | (x: Number): Number                   | Retorna o inteiro mais próximo de x       |

## Funções trigonométricas

| Nome     | Assinatura                 | Breve descrição                 |
| -------- | -------------------------- | ------------------------------- |
| sin      | (x: Number): Number        | Retorna o seno de x             |
| cos      | (x: Number): Number        | Retorna o cosseno de x          |
| tan      | (x: Number): Number        | Retorna a tangente de x         |
| asin     | (x: Number): Number        | Retorna o arco seno de x        |
| acos     | (x: Number): Number        | Retorna o arco cosseno de x     |
| atan     | (x: Number, y = 1): Number | Retorna o arco tangente de x, y |
| radToDeg | (r: Number): Number        | Converte radianos para graus    |
| degToRad | (d: Number): Number        | Converte graus para radianos    |

## Funções hiperbólicas

| Nome | Assinatura          | Breve descrição                     |
| ---- | ------------------- | ----------------------------------- |
| sinh | (x: Number): Number | Retorna o seno hiperbólico de x     |
| cosh | (x: Number): Number | Retorna o cosseno hiperbólico de x  |
| tanh | (x: Number): Number | Retorna a tangente hiperbólica de x |

## Funções estatísticas

| Nome     | Assinatura               | Breve descrição                     |
| -------- | ------------------------ | ----------------------------------- |
| mean     | (v: List/Number): Number | Retorna a média de um vetor         |
| median   | (v: List/Number): Number | Retorna a mediana de um vetor       |
| sigma    | (v: List/Number): Number | Retorna o desvio padrão de um vetor |
| variance | (v: List/Number): Number | Retorna a variância de um vetor     |
| max      | (v: List/Number): Number | Retorna o valor máximo no vetor     |
| min      | (v: List/Number): Number | Retorna o valor mínimo no vetor     |
| sum      | (v: List/Number): Number | Retorna a soma do vetor             |

## Outras funções

| Nome    | Assinatura          | Breve descrição                   |
| ------- | ------------------- | --------------------------------- |
| fact    | (x: Number): Number | Retorna o fatorial de x           |
| exp     | (x: Number): Number | Retorna e elevado à potência de x |
| sigmoid | (x: Number): Number | Retorna o sigmoid de x            |
| relu    | (x: Number): Number | Retorna o ReLU de x               |

### Exemplo

```
math <- fat.math  # importação nomeada
math.abs(-52)     # retorna 52
```

## Veja também

- [Number (sintaxe)](../syntax/types/number.md)
- [Extensões do protótipo Number](type/number.md)
