{% if output.name != "ebook" %}

# http

{% endif %}

Framework de manipulação HTTP

## Importação

```
_ <- fat.http
```

### Route

Uma rota é uma estrutura usada para mapear métodos HTTP para certos padrões de caminho, especificando qual código deve ser executado quando uma requisição é recebida. Cada rota pode definir um comportamento diferente para cada método HTTP (`POST`, `GET`, `PUT`, `DELETE`).

#### Construtor

| Nome  | Assinatura                                                           | Breve descrição          |
| ----- | -------------------------------------------------------------------- | ------------------------ |
| Route | (path: Text, post: Method, get: Method, put: Method, delete: Method) | Constrói um objeto Route |

> cada método implementado recebe um HttpRequest como argumento e deve retornar um objeto HttpResponse

### HttpRequest

Um `HttpRequest` representa uma mensagem de requisição HTTP. Isso é o que seu servidor recebe de um cliente quando ele faz uma requisição ao seu servidor.

#### Construtor

| Nome        | Assinatura                                                               | Breve descrição                |
| ----------- | ------------------------------------------------------------------------ | ------------------------------ |
| HttpRequest | (method: Text, path: Text, params: Scope, headers: List/Text, body: Any) | Constrói um objeto HttpRequest |

### HttpResponse

Um `HttpResponse` representa uma mensagem de resposta HTTP. Isso é o que um servidor envia de volta ao cliente em resposta a uma requisição HTTP.

#### Construtor

| Nome         | Assinatura                                      | Breve descrição                 |
| ------------ | ----------------------------------------------- | ------------------------------- |
| HttpResponse | (status: Number, headers: List/Text, body: Any) | Constrói um objeto HttpResponse |

## Métodos

| Nome       | Assinatura                            | Breve descrição                      |
| ---------- | ------------------------------------- | ------------------------------------ |
| setHeaders | (headers: List): Void                 | Definir cabeçalhos de solicitações   |
| post       | (url: Text, body, wait): HttpResponse | Criar/postar body para url           |
| get        | (url: Text, wait): HttpResponse       | Ler/obter de url                     |
| put        | (url: Text, body, wait): HttpResponse | Atualizar/colocar body na url        |
| delete     | (url: Text, wait): HttpResponse       | Excluir em url                       |
| setName    | (name: Text): Void                    | Definir agente/nome do servidor      |
| verifySSL  | (enabled: Boolean): Void              | Configuração SSL (modo cliente)      |
| setSSL     | (certPath: Text, keyPath: Text): Void | Configuração SSL (modo servidor)     |
| listen     | (port: Number, routes: List/Route)    | Provedor de endpoint (modo servidor) |

> `body: Any` e `wait: Number` são sempre parâmetros opcionais, sendo que caso `body` não se enquadre como `Text` ou `Chunk` será automaticamente convertido para JSON no processo de envio e `wait` é o tempo máximo de espera e o padrão é 30.000ms (30 segundos)

> `verifySSL` está habilitado por padrão para o modo cliente

> `setSSL` pode não estar disponível, caso o sistema não tenha a biblioteca OpenSSL

## Notas de uso

### Modo cliente

Em `HttpResponse.body`, você pode precisar decodificar explicitamente uma resposta JSON para `Scope` usando o método `fromJSON`. Para postar um tipo nativo como JSON, você pode codificá-lo usando o método `toJSON`; no entanto, isso não é estritamente necessário, pois será feito implicitamente. Ambos os métodos estão disponíveis na biblioteca [fat.recode](recode.md).

Se os cabeçalhos não forem definidos, o cabeçalho `Content-Type` padrão para `Chunk` será `application/octet-stream`, para `Text` será `text/plain; charset=UTF-8` e para outros tipos, será `application/json; charset=UTF-8` (devido à conversão implícita).

Você pode definir cabeçalhos de solicitação personalizados da seguinte forma:

```
http <- fat.http

url = ...
token = ...
body = ...

http.setHeaders([
  "Accept: application/json; charset=UTF-8"
  "Content-Type: application/json; charset=UTF-8"
  "Authorization: Bearer " + token  # cabeçalhos personalizado
])

http.post(url, body)
```

> definir cabeçalhos substituirá completamente a lista anterior pela nova lista

Ao realizar solicitações assíncronas, você pode precisar chamar `setHeaders`, `setName` e configurar `verifySSL` dentro de cada Worker, já que essas configurações são locais para cada thread.

### Modo servidor

### Lidando com respostas HTTP

O servidor FatScript lida automaticamente com os códigos de status HTTP comuns, como 200, 400, 404, 405, 500 e 501. Sendo 200 o padrão ao construir um objeto `HttpResponse`.

Além dos códigos de status retornados de forma automática, você também pode retornar explicitamente estes e outros códigos de status, como 201, 202, 203, 204, 205, 301, 401 e 403, especificando o código de status no objeto `HttpResponse`, por exemplo: `HttpResponse(status = 401)`. Em todos os casos, quando aplicável, o servidor fornece corpos de resposta padrão em texto simples. No entanto, você tem a opção de substituir esses valores padrão e fornecer seus próprios corpos de resposta personalizados, quando necessário.

Ao lidar automaticamente com esses códigos de status e fornecer corpos de resposta padrão, o servidor FatScript simplifica o processo de desenvolvimento, ao mesmo tempo em que permite que você tenha controle sobre o conteúdo da resposta quando necessário.

> não pertencendo a nenhum dos códigos anteriores, o servidor irá retornar o código 500

Veja um exemplo de um simples servidor HTTP de arquivos:

```
_    <- fat.type.Text
file <- fat.file
http <- fat.http
{ Route, HttpRequest, HttpResponse } = http

# adapte para o local do conteúdo
basePath = '/home/user/contentFolder'

# restrito a algumas extensões somente
getContentType = (path: Text): Text -> {
  ext2 = path(-3..).toLower
  ext3 = path(-4..).toLower
  ext4 = path(-5..).toLower

  ext4 == '.html' => 'Content-Type: text/html'
  ext3 == '.htm'  => 'Content-Type: text/html'
  ext2 == '.js'   => 'Content-Type: application/javascript'
  ext4 == '.json' => 'Content-Type: application/json'
  ext3 == '.css'  => 'Content-Type: text/css'
  ext2 == '.md'   => 'Content-Type: text/markdown'
  ext3 == '.xml'  => 'Content-Type: application/xml'
  ext3 == '.csv'  => 'Content-Type: text/csv'
  ext3 == '.txt'  => 'Content-Type: text/plain'
  ext4 == '.svg'  => 'Content-Type: image/svg+xml'
  ext3 == '.rss'  => 'Content-Type: application/rss+xml'
  ext4 == '.atom' => 'Content-Type: application/atom+xml'
  ext3 == '.png'  => 'Content-Type: image/png'
  ext3 == '.jpg'  => 'Content-Type: image/jpeg'
  ext4 == '.jpeg' => 'Content-Type: image/jpeg'
  ext3 == '.gif'  => 'Content-Type: image/gif'
  ext3 == '.ico'  => 'Content-Type: image/icon'
}

routes: List/Route = [
  Route(
    '*'
    get = (request: HttpRequest): HttpResponse -> {
      path = basePath + request.path
      type = getContentType(path)

      !type             => HttpResponse(status = 403)  # forbidden
      file.exists(path) => HttpResponse(body = file.readBin(path), headers = [ type ])
      _                 => HttpResponse(status = 404)  # not found
    }
  )
]

http.listen(8080, routes)
```

> em uma aplicação real, `request.path` deve ser sanitizado antes de ser utilizado para acessar arquivos no servidor; aqui, é utilizado diretamente apenas como exemplo
