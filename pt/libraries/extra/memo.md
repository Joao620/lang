{% if output.name != "ebook" %}

# Memo

{% endif %}

Utilitário de memoização genérica
(também pode criar valores preguiçosos)

## Importação

```
_ <- fat.extra.Memo
```

## Construtor

| Nome | Assinatura       | Breve descrição                        |
| ---- | ---------------- | -------------------------------------- |
| Memo | (method: Method) | Cria uma instância Memo para um método |

> a aridade do método memoizado pode ser 1 ou então 0 (para valores preguiçosos)

## Membros do protótipo

| Nome     | Assinatura      | Breve descrição                                 |
| -------- | --------------- | ----------------------------------------------- |
| asMethod | (): Method      | Retorna uma versão memoizada do método original |
| call     | (arg: Any): Any | Chamada memoizada; armazena e retorna resultado |

### Exemplo

Memo é útil para otimizar funções, armazenando os resultados. Ela armazena o resultado das chamadas de função e retorna o resultado armazenado quando as mesmas entradas ocorrem novamente.

```
_ <- fat.extra.Memo

fib = (n: Number) -> {
  n <= 2 => 1
  _      => quickFib(n - 1) + quickFib(n - 2)
}

quickFib = Memo(fib).asMethod

quickFib(50)  # 12586269025
```

Agora você pode chamar `quickFib` como se estivesse chamando `fib`, mas com resultados armazenados em cache para entradas computadas anteriormente.

> aviso: pode causar acúmulo na alocação de memória
