{% if output.name != "ebook" %}

# HashMap

{% endif %}

Armazenamento otimizado em memória de par chave-valor, servindo como um substituto de melhor desempenho para a implementação padrão do Scope, projetado para lidar eficientemente com grandes conjuntos de dados.

> os ganhos de velocidade vem em detrimento de um maior uso de memória

## Importação

```
_ <- fat.extra.HashMap
```

## Construtor

| Nome    | Assinatura              | Breve descrição                                 |
| ------- | ----------------------- | ----------------------------------------------- |
| HashMap | (capacity: Number = 97) | Cria um HashMap com uma capacidade especificada |

> a capacidade padrão de 97 é geralmente eficiente para até 10.000 itens

### Otimização de Capacidade

Idealmente, você deve manter no máximo cerca de 100 itens por 'compartimento' na tabela hash. Neste contexto, 'capacidade' refere-se ao número de compartimentos disponíveis para seus dados. Note que esta implementação não ajusta seu tamanho automaticamente, portanto, um dimensionamento inicial adequado é crucial. A seguinte tabla pode ajudar a determinar a capacidade ótima para armazenar `n` itens:

```
n <   5000 => 53
n <  10000 => 97
n <  20000 => 193
n <  40000 => 389
n <  80000 => 769
n < 160000 => 1543
_          => 3079
```

> usar números primos pode ajudar a reduzir colisões

Estes valores são baseados em testes empíricos e devem ser ajustados de acordo com suas necessidades específicas de dados e objetivos de desempenho. Tenha em mente que a relação entre capacidade e desempenho não é totalmente linear; à medida que o número de itens aumenta, os benefícios de aumentar ainda mais a capacidade diminuem.

### Recomendação

Embora o Scope padrão do FatScript mostre um desempenho mais lento para inserções e seja particularmente lento para deleções (como definir para `null`), ele se destaca na recuperação e atualização de dados, superando a velocidade do `HashMap` para pequenas coleções (menos de ~500 itens). Portanto, os benefícios de usar o `HashMap` são mais notáveis em cenários que envolvem inserções e deleções frequentes em grandes conjuntos de dados.

## Membros do protótipo

| Nome     | Assinatura                   | Breve descrição                                 |
| -------- | ---------------------------- | ----------------------------------------------- |
| isEmpty  | (): Boolean                  | Retorna verdadeiro se o comprimento for zero    |
| nonEmpty | (): Boolean                  | Retorna verdadeiro para comprimento não zero    |
| size     | (): Number                   | Retorna o comprimento da tabela hash            |
| toText   | (): Text                     | Retorna o literal de texto 'HashMap/capacity'   |
| set      | (key: Text, value: Any): Any | Define um par chave-valor no HashMap            |
| get      | (key: Text): Any             | Obtém o valor associado a uma chave             |
| keys     | (): List/Text                | Retorna uma lista de todas as chaves do HashMap |

### Exemplo

```
_ <- fat.extra.HashMap

hmap = HashMap()
hmap.set('key1', 'value1')

hmap.get('key1')  # retorna 'value1'
hmap.keys         # retorna [ 'key1' ]
```
