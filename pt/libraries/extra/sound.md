{% if output.name != "ebook" %}

# Sound

{% endif %}

Interface de reprodução de som

Wrapper para players de áudio de linha de comando usando [fork e kill](../system.md).

## Importação

```
_ <- fat.extra.Sound
```

## Construtor

O construtor `Sound` recebe três argumentos:

- **path**: o caminho do arquivo de áudio.
- **duração** (opcional): o tempo de espera (em milissegundos) para poder reproduzir novamente o arquivo. Geralmente, você deseja definir isso como a duração exata do seu áudio.
- **player** (opcional): o player padrão utilizado é `aplay` (utilitário de áudio comum no Linux, apenas dá suporte a wav), mas você pode usar `ffplay` para reproduzir mp3, por exemplo, definindo `ffplay = [ 'ffplay', '-nodisp', '-autoexit', '-loglevel', 'quiet' ]` e fornecendo-o como argumento para sua instância de som. Neste caso o pacote `ffmpeg` precisa estar instalado no sistema.

## Membros do protótipo

| Nome | Assinatura | Breve descrição                                 |
| ---- | ---------- | ----------------------------------------------- |
| play | (): Void   | Inicia reprodução, se ainda não estiver tocando |
| stop | (): Void   | Interrompe reprodução, se ainda estiver tocando |

> o estado de "estar tocando" é inferido do parâmetro de duração

### Exemplo

```
_    <- fat.extra.Sound
time <- fat.time

applause = Sound('applause.wav', 5000);
applause.play
time.wait(5000)
```

> note que Sound cria um processo filho para reproduzir o áudio, portanto, a reprodução é assíncrona

### Som na Web Build

Ao usar `fry` construído com Emscripten (por exemplo, ao usar FatScript Playground), este protótipo utiliza comandos embutidos `$soundPlay` e `$soundStop`, que são definidos apenas na compilação para web. Portanto, em vez de utilizar um reprodutor de áudio CLI através de fork do processo, há suporte de áudio via SDL2/WebAudio.

## Veja também

- [Pacote extra](index.md)
