{% if output.name != "ebook" %}

# Logger

{% endif %}

Suporte ao registro de logs

> desde logs simples em console a registro baseado em arquivos

## Importação

```
_ <- fat.extra.Logger
```

> [biblioteca console](../console.md), [biblioteca color](../color.md), [biblioteca file](../file.md), [biblioteca time](../time.md), [biblioteca sdk](../sdk.md) e [biblioteca de tipos](../type/index.md) são automaticamente importadas com esta importação

## Tipo Logger

`Logger` oferece capacidades de registro de logs personalizáveis com vários níveis e formatos.

### Propriedades

- `level`: Text (padrão 'debug') - Nível de log
- `showTime`: Boolean (padrão verdadeiro) - Indicador para exibir carimbos de hora

> níveis válidos: 'debug', 'info', 'warn', 'error'

### Membros do protótipo

| Nome        | Assinatura                        | Breve descrição                                 |
| ----------- | --------------------------------- | ----------------------------------------------- |
| setLevel    | (level: Text)                     | Define o nível de log                           |
| setShowTime | (showTime: Boolean)               | Alterna a exibição de carimbos de hora nos logs |
| asMessage   | (level: Text, args: Scope): Texto | Formata mensagens de log (pode ser substituído) |
| log         | (msg: Any, fg: Number)            | Registra mensagens (pode ser substituído)       |

### Métodos de log

- `debug(_1, _2, _3, _4, _5)`: Registra uma mensagem de debug
- `info(_1, _2, _3, _4, _5)`: Registra uma mensagem informativa
- `warn(_1, _2, _3, _4, _5)`: Registra uma mensagem de aviso
- `error(_1, _2, _3, _4, _5)`: Registra uma mensagem de erro

## Subtipos

### BoringLogger

- Herda de `Logger`
- Substitui `log` para emitir texto simples sem cor

### FileLogger

- Herda de `Logger`
- Propriedades Adicionais:
  - `logfile`: Texto (padrão 'log.txt') - arquivo para registro de logs
- Substitui `log` para anexar mensagens a um arquivo

## Exemplo de uso

```
_ <- fat.extra.Logger

# Crie uma instância com configurações personalizadas
myLogger = Logger(level = 'info', showTime = false)

# Registra uma mensagem informativa
myLogger.info('Esta é uma mensagem informativa.')

# Crie um FileLogger para registrar mensagens em um arquivo
fileLogger = FileLogger('meuLog.txt')
fileLogger.info('Registrado no arquivo.')
```
