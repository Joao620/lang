{% if output.name != "ebook" %}

# pacote extra

{% endif %}

Tipos adicionais implementados em FatScript puro:

- [Date](date.md) - Gerenciamento de calendário e datas
- [Duration](duration.md) - Construtor de duração em milissegundos
- [HashMap](hmap.md) - Armazenamento rápido de chave-valor
- [Logger](logger.md) - Suporte ao registro de logs
- [Memo](memo.md) - Utilitário de memoização genérica
- [Option](option.md) - Encapsulamento de valor opcional
- [Param](param.md) - Verificação de presença e tipo de parâmetro
- [Sound](sound.md) - Interface de reprodução de som
- [Storable](storable.md) - Armazenamento de dados

## Importando

Se você quiser disponibilizar todos eles de uma só vez, basta escrever:

```
_ <- fat.extra._
```

...ou importe um por um, conforme necessário, por exemplo:

```
_ <- fat.Date
```

## Nota do desenvolvedor

Atualmente, a maioria desses utilitários não são otimizados para recursos ou desempenho.

A intenção aqui era mais fornecer recursos simples, como modelos básicos que podem ser extraídos via [readLib](../sdk.md), para que qualquer desenvolvedor com requisitos específicos tenha um ponto de partida para suas próprias implementações.
