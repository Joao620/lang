{% if output.name != "ebook" %}

# Date

{% endif %}

Gerenciamento de calendário e datas

> operações como adição e subtração de dias, meses e anos, garantindo o tratamento preciso de várias complexidades relacionadas a datas, como anos bissextos e cálculos de final de mês

## Importação

```
_ <- fat.extra.Date
```

> [biblioteca time](../time.md), [biblioteca math](../math.md), [tipo Error](../type/error.md), [tipo Text](../type/text.md), [tipo List](../type/list.md), [tipo Number](../type/number.md), [tipo Duration](duration.md) são automaticamente importados com esta importação

## Tipo Date

`Date` oferece uma solução abrangente para o gerenciamento de datas, incluindo anos bissextos e horário do dia.

### Propriedades

- `year`: Número - Ano da data
- `month`: Número - Mês da data
- `day`: Número - Dia da data
- `tms`: Milissegundos - Horário do dia em milissegundos

> valor padrão aponta para: 1 de janeiro de 1970

### Membros do protótipo

| Nome           | Assinatura                            | Breve descrição                           |
| -------------- | ------------------------------------- | ----------------------------------------- |
| fromEpoch      | (ems: Epoch): Date                    | Cria uma instância a partir de um epoch   |
| isLeapYear     | (year: Número): Boolean               | Determina se um ano é bissexto            |
| normalizeMonth | (month: Número): Número               | Normaliza o número do mês                 |
| daysInMonth    | (year: Número, month: Número): Número | Retorna o número de dias no mês de um ano |
| isValid        | (year, month, day, tms): Boolean      | Valida os componentes da data             |
| truncate       | (): Date                              | Trunca o horário do dia                   |
| toEpoch        | (): Epoch                             | Converte a instância para tempo em epoch  |
| addYears       | (yearsToAdd: Número): Date            | Adiciona anos à data                      |
| addMonths      | (monthsToAdd: Número): Date           | Adiciona meses à data                     |
| addWeeks       | (weeksToAdd: Número): Date            | Adiciona semanas à data                   |
| addDays        | (daysToAdd: Número): Date             | Adiciona dias à data                      |

### Exemplos de uso

```
_ <- fat.extra.Date

# Criar uma instância de Data
myDate = Date(2023, 1, 1)

# Adicionar um ano à data
newDate = myDate.addYears(1)

# Adicionar duas semanas à uma data
datePlusTwoWeeks = myDate.addWeeks(2)

# Criar uma Data a partir de um tempo em epoch (em milissegundos)
# o resultado é influenciado pelo fuso horário atual, veja: time.setZone
epochTime = 1672531200000
dateFromEpoch = Date.fromEpoch(Epoch(epochTime))

# Converter uma data para tempo em epoch
epochFromDate = myDate.toEpoch
```
