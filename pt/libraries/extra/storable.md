{% if output.name != "ebook" %}

# Storable

{% endif %}

Armazenamento de dados

## Importação

```
_ <- fat.extra.Storable
```

> [biblioteca file](../file.md), [biblioteca sdk](../sdk.md), [biblioteca enigma](../enigma.md), [tipo Error](../type/error.md), [tipo Text](../type/text.md), [tipo Void](../type/void.md) e [tipo Method](../type/method.md) são automaticamente importados com esta importação

## Mixins

Esta biblioteca introduz dois tipos de mixin: `Storable` e `EncryptedStorable`

### Storable

O mixin `Storable` fornece métodos para armazenar e recuperar objetos no sistema de arquivos usando serialização JSON.

#### Membros do protótipo

| Nome  | Assinatura      | Breve descrição                                |
| ----- | --------------- | ---------------------------------------------- |
| list  | (): List/Text   | Obtém lista de ids para instâncias armazenadas |
| load  | (id: Text): Any | Carrega um objeto do sistema de arquivos       |
| save  | (): Boolean     | Salva a instância do objeto atual              |
| erase | (): Boolean     | Exclui o arquivo associado ao id               |

> os métodos `load` e `save` emitem `FileError` em caso de falha

### EncryptedStorable

Estende `Storable` com capacidades de criptografia para um armazenamento de dados mais seguro. Requer uma implementação do método `getEncryptionKey`.

## Exemplo de uso

```
_ <- fat.extra.Storable

# Defina um tipo que inclua Storable (ou EncryptedStorable)
User = (
  Storable  # Inclui o mixin Storable

  # EncryptedStorable                             # implementação alternativa
  # getEncryptionKey = (): Text -> '3ncryp1ptM3'  # poderia obter via KMS ou configuração

  ## Argumentos
  name: Text
  email: Text

  # Os setters retornam uma nova cópia imutável da instância com o campo atualizado
  setName = (name: Text) -> self + User * { name }
  setEmail = (email: Text) -> self + User * { email }
)

# Cria uma nova instância de usuário
newUser = User('Jane Doe', 'jane.doe@example.com')

# Salva o novo usuário
newUser.save

# Atualiza as informações do usuário e salva as alterações
updatedUser = newUser
  .setName('Jane Smith')
  .setEmail('jane.smith@example.com')
updatedUser.save

# Lista todos os usuários salvos
userIds = User.list

# Carrega um usuário do sistema de arquivos
userId = userIds(0)  # ...ou newUser.id
loadedUser = User.load(userId)

# Exclui os dados do usuário do sistema de arquivos
loadedUser.erase  # ...ou User.erase(userId)
```

### Storable na Web Build

Ao usar `fry` construído com Emscripten (por exemplo, ao usar FatScript Playground), este protótipo utiliza comandos embutidos `$storableSave`, `$storableLoad`,`$storableList` e `$storableErase` , que são definidos apenas na compilação para web. Portanto, em vez de utilizar o sistema de arquivos convencional para o armazenamento, há suporte especial para utilização do objeto `localStorage` do navegador.

## Veja também

- [Pacote extra](index.md)
