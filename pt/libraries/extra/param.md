{% if output.name != "ebook" %}

# Param

{% endif %}

Verificação de presença e tipo de parâmetro

## Importação

```
_ <- fat.extra.Param
```

> [tipo Text](../type/text.md) e [tipo Error](../type/error.md) são automaticamente importados com esta importação

## Tipos

Esta biblioteca introduz o tipo `Param` e o utilitário `Using` para declaração de parâmetros implícitos.

## Construtores

Os construtores de `Param` e `Using` recebem dois argumentos:

- **\_exp**: o nome do parâmetro a ser verificado no contexto.
- **\_typ**: o tipo esperado do valor avaliado.

### Param

O tipo `Param` oferece mecanismos para verificar a presença e o tipo de parâmetros no contexto de execução.

#### Membros do protótipo

| Nome | Assinatura | Resumo                                       |
| ---- | ---------- | -------------------------------------------- |
| get  | (): Any    | Recupera o parâmetro se corresponder ao tipo |

> o método `get` gera `KeyError` se o parâmetro não estiver definido, e `TypeError` se o tipo não corresponder

#### Exemplo

```
_ <- fat.extra.Param

currentUser = Param('userId', 'Text')

...

# Assumindo que userId está definido no contexto e é um texto,
# recupere de forma segura seu valor do namespace atual
userId = currentUser.get
```

### Using

Aplique `Using` para suprimir dicas de parâmetros implícitos em declarações de métodos para entradas esperadas no escopo.

> alternativamente, para suprimir avisos sobre parâmetros implícitos, nomeie a entrada implícita começando com um sublinhado (`_`)

#### Exemplo

```
_ <- fat.extra.Param

printUserIdFromContext = -> {
  Using('userId', 'Text')
  console.log(userId)
}
```

> se o parâmetro implícito estiver faltando ou não corresponder, um erro será gerado em tempo de execução quando o método for chamado

## Veja também

- [Pacote extra](index.md)
