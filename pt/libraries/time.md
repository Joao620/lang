{% if output.name != "ebook" %}

# time

{% endif %}

Manipulação de hora e data

## Importação

```
_ <- fat.time
```

> [tipo Number](type/number.md) é importado automaticamente com esta importação

## Métodos

| Nome       | Assinatura                         | Breve descrição                      |
| ---------- | ---------------------------------- | ------------------------------------ |
| setZone    | (offset: Number): Void             | Definir fuso em milissegundos        |
| getZone    | (): Number                         | Obter diferença de fuso atual        |
| now        | (): Epoch                          | Obtenha o UTC atual em Epoch         |
| format     | (date: Text, fmt: Text = ø): Epoch | Converter data para Epoch            |
| parse      | (date: Text, fmt: Text = ø): Epoch | Converter Epoch em formato de data   |
| wait       | (ms: Number): Void                 | Aguardar milissegundos (suspender)   |
| getElapsed | (since: Epoch): Text               | Retorna o tempo decorrido como texto |

## Notas de uso

### Epoch

No FatScript, o tempo é representado como um tipo aritmético para que você possa fazer contas.

Você pode obter o tempo decorrido entre tempo1 e tempo2 como:

```
decorrido = tempo2 - tempo1
```

Você também pode verificar se `tempo2` acontece após `tempo1`, simplesmente assim:

```
tempo2 > tempo1
```

### format

Formata a data em texto como "%Y-%m-%d %H:%M:%S.milliseconds" (padrão), quando `fmt` é omitido.

> milissegundos só podem ser transformados no formato padrão, caso contrário, a precisão é de até segundos

### parâmetro fmt

A especificação de formato é um texto contendo uma sequência de caracteres especiais chamada especificações de conversão, cada uma das quais é introduzida por um caractere '%' e terminada por algum outro caractere conhecido como especificador de conversão. Todos os outros caracteres são tratados como texto comum.

| Especificador | Significado                                                                                                                                                                                                                                                                              |
| ------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| %a            | Nome abreviado do dia da semana                                                                                                                                                                                                                                                          |
| %A            | Nome completo do dia da semana                                                                                                                                                                                                                                                           |
| %b            | Nome do mês abreviado                                                                                                                                                                                                                                                                    |
| %B            | Nome completo do mês                                                                                                                                                                                                                                                                     |
| %c            | Data/Hora no formato da localidade                                                                                                                                                                                                                                                       |
| %C            | Número do século [00-99], o ano dividido por 100 e truncado para um número inteiro                                                                                                                                                                                                       |
| %d            | Dia do mês [01-31]                                                                                                                                                                                                                                                                       |
| %D            | Formato de data, igual a %m/%d/%y                                                                                                                                                                                                                                                        |
| %e            | O mesmo que %d, exceto que um único dígito é precedido por um espaço [1-31]                                                                                                                                                                                                              |
| %g            | Parte do ano de 2 dígitos da data da semana ISO [00,99]                                                                                                                                                                                                                                  |
| %F            | Formato de data ISO, igual a %Y-%m-%d                                                                                                                                                                                                                                                    |
| %G            | Parte do ano de 4 dígitos da data da semana ISO                                                                                                                                                                                                                                          |
| %h            | O mesmo que %b                                                                                                                                                                                                                                                                           |
| %H            | Hora no formato de 24 horas [00-23]                                                                                                                                                                                                                                                      |
| %I            | Hora em formato de 12 horas [01-12]                                                                                                                                                                                                                                                      |
| %j            | Dia do ano [001-366]                                                                                                                                                                                                                                                                     |
| %m            | Mês [01-12]                                                                                                                                                                                                                                                                              |
| %M            | Minuto [00-59]                                                                                                                                                                                                                                                                           |
| %n            | Caractere de nova linha                                                                                                                                                                                                                                                                  |
| %p            | Cadeia AM ou PM                                                                                                                                                                                                                                                                          |
| %r            | Hora no formato AM-PM da localidade                                                                                                                                                                                                                                                      |
| %R            | Formato de 24 horas sem segundos, igual a %H:%M                                                                                                                                                                                                                                          |
| %S            | Segundo [00-61], o intervalo de segundos permite um segundo bissexto e um segundo bissexto duplo                                                                                                                                                                                         |
| %t            | Caractere de tabulação                                                                                                                                                                                                                                                                   |
| %T            | Formato de 24 horas com segundos, igual a %H:%M:%S                                                                                                                                                                                                                                       |
| %u            | Dia da semana [1,7], segunda-feira é 1 e domingo é 7                                                                                                                                                                                                                                     |
| %U            | Número da semana do ano [00-53], domingo é o primeiro dia da semana                                                                                                                                                                                                                      |
| %V            | Número da semana ISO do ano [01-53]. Segunda-feira é o primeiro dia da semana. Se a semana contendo 1º de janeiro tiver quatro ou mais dias no ano novo, será considerada a semana 1. Caso contrário, será a última semana do ano anterior e o ano seguinte será a semana 1 do ano novo. |
| %w            | Dia da semana [0,6], domingo é 0                                                                                                                                                                                                                                                         |
| %W            | Número da semana do ano [00-53], segunda-feira é o primeiro dia da semana                                                                                                                                                                                                                |
| %x            | Data no formato da localidade                                                                                                                                                                                                                                                            |
| %X            | Hora no formato da localidade                                                                                                                                                                                                                                                            |
| %y            | Ano de 2 dígitos [00,99]                                                                                                                                                                                                                                                                 |
| %Y            | Ano de 4 dígitos (pode ser negativo)                                                                                                                                                                                                                                                     |
| %z            | String de deslocamento UTC com formato +HHMM ou -HHMM                                                                                                                                                                                                                                    |
| %Z            | Nome do fuso horário                                                                                                                                                                                                                                                                     |
| %%            | Caractere %                                                                                                                                                                                                                                                                              |

Sob o capô `format` usa C's [strftime](https://man7.org/linux/man-pages/man3/strftime.3.html) e `parse` usa C's [strptime](https://man7.org/linux/man-pages/man3/strptime.3.html), mas a tabela de especificação de formato acima se aplica praticamente nos dois sentidos.
