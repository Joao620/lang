{% if output.name != "ebook" %}

# failure

{% endif %}

Tratamento de erros e gerenciamento de exceções

## Importação

```
_ <- fat.failure
```

## Métodos

| Nome     | Assinatura              | Breve descrição                               |
| -------- | ----------------------- | --------------------------------------------- |
| trap     | (): Void                | Aplicar manipulador genérico de erro genérico |
| trapWith | (handler: Method): Void | Definir um manipulador para erros no contexto |
| untrap   | (): Void                | Desarmar o manipulador para erros no contexto |

## Notas de uso

Quando um erro é criado se um manipulador de erro for encontrado, buscando do contexto de execução interno para externo, o manipulador que envolve a falha é invocado automaticamente com esse erro como argumento e o contexto de chamada é encerrado com o valor de retorno do manipulador de erro.

> não é possível definir um manipulador para o escopo global

### trapWith

Este método vincula um manipulador de erros ao contexto do site de chamada, por exemplo quando usado dentro de um método, ele apenas protegerá a lógica executada dentro do corpo desse método.

## Exemplo

Defina um manipulador de erro que imprima o erro e saia:

```
console <- fat.console
system  <- fat.system
sdk     <- fat.sdk

simpleErrorHandler = (error) -> {
  console.log(error)
  sdk.printStack(10)
  system.exit(system.failureCode)
}
```

Finalmente, use o método `trapWith` para atribuir o manipulador de erro:

```
failure <- fat.failure
failure.trapWith(simpleErrorHandler)
```

### Trap it!

Você pode lidar com erros esperados ou deixar passar o inesperado:

```
failure <- fat.failure
_       <- fat.type.Error

MyError = Error

errorHandler = -> _ >> {
  MyError => 0  # handle (expected)
  _       => _  # pass through (unexpected)
}

unsafeMethod = (n) -> {
  failure.trapWith(errorHandler)
  n < 10 ? MyError('arg is less than ten')
  n - 10
}
```

Neste caso, o programa não travará se você chamar `unsafeMethod(5)`, mas se você comentar a linha `trapWith`, verá que ele trava com `MyError`.

## Veja também

- [Error (sintaxe)](../syntax/types/error.md)
- [Extensões do protótipo Error](type/error.md)
- [Controle de fluxo](../syntax/flow.md)
