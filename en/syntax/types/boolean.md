{% if output.name != "ebook" %}

# Boolean

{% endif %}

Booleans are very primitive, they can only be 'true' or 'false'.

## Comparisons

Aside from equality `==` and inequality `!=`, booleans also accept the following operators:

### `&` logical AND

```
true  & true  == true
true  & false == false
false & true  == false
false & false == false
```

> AND short-circuits expression if left-hand side is false

### `|` logical OR

```
true  | true  == true
true  | false == true
false | true  == true
false | false == false
```

> OR short-circuits expression if left-hand side is true

### `%` logical XOR (exclusive OR)

```
true  % true  == false
true  % false == true
false % true  == true
false % false == false
```

> XOR always evaluates both sides of the expression

## Bang operator

`!!` coerces any type into boolean, like so:

- null -> false
- zero (number) -> false
- non-zero (number) -> true
- empty (text/list/scope/chunk) -> false
- non-empty (text/list/scope/chunk) -> true
- method -> true
- error -> false

> logical AND/OR (`&`, `|`) and conditional flows (`=>`, `?`) will implicitly coerce to boolean

## See also

- [Boolean prototype extensions](../../libraries/type/boolean.md)
- [Flow control](../flow.md)
