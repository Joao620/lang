{% if output.name != "ebook" %}

# Chunk

{% endif %}

Chunks are just binary blocks of data.

## Declaration

Chunks cannot be declared explicitly; you must use the [type constructor](../../libraries/type/chunk.md) and apply one of the following strategies:

```
_ <- fat.type.Chunk

Chunk(null)            # Void -> (empty chunk)
Chunk(true)            # Boolean -> '\001'
Chunk(65)              # Number -> 'A'
Chunk('ABC')           # Text -> 'ABC'
Chunk([ 65, 66, 67 ])  # List/Number -> 'ABC'
```

> numbers are expected to be valid byte values (0-255), otherwise an error is raised

## Manipulating Chunks

### Concatenation

In FatScript, you can concatenate, or join, two chunks using the `+` operator. For example:

```
abCombined = chunkA + chunkB
```

### Chunk Selection

Selection allows access to specific parts of a chunk using indices. FatScript supports both positive and negative indices. Positive indices start from the beginning of the chunk (with `0` as the first byte), while negative indices start from the end (`-1` is the last byte).

> for detailed explanation about the indexing system in FatScript, refer to the section on accessing and selecting items in [List](list.md)

Selecting with one index retrieves a single byte from the chunk (as number). Using a range of bytes, selects a fragment inclusive of both start and end indices, except when using the half-open range operator `..<`, which is exclusive on the right-hand side.

Accessing indices outside the valid range will generate an error for individual selections. For range selections, out-of-bounds indices result in an empty chunk.

```
x3 = Chunk('example')
x3(1)     # 120 (ASCII value of 'x')
x3(..2)   # new Chunk containing 3 bytes (corresponding to 'exa')
```

### Comparisons

Chunk equality `==` and inequality `!=` comparisons are supported.

## See also

- [Chunk prototype extensions](../../libraries/type/chunk.md)
