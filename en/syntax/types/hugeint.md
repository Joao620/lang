{% if output.name != "ebook" %}

# HugeInt

{% endif %}

An advanced numerical data type designed to handle very large integers.

## Declaration

The `HugeInt` type supports integers up to 4096 bits. Here's how you can declare a `HugeInt`:

```
h = 0x123456789abcdef  # HugeInt declaration
```

> `HugeInt` is always expressed in hexadecimal format

## Operating HugeInts

`HugeInt` supports a variety of operations, making it versatile for complex calculations:

- `==` equal
- `!=` not equal
- `+` plus
- `-` minus
- `*` multiply
- `/` divide
- `%` modulus
- `**` power
- `<` less
- `<=` less or equal
- `>` more
- `>=` more or equal
- `&` logical AND
- `|` logical OR

### Caveats

In FatScript, `HugeInt` is specifically designed as an unsigned type, and thus it can only represent positive values.

Interactions between `HugeInt` and other numeric types, such as [Number](number.md), are not directly available. To perform such operations, you should convert the value to `HugeInt` using its constructor (available through the prototype extensions).

## Precision

`HugeInt` offers high precision for very large integers, essential in fields like cryptography and large-scale computations. This precision remains consistent across its entire range.

```
prime = 0xfffffffffffffffc90fdA... # a large prime number
```

Contrary to floating-point numbers, `HugeInt` represents discrete integer values, maintaining consistent precision and spacing throughout its range:

```
0________________________________________
|    |    |    |    |    |    |    |    | overflow
```

> the maximum value is 2^4096 - 1, equivalent to a number with 1233 decimal digits or the 0xfff... literal (with 1024 repetitions of the letter f)

`HugeInt` is particularly well-suited for scenarios that demand exact integer arithmetic without rounding errors, especially when dealing with values far beyond the limits of [Number](number.md) type. It is important to ensure that all operations remain within its supported capacity, as exceeding this limit will raise a `ValueError`.

## See also

- [HugeInt prototype extensions](../../libraries/type/hugeint.md)
