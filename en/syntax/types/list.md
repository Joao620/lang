{% if output.name != "ebook" %}

# List

{% endif %}

Lists are ordered collections of items of the same type, accessed by index.

## Definition

Lists are defined with square brackets `[]`, like so:

```
list: List/Text = [ 'apple', 'pizza', 'pear' ]
```

Lists do not allow mixing of types. The type of a list is determined by the first item added to it, consequently, empty lists are untyped.

Lists skip empty positions, so an item that evaluates to `null` is ignored:

```
a = 1
c = 3
[ a, b, c ]  # outputs: [ 1, 3 ] (b is skipped over)
```

## Access

### Individual items

List items can be accessed individually with zero-based index call:

```
list(0)  # 'apple'
list(2)  # 'pear'
```

Negative values will index backwards, starting from -1 as the last item:

```
list(-1)  # 'pear'
```

Accessing items that are out of valid indices will generate an error:

```
         0        1        2       > 2
Error [ 'apple', 'pizza', 'pear' ] Error
 < -3       -3       -2      -1
```

### Selections

Indexes for start and end work exactly the same as when accessing individual items, so negatives count from the last item and can be regressive. However, when using ranges, no errors are generated when accessing out-of-bounds indices; instead, an empty list is returned.

```
list(0..0)   # [ 'apple' ]
list(4..8)   # []
list(1..-1)  # [ 'pizza', 'pear' ]
```

One index can be left blank, and the start from the first or the end at the last item is assumed:

```
list(..1)   # [ 'apple', 'pizza' ]
list(1..)   # [ 'pizza', 'pear' ]
```

#### Deprecation warning

The option to passing a second argument (using a comma, and not dot-dot) to perform a selection is being deprecated starting from version 3.x.x:

```
list(0, 0)   # [ 'apple' ]          # deprecated syntax
list(4, 8)   # []                   # deprecated syntax
list(1, -1)  # [ 'pizza', 'pear' ]  # deprecated syntax
```

## Nested lists

A matrix can be used and accessed like so:

```
matrix = [
  [ 1, 2, 3 ]
  [ 4, 5, 6 ]
]

matrix(1)(0)  # yields 4 (1: second line, then 0: first index)
```

> for simplicity, the example uses a 2D matrix, but could be n-dimensional

## Operations

- `==` equal
- `!=` not equal
- `+` addition (concatenation effect)
- `-` subtraction (difference effect)
- `&` logical AND
- `|` logical OR

> logical AND/OR evaluate empty lists as `false`, otherwise `true`

### List addition (concatenation)

The list addition operation allows you to combine two lists into a new list:

```
x = [1, 2, 2, 3]
y = [3, 3, 4, 4]

x + y  # result: [1, 2, 2, 3, 3, 3, 4, 4]
```

In this case, using the addition operator `+` to merge lists `x` and `y`, the elements from both lists are combined into a single list. The order of the elements in the resulting list is determined by the order in which the lists were added.

> there is no removal of duplicate elements during the concatenation

### Quick-append

For better performance, you can take advantage of `+=` operator, e.g.:

```
~ list += [ value ]  # faster

# same effect as
~ list = []
list = list + [ value ]  # concatenation (slower)
```

Another detail of the `+=` operator, which also applies to other types, is the automatic initialization by omission, where if the entry has not yet been declared previously, it acts as a simple assignment.

### List subtraction (difference)

The list subtraction operation allows you to remove elements from the second operand that are present in the first operand, resulting in a list containing only unique values:

```
x = [ 1, 2, 2, 3 ]
y = [ 3, 3, 4, 4 ]

x - y  # result: [ 1, 2 ]
y - x  # result: [ 4 ]
```

In this case, when we subtract the list `y` from the list `x`, the elements with the value 3 are removed because they are present in both lists. The result is the list `[1, 2]`. Similarly, when we subtract the list `x` from the list `y`, the only remaining element is the value 4.

> only exactly identical values are removed during the subtraction

## See also

- [List prototype extensions](../../libraries/type/list.md)
- [Mapping over a List](../loops.md#mapping-over-a-list)
