{% if output.name != "ebook" %}

# Void

{% endif %}

When you look into the 'Void', only 'null' can be seen.

## Is there anybody out there?

An entry is evaluated to `null` if not defined on current scope.

You can compare with `null` using equality `==` or inequality `!=`, like:

```
a == null  # true, if 'a' is not defined
0 != null  # true, because 0 is a defined value
```

Keep in mind that you can't declare an entry with no value in FatScript.

While you can assign `null` to an entry, it causes different behaviors depending on whether the entry already exists in the scope and whether it's mutable or not:

- If an entry hasn't been declared yet, assigning it `null` has no effect.
- If it already exists and is immutable, assigning `null` raises an error.
- If it already exists and is mutable, assigning `null` removes the entry.

## Delete statement

Assigning `null` to a mutable entry is the same as deleting that entry from the scope. If deleted, nothing is remembered about that entry in the scope, not even it's original type.

```
~ m = 4   # mutable number entry
m = null  # deletes m from scope
```

> null "values" are always mutable, as in fact nothing is stored about them, and therefore they are the only kind of "value" that may transition from a mutable state to an immutable state when "reassigned"

## Comparisons

You can use `Void` to check against the value of an entry also, like:

```
()    == Void  # true
null  == Void  # true
false == Void  # false
0     == Void  # false
''    == Void  # false
[]    == Void  # false
{}    == Void  # false
```

Note that `Void` only accepts `()` and `null`.

## Forms of emptiness

In FatScript, the concept of "emptiness" or the absence of a value can be represented in two ways: using `null` or empty parentheses `()`. They are effectively identical, in terms of behavior in code:

```
null  == null  # true
()    == null  # true
()    == ()    # true
```

### Using null

The `null` keyword explicitly denotes the absence of a value. It is commonly used in scenarios where a parameter or return value might not point to any value.

```
method(null, otherParam)

var = null
```

It can also be used to make a parameter optional, allowing methods to be called with varying numbers of arguments:

```
method = (mandatory: Text, optional: Text = null) -> {
  ...
}
```

> `null` can be used explicitly in any context where an absence of value needs to be represented

### Using empty parentheses

When used in the context of method returns, `()` can signify that the method does not return any meaningful value.

```
fn = -> {
  doSomething

  ()
}
```

Here, `fn` performs some action and then uses `()` to indicate the absence of a meaningful return value, effectively returning void.

The difference lies in code style, so this is just a suggestion, not a hard rule.

> in modern versions of the interpreter, empty parentheses `()` are treated as `null`, ensuring consistent behavior, but, earlier versions required explicitly using `null` to denote the absence of a return value

## See also

- [Void prototype extensions](../../libraries/type/void.md)
