{% if output.name != "ebook" %}

# Any

{% endif %}

A virtual type that encompasses all types and no types at the same time.

## Default type

`Any` is the inferred type and return type when no type is explicitly annotated in a method. For example:

```
identity = _ -> _
```

is equivalent to:

```
identity = (_: Any): Any -> _
```

Using `Any`, be it implicitly or explicitly, disables type checking for a parameter. The explicit annotation can be a useful in cases where you want to make it clear that you are giving flexibility in the accepted type.

Being too liberal with `Any` can make your code less predictable and harder to maintain. It's generally recommended to be more specific with type annotations whenever possible:

```
# Example of using Any that can lead to issues

console <- fat.console

doubleIt = (arg: Any): Void -> console.log(arg * 2)

doubleIt(2)    # prints: '4'
doubleIt('a')  # yields: Error: unsupported expression > Text <multiply> Number
```

This example shows that although the `Any` type annotation allows flexibility in the parameter type, it can also result in unexpected behavior if an argument of an unexpected type is passed in. By being more specific with the type annotation, such as `Number`, you can make your code more predictable and self-evident.

```
# Example of using a specific type annotation for more predictability

console <- fat.console

doubleIt = (num: Number): Void -> console.log(num * 2)

doubleIt(2)    # prints: '4'
doubleIt('a')  # yields: TypeError: type mismatch > num
```

By using `Number` as the type annotation, the `doubleIt` method is now more specific and only accepts arguments of type `Number`.

## Comparisons

The only possible operation with `Any` is comparing to it, but note that `Any` accepts all values indistinctly, so there is no practical use for it:

```
null      == Any  # true
true      == Any  # true
12345     == Any  # true
'abcd'    == Any  # true
[ 1, 2 ]  == Any  # true
{ a = 8 } == Any  # true
```

> comparisons with `Any` can't be used to check for the presence of a value in a scope as even `null` is accepted
