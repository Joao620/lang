{% if output.name != "ebook" %}

# Method

{% endif %}

Methods are recipes that can take arguments to "fill in the blanks".

## Definition

A method is anonymously defined with a thin arrow `->`, like so:

```
<parameters> -> <recipe>
```

Parameters can be omitted if none are needed:

```
-> <recipe>  # arity zero
```

To register a method to the scope, assign it to an identifier:

```
<identifier> = <parameters> -> <recipe>
```

Parameters within a method's execution scope are immutable, ensuring that the method's operations do not alter their original state. For mutable behavior, consider passing a scope or utilizing a [custom type](index.md#custom-types) capable of encapsulating multiple values and states.

## Optional parameters

While method signatures typically require a fixed number of mandatory parameters, FatScript supports optional parameters through default values:

```
greet = (message: Text, name: Text = 'World') -> {
  "Hello, {name}, {message}"
}
```

In this example, the `name` parameter is optional, defaulting to 'World' if no argument is provided. This feature allows for more flexible method invocations.

## Argument handling

Method calls in FatScript are designed to accept more arguments than required; extra arguments are simply ignored. This behavior is part of the language's design to enhance flexibility and performance.

## Auto-return

FatScript uses auto-return, meaning the last standing value is returned:

```
answer: Method = (theGreatQuestion) -> {
  # TODO: explain Life, the Universe and Everything
  42
}

answer("6 x 7 = ?")  # outputs: 42
```

## Automatic calls

FatScript introduces a unique feature that simplifies method calls, when no arguments are involved. This feature is known as the "automatic call trick" and it offers several key benefits:

- **Reduced Boilerplate**: Reduces the need for parentheses, making code cleaner and more concise, for zero-parameter methods that act like properties.

- **Dynamic Computation**: Allows for dynamic computation with outputs that can change based on the object's internal or global state.

- **Deferred Execution**: Enables deferred execution, useful in asynchronous programming and complex initialization patterns.

### Basic usage

In FatScript, a method defined without parameters is executed "automagically" when referenced:

```
foo = {
  bar = -> 'Hello!'
}

# Both lines below output 'Hello!'
foo.bar()  # explicit call
foo.bar    # automatic call
```

### Referencing

To reference a method without triggering the automatic calling feature, you can use the the get syntax:

```
foo('bar')  # yields a reference to foo.bar, without calling it
```

FatScript also offers `self` and `root` keywords to reference methods at the local and global levels, respectively:

```
self('myLocalMethod')
root('myGlobalMethod')
```

### Opting out of automatic calls

The tilde `~` operator allows you to bypass the automatic call feature, providing flexibility in method handling:

```
# Both lines below fetch the method reference, without calling it
foo.~bar
~ myMethod
```

Or you can simply wrap the method call into yet another (anonymous) method:

```
-> foo.bar
```

### Passing methods as arguments

There's an important exception when it comes to passing methods as arguments, specifically in the case of a local method:

```
another(bar)  # passes `bar` as a reference, without executing it
```

> however, this does not apply with chaining: `another(foo.bar)` passes the result of `bar`, not the reference

In this case, to pass the value resulting of the local method `bar`, an explicit call must be made:

```
another(bar())
```

> this behavior might seem counterintuitive, but it is extremely useful in various use cases, such as when passing methods to [reduce](../../libraries/type/list.md#reducing), to an [asynchronous task](../../libraries/async.md), to a [mapping](../loops.md#mapping-syntax) operation etc.

## Implicit argument

A convenience offered by FatScript is the ability to reference a value passed to the method without explicitly specifying a name for it. In this case, the implicit argument is represented by the underscore `_`.

Here's an example that illustrates the use of implicit argument:

```
double = -> _ * 2
double(3)  # output: 6
```

You can use an implicit argument whenever you need to perform a simple operation on a single parameter without assigning a specific name to it, but note that the method must have arity zero to trigger it.

## See also

- [Method prototype extensions](../../libraries/type/method.md)
