{% if output.name != "ebook" %}

# Syntax

{% endif %}

In the following pages, you will find information on the central aspects of writing FatScript code, using both the basic language features as well as the advanced type system and standard libraries features.

## Topics covered

- [Formatting](formatting.md): how to format FatScript code properly

- [Imports](imports.md): how to import libraries into your code

- [Entries](entries.md): understanding the concept of entries and scopes

- [Types](types/index.md): a guide to FatScript type system

- [Flow control](flow.md): controlling the program execution with conditionals

- [Loops](loops.md): making use of ranges, map-over and while loops
