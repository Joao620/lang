{% if output.name != "ebook" %}

# Formatting

{% endif %}

In FatScript, whitespace and indentation are irrelevant, yet they are very welcome to make the code more readable and easier to understand.

## Whitespace

- A newline character (`\n`) indicates the end of an expression, except when:
  - the last token on the line is an operator
  - the first token of the next line is a non-unary operator
  - using parentheses to group expressions
- Expressions can be on the same line if separated by comma (`,`) or semicolon (`;`)

## Comments

Comments start with `#`, and are terminated by a newline:

```
a = 5  # this is a comment
```

### Note

FatScript does not support multiline comments at the moment. Additionally, text literals may end up as a valid return value if left as the last standing line, due to the [auto-return](types/method.md#auto-return) feature. Therefore, it is recommended to stick to the single line comment format.

## See also

- [Source auto-formatter](../general/tooling.md)
