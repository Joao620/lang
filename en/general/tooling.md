{% if output.name != "ebook" %}

# Tooling

{% endif %}

Here are a few hints that can enhance your coding experience with FatScript.

## Static analysis

Use the probe mode to check the syntax and receive hints about your code:

```
fry -p mySweetProgram.fat
```

## Debugger

A breakpoint, indicated by the command `$break`, serves as a debug tool by temporarily halting the program execution at a designated location and loading the built-in debugging console. It provides an interactive environment for examining the current state of the program by inspecting values in scope, evaluating expressions, and tracing program flow.

To activate breakpoints, it is necessary to run the program with interactive mode enabled:

```
fry -i mySweetProgram.fat
```

In FatScript, `$break` returns `null`, which can alter a return value if placed at the end of a block, due to the [auto-return](../syntax/types/method.md#auto-return) feature. Be cautious with `$break` placement to avoid unintended effects on program functionality.

## Source code formatting

### Built-in support

You can apply auto-indentation to your sources using the following command:

```
fry -f mySweetProgram.fat
```

### Visual Studio Code Extension

To add code formatter support to VS Code, you can install the [fatscript-formatter](https://marketplace.visualstudio.com/items?itemName=aprates.fatscript-formatter) extension. Launch VS Code Quick Open (Ctrl+P), paste the following command, and press enter:

```
ext install aprates.fatscript-formatter
```

> `fry` needs to be installed on your system for this extension to work

## Syntax highlighting

### Visual Studio Code Extension

To add FatScript syntax highlighting to VS Code, you can install the [fatscript-syntax](https://marketplace.visualstudio.com/items?itemName=aprates.fatscript-syntax) extension. Launch VS Code Quick Open (Ctrl+P), paste the following command, and press enter:

```
ext install aprates.fatscript-syntax
```

You can also find and install these extensions from the VS Code Extension Marketplace.

### Vim and Neovim Plugin

To install FatScript's syntax highlighting for Vim and Neovim, check out the [vim-syntax](https://gitlab.com/fatscript/vim-syntax) plugin.

For Neovim users, add the respective line to your configuration:

**Using packer.nvim**:

```lua
use { 'https://gitlab.com/fatscript/vim-syntax', as = 'fatscript' }
```

**Using lazy.nvim**:

```lua
{ 'https://gitlab.com/fatscript/vim-syntax', name = 'fatscript' }
```

### Nano Syntax File

To install FatScript's syntax highlighting for `nano`, follow these steps:

1. Download the `fat.nanorc` file from [here](https://gitlab.com/fatscript/fry/-/raw/main/extras/fat.nanorc?inline=false).
2. Copy the `fat.nanorc` file to the `nano` system directory:

```bash
sudo cp fat.nanorc /usr/share/nano/
```

If the syntax highlighting does not get automatically enabled, you may need to explicitly enable it in your `.nanorc` file. Refer to the instructions in the [Arch Linux Wiki](https://wiki.archlinux.org/title/Nano#Syntax_highlighting) for more information.

After installing the syntax highlighting, you can also use the code formatter in `nano` with the following shortcut sequence:

- Ctrl+T Execute; and then...
- Ctrl+O Formatter

## Other tips

### Console file navigation

To navigate your project folders from the terminal, you can try using a console file manager such as [ranger](https://ranger.github.io/), paired with `nano`, `vim` or `nvim`. Set it as the default editor for `ranger` by adding the following line to your `~/.bashrc` file:

```
export EDITOR="nano"
```
