{% if output.name != "ebook" %}

# General overview

{% endif %}

FatScript is a lightweight, interpreted programming language designed for building console-based applications. It emphasizes simplicity, ease of use, and functional programming concepts.

## Free and open-source

`fatscript/fry` is an open-source project that encourages knowledge sharing and collaboration. We welcome developers to [contribute](https://gitlab.com/fatscript/fry/blob/main/CONTRIBUTING.md) to the project and help us improve it over time.

## Key Concepts

- Automatic memory management through garbage collection (GC)
- Symbolic character combinations for a minimalistic syntax
- REPL (Read-Eval-Print Loop) for quick expression testing
- Support for type system, inheritance, and sub-typing via aliases
- Support for immutable programming and passable methods (as values)
- Keep it simple and intuitive, whenever possible

## Contents of this section

- [Setup](setup.md): how to install the FatScript interpreter
- [Options](options.md): how to customize the runtime
- [Bundling](bundling.md): how to pack a FatScript application
- [Tooling](tooling.md): overview of a few extra tools and resources

## Limitations and challenges

While FatScript is designed to be simple and intuitive, it is still a relatively new language and may not be suitable for all use cases. For example, it may underperform compared to more mature programming languages when dealing with complex workloads or high-performance computing tasks.
