{% if output.name != "ebook" %}

# enigma

{% endif %}

Cryptography, hash and UUID methods

## Import

```
_ <- fat.enigma
```

## Methods

| Name    | Signature                        | Brief                       |
| ------- | -------------------------------- | --------------------------- |
| getHash | (msg: Text): Number              | Get 32-bit hash of text     |
| genUUID | (): Text                         | Generate a UUID (version 4) |
| genKey  | (len: Number): Text              | Generate random key         |
| derive  | (secret: Text): Text             | Key derivation function     |
| encrypt | (msg: Text, key: Text = ø): Text | Encrypt msg using key       |
| decrypt | (msg: Text, key: Text = ø): Text | Decrypt msg using key       |

> `derive` is deterministic and uses the Base64 alphabet for a 32 chars output

## Usage Notes

You can omit or pass a blank key `''` for using the default key.

### Heads Up!

Although `enigma` makes encrypted text "non-human-readable", this schema is not cryptographically safe! DO NOT use it alone to protect data!

If paired with a custom key that is not stored alongside the message it may offer some data protection.

### UUID method conformance

A UUID, or Universally Unique Identifier, is a 128-bit number used to identify objects or entities in computer systems. The provided implementation generates random UUIDs as text that follow the format of version 4 RFC 4122 specification, but does not strictly adhere to the required cryptographically secure randomness. In practice, the collision risk has an extremely low probability and is very unlikely to occur, and for most applications can be considered good enough.
