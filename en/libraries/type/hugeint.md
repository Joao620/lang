{% if output.name != "ebook" %}

# HugeInt

{% endif %}

HugeInt prototype extensions

## Import

```
_ <- fat.type.HugeInt
```

## Constructor

| Name    | Signature  | Brief                            |
| ------- | ---------- | -------------------------------- |
| HugeInt | (val: Any) | Number or text parsing to HugInt |

> performs the conversion from text to number assuming hexadecimal representation

## Prototype members

| Name     | Signature                             | Brief                                     |
| -------- | ------------------------------------- | ----------------------------------------- |
| isEmpty  | (): Boolean                           | Return true if zero                       |
| nonEmpty | (): Boolean                           | Return true if non-zero                   |
| size     | (): Number                            | Return number of bits needed to represent |
| toText   | (): Text                              | Return number as hexadecimal text         |
| modExp   | (exp: HugeInt, mod: HugeInt): HugeInt | Return modular exponentiation             |
| toNumber | (): Number                            | Converts to number (with precision loss)  |

### Usage notes

When converting from `Number` type to `HugeInt`, the limit is 2^53, which is the maximum value that can be safely represented as an integer without precision loss. Attempting to pass a value higher than this limit will raise a `ValueError`.

Conversely, when converting from `HugeInt` to `Number`, values up to 2^1023 - 1 can be converted with some degree of precision loss. Attempting to convert a value higher than this will result in `infinity`, which can be checked using the `isInf` method provided by the [math library](../math.md).

> the math library also provides the `maxInt` value, which serves to assess potential precision loss; if a number is less than `maxInt`, its conversion from `HugeInt` is considered safe without precision loss

## See also

- [HugeInt (syntax)](../../syntax/types/hugeint.md)
- [Type package](index.md)
