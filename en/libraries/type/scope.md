{% if output.name != "ebook" %}

# Scope

{% endif %}

Scope prototype extensions

## Import

```
_ <- fat.type.Scope
```

## Constructor

| Name  | Signature  | Brief                 |
| ----- | ---------- | --------------------- |
| Scope | (val: Any) | Wrap val into a scope |

## Prototype members

| Name     | Signature           | Brief                        |
| -------- | ------------------- | ---------------------------- |
| isEmpty  | (): Boolean         | Return true if size is zero  |
| nonEmpty | (): Boolean         | Return true if non-zero size |
| size     | (): Number          | Return number of entries     |
| toText   | (): Text            | Return 'Scope' text literal  |
| copy     | (): Scope           | Return deep copy of scope    |
| keys     | (): List            | Return list of scope keys    |
| maybe    | (key: Text): Option | Return Option wrapped value  |

### Example

```
_ <- fat.type.Scope
x = { num = 12, prop = 'other' }
x.size  # yields 2
```

## See also

- [Scope (syntax)](../../syntax/types/scope.md)
- [Option type](../extra/option.md)
- [Type package](index.md)
