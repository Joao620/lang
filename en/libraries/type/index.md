{% if output.name != "ebook" %}

# types package

{% endif %}

Prototype extensions for [native types](../../syntax/types/index.md#native-types):

- [Void](void.md)
- [Boolean](boolean.md)
- [Number](number.md)
- [HugeInt](hugeint.md)
- [Text](text.md)
- [Method](method.md)
- [List](list.md)
- [Scope](scope.md)
- [Error](error.md)
- [Chunk](chunk.md)

> FatScript **does not** load these definitions automatically into global scope, therefore you have to **explicitly** [import](../../syntax/imports.md) those where needed

## Importing

If you want to make all of them available at once you can simply write:

```
_ <- fat.type._
```

...or import one-by-one, as needed, e.g.:

```
_ <- fat.type.List
```

## Common trait

All types on this package support the following prototype methods:

- apply (constructor)
- isEmpty
- nonEmpty
- size
- toText

## See also

- [Types (syntax)](../../syntax/types/index.md)
