{% if output.name != "ebook" %}

# Method

{% endif %}

Method prototype extensions

## Import

```
_ <- fat.type.Method
```

## Constructor

| Name   | Signature  | Brief                |
| ------ | ---------- | -------------------- |
| Method | (val: Any) | Wrap val in a method |

## Prototype members

| Name     | Signature   | Brief                        |
| -------- | ----------- | ---------------------------- |
| isEmpty  | (): Boolean | Return false, always         |
| nonEmpty | (): Boolean | Return true, always          |
| size     | (): Number  | Return 1, always             |
| toText   | (): Text    | Return 'Method' text literal |
| arity    | (): Number  | Return method arity          |

### Example

```
_ <- fat.type.Method
x = (): Number -> 3
(~ x).toText  # yields 'Method'
```

> note that it is necessary to explicitly [opt out of using automatic calls](../../syntax/types/method.md#opting-out-of-automatic-calls) to make use of the prototype members

## See also

- [Method (syntax)](../../syntax/types/method.md)
- [Type package](index.md)
