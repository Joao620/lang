{% if output.name != "ebook" %}

# Void

{% endif %}

Void prototype extensions

## Import

```
_ <- fat.type.Void
```

## Constructor

| Name | Signature  | Brief                             |
| ---- | ---------- | --------------------------------- |
| Void | (val: Any) | Return null, just ignore argument |

## Prototype members

| Name     | Signature   | Brief                 |
| -------- | ----------- | --------------------- |
| isEmpty  | (): Boolean | Return true, always   |
| nonEmpty | (): Boolean | Return false, always  |
| size     | (): Number  | Return 0, always      |
| toText   | (): Text    | Return 'null' as text |

### Example

```
_ <- fat.type.Void
x.isEmpty  # true, since x has not been declared
```

## See also

- [Void (syntax)](../../syntax/types/void.md)
- [Type package](index.md)
