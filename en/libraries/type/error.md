{% if output.name != "ebook" %}

# Error

{% endif %}

Error prototype extensions

## Import

```
_ <- fat.type.Error
```

## Aliases

- AssignError: assigning a new value to an immutable entry
- AsyncError: asynchronous operation failure
- CallError: a call is made with insufficient arguments
- FileError: file operation failure
- IndexError: index is out of list/text bounds
- KeyError: the key (name) is not found in scope
- SyntaxError: syntax or code structure error
- TypeError: type mismatch on method call, return, or assign
- ValueError: type may be okay, but content is not accepted

## Constructor

| Name  | Signature  | Brief                                       |
| ----- | ---------- | ------------------------------------------- |
| Error | (val: Any) | Return val coerced to text wrapped in error |

## Prototype members

| Name     | Signature   | Brief                 |
| -------- | ----------- | --------------------- |
| isEmpty  | (): Boolean | Return true, always   |
| nonEmpty | (): Boolean | Return false, always  |
| size     | (): Number  | Return 0, always      |
| toText   | (): Text    | Return error text val |

### Example

```
_ <- fat.type.Error
x = Error('ops')
x.toText  # yields "Error: ops"

# ...or something unexpected
e = undeclared.item  # raises Error
e.toText             # yields "can't resolve scope of 'item'"
```

## See also

- [Failure library](../failure.md)
- [Error (syntax)](../../syntax/types/error.md)
- [Type package](index.md)
