{% if output.name != "ebook" %}

# Boolean

{% endif %}

Boolean prototype extensions

## Import

```
_ <- fat.type.Boolean
```

## Constructor

| Name    | Signature  | Brief                    |
| ------- | ---------- | ------------------------ |
| Boolean | (val: Any) | Coerces value to boolean |

## Prototype members

| Name     | Signature   | Brief                            |
| -------- | ----------- | -------------------------------- |
| isEmpty  | (): Boolean | Return true if false             |
| nonEmpty | (): Boolean | Return false if true             |
| size     | (): Number  | Return 1 if true, 0 if false     |
| toText   | (): Text    | Return 'true' or 'false' as text |

### Examples

```
_ <- fat.type.Boolean

x = true
x.isEmpty  # false, since x is true

Boolean('false')  # yields true, because text is non-empty
Boolean('')       # yields false, because text is empty
```

> note that the constructor does not attempt to convert value from text, which is consistent with flow control evaluations, and you can use a simple [case](../../syntax/flow.md#cases) if you need to make conversion from text to boolean

## See also

- [Boolean (syntax)](../../syntax/types/boolean.md)
- [Type package](index.md)
