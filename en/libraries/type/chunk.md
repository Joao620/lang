{% if output.name != "ebook" %}

# Chunk

{% endif %}

Chunk prototype extensions

## Import

```
_ <- fat.type.Chunk
```

## Constructor

| Name  | Signature  | Brief                           |
| ----- | ---------- | ------------------------------- |
| Chunk | (val: Any) | Coerces value to chunk (binary) |

## Prototype members

| Name     | Signature                                  | Brief                              |
| -------- | ------------------------------------------ | ---------------------------------- |
| isEmpty  | (): Boolean                                | Returns true if size is zero       |
| nonEmpty | (): Boolean                                | Returns true if non-zero size      |
| size     | (): Number                                 | Returns chunk size (in bytes)      |
| toText   | (): Text                                   | Converts chunk to text format      |
| toBytes  | (): List/Number                            | Converts chunk to bytes list       |
| seekByte | (byte: Number, offset: Number = 0): Number | Returns index of first match       |
| patch    | (i, n, val: Text): Text                    | Inserts val at i, removing n bytes |

> `toText` replaces any invalid UTF-8 sequences with U+FFFD, represented as � in UTF-8

### Example

```
_ <- fat.type.Chunk

x = Chunk('example')

x.size     # yields 7
x.toText   # yields 'example'
x.toBytes  # yields [ 101, 120, 97, 109, 112, 108, 101 ]
```

## See also

- [Chunk (syntax)](../../syntax/types/chunk.md)
- [Type package](index.md)
