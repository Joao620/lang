{% if output.name != "ebook" %}

# Libraries

{% endif %}

Let's talk about the sweet fillings baked into FatScript: the libraries!

## Standard libraries

### Essentials

These are the fundamental libraries you would expect to be available in a programming language, providing essential functionality:

- [async](async.md) - Asynchronous workers and tasks
- [color](color.md) - ANSI color codes for console
- [console](console.md) - Console input and output operations
- [curses](curses.md) - Terminal-based user interface
- [enigma](enigma.md) - Cryptography, hash and UUID methods
- [failure](failure.md) - Error handling and exception management
- [file](file.md) - File input and output operations
- [http](http.md) - HTTP handling framework
- [math](math.md) - Mathematical operations and functions
- [recode](recode.md) - Data conversion between various formats
- [sdk](sdk.md) - Fry's software development kit utilities
- [system](system.md) - System-level operations and information
- [time](time.md) - Time and date manipulation

### Type Package

[This package](type/index.md) extends the features of FatScript's [native types](../syntax/types/index.md#native-types):

- [Void](type/void.md)
- [Boolean](type/boolean.md)
- [Number](type/number.md)
- [HugeInt](type/hugeint.md)
- [Text](type/text.md)
- [Method](type/method.md)
- [List](type/list.md)
- [Scope](type/scope.md)
- [Error](type/error.md)
- [Chunk](type/chunk.md)

### Extra package

[Additional types](extra/index.md) implemented in vanilla FatScript:

- [Date](extra/date.md) - Calendar and date handling
- [Duration](extra/duration.md) - Millisecond duration builder
- [HashMap](extra/hmap.md) - Quick key-value store
- [Logger](extra/logger.md) - Logging support
- [Memo](extra/memo.md) - Generic memoization utility
- [Option](extra/option.md) - Encapsulation of optional value
- [Param](extra/param.md) - Parameter presence and type verification
- [Sound](extra/sound.md) - Sound playback interface
- [Storable](extra/storable.md) - Data store facilities

## Import-all shorthand

If you want to make all of them available at once, you can simply do the following, and all that good stuff will be available to your code:

```
_ <- fat._
```

While this feature can be convenient when experimenting on the REPL, be aware that it brings in all the library's constants and method names, potentially polluting your global namespace.

### fat.std

Alternatively, import the "standard" library, which imports all types (including those from the extra package), as well as named imports from all other packages, like this:

```
_ <- fat.std
```

This is equivalent to:

```
_       <- fat.type._
_       <- fat.extra._
async   <- fat.async
color   <- fat.color
console <- fat.console
curses  <- fat.curses
enigma  <- fat.enigma
failure <- fat.failure
http    <- fat.http
file    <- fat.file
math    <- fat.math
recode  <- fat.recode
sdk     <- fat.sdk
system  <- fat.system
time    <- fat.time
```

Note that importing everything in advance can add unnecessary overhead to the startup time of your program, even if you only need to use a few methods.

As a best practice, consider importing only the specific modules you need, with [named imports](../syntax/imports.md#named-import). This way, you can keep your code clean and concise, while minimizing the risk of naming conflicts or performance issues.

## Hacking and more

Under the hood, libraries are built using embedded commands. To gain a deeper understanding and explore the inner workings of the interpreter, dive into [this more advanced topic](embedded.md).
