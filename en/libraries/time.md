{% if output.name != "ebook" %}

# time

{% endif %}

Time and date manipulation

## Import

```
_ <- fat.time
```

> [number type](type/number.md) is automatically imported with this import

## Methods

| Name       | Signature                          | Brief                         |
| ---------- | ---------------------------------- | ----------------------------- |
| setZone    | (offset: Number): Void             | Set timezone in milliseconds  |
| getZone    | (): Number                         | Get current timezone offset   |
| now        | (): Epoch                          | Get current UTC in Epoch      |
| format     | (date: Text, fmt: Text = ø): Epoch | Convert Epoch to date format  |
| parse      | (date: Text, fmt: Text = ø): Epoch | Parse date to Epoch           |
| wait       | (ms: Number): Void                 | Wait for milliseconds (sleep) |
| getElapsed | (since: Epoch): Text               | Return elapsed time as text   |

## Usage Notes

### Epoch

In FatScript time is represented as an arithmetic type so that you can do maths.

You can get the elapsed time between `time1` and `time2` like:

```
elapsed = time2 - time1
```

You can also check if `time2` happens after `time1`, simply like:

```
time2 > time1
```

### format

Formats text date as "%Y-%m-%d %H:%M:%S.milliseconds" (default), when `fmt` is omitted.

> milliseconds can only be transformed in default format, otherwise the precision is up to seconds

### fmt parameter

The format specification is a text containing a special character sequence called conversion specifications, each of which is introduced by a '%' character and terminated by some other character known as a conversion specifier. All other characters are treated as ordinary text.

| Specifier | Meaning                                                                                                                                                                                                                                                                            |
| --------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| %a        | Abbreviated weekday name                                                                                                                                                                                                                                                           |
| %A        | Full weekday name                                                                                                                                                                                                                                                                  |
| %b        | Abbreviated month name                                                                                                                                                                                                                                                             |
| %B        | Full month name                                                                                                                                                                                                                                                                    |
| %c        | Date/Time in the format of the locale                                                                                                                                                                                                                                              |
| %C        | Century number [00-99], the year divided by 100 and truncated to an integer                                                                                                                                                                                                        |
| %d        | Day of the month [01-31]                                                                                                                                                                                                                                                           |
| %D        | Date Format, same as %m/%d/%y                                                                                                                                                                                                                                                      |
| %e        | Same as %d, except single digit is preceded by a space [1-31]                                                                                                                                                                                                                      |
| %g        | 2 digit year portion of ISO week date [00,99]                                                                                                                                                                                                                                      |
| %F        | ISO Date Format, same as %Y-%m-%d                                                                                                                                                                                                                                                  |
| %G        | 4 digit year portion of ISO week date                                                                                                                                                                                                                                              |
| %h        | Same as %b                                                                                                                                                                                                                                                                         |
| %H        | Hour in 24-hour format [00-23]                                                                                                                                                                                                                                                     |
| %I        | Hour in 12-hour format [01-12]                                                                                                                                                                                                                                                     |
| %j        | Day of the year [001-366]                                                                                                                                                                                                                                                          |
| %m        | Month [01-12]                                                                                                                                                                                                                                                                      |
| %M        | Minute [00-59]                                                                                                                                                                                                                                                                     |
| %n        | Newline character                                                                                                                                                                                                                                                                  |
| %p        | AM or PM string                                                                                                                                                                                                                                                                    |
| %r        | Time in AM/PM format of the locale                                                                                                                                                                                                                                                 |
| %R        | 24-hour time format without seconds, same as %H:%M                                                                                                                                                                                                                                 |
| %S        | Second [00-61], the range for seconds allows for a leap second and a double leap second                                                                                                                                                                                            |
| %t        | Tab character                                                                                                                                                                                                                                                                      |
| %T        | 24-hour time format with seconds, same as %H:%M:%S                                                                                                                                                                                                                                 |
| %u        | Weekday [1,7], Monday is 1 and Sunday is 7                                                                                                                                                                                                                                         |
| %U        | Week number of the year [00-53], Sunday is the first day of the week                                                                                                                                                                                                               |
| %V        | ISO week number of the year [01-53]. Monday is the first day of the week. If the week containing January 1st has four or more days in the new year then it is considered week 1. Otherwise, it is the last week of the previous year, and the next year is week 1 of the new year. |
| %w        | Weekday [0,6], Sunday is 0                                                                                                                                                                                                                                                         |
| %W        | Week number of the year [00-53], Monday is the first day of the week                                                                                                                                                                                                               |
| %x        | Date in the format of the locale                                                                                                                                                                                                                                                   |
| %X        | Time in the format of the locale                                                                                                                                                                                                                                                   |
| %y        | 2 digit year [00,99]                                                                                                                                                                                                                                                               |
| %Y        | 4-digit year (can be negative)                                                                                                                                                                                                                                                     |
| %z        | UTC offset string with format +HHMM or -HHMM                                                                                                                                                                                                                                       |
| %Z        | Time zone name                                                                                                                                                                                                                                                                     |
| %%        | % character                                                                                                                                                                                                                                                                        |

Under the hood `format` uses C's [strftime](https://man7.org/linux/man-pages/man3/strftime.3.html) and `parse` uses C's [strptime](https://man7.org/linux/man-pages/man3/strptime.3.html), but the above format specification table applies pretty much both ways.
