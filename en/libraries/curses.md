{% if output.name != "ebook" %}

# curses

{% endif %}

Terminal-based user interface

> although the inspiration is acknowledged, FatScript has it's own way of approaching terminal UI which differs in many ways from the original curses library

## Import

```
_ <- fat.curses
```

## Methods

| Name      | Signature                                         | Brief                         |
| --------- | ------------------------------------------------- | ----------------------------- |
| box       | (p1: Scope, p2: Scope): Void                      | Draw square from pos1 to pos2 |
| fill      | (p1: Scope, p2: Scope, p: Text = ' '): Void       | Fill from pos1 to pos2 with p |
| clear     | (): Void                                          | Clear screen buffer           |
| refresh   | (): Void                                          | Render screen buffer          |
| getMax    | (): Scope                                         | Return screen size as x, y    |
| printAt   | (pos: Scope, msg: Any, width: Number = ø): Void   | Print msg at { x, y } pos     |
| makePair  | (fg: Number = ø, bg: Number = ø): Number          | Create a color pair           |
| usePair   | (pair: Number): Void                              | Apply color pair              |
| frameTo   | (cols: Number, rows: Number)                      | Align view to screen center   |
| readKey   | (): Text                                          | Return key pressed            |
| readText  | (pos: Scope, width: Number, prev: Text = ø): Text | Start a text box input        |
| flushKeys | (): Void                                          | Flush input buffer            |
| endCurses | (): Void                                          | Exit curses mode              |

> positions (pos) are of form { x: Number, y: Number }

> the methods in this library **do not ensure** thread safety in asynchronous scenarios, use either the main thread **or** a single [worker](async.md) to render console updates

## Usage Notes

Any method of this library, except `getMax` and `endCurses`, will start curses mode if not yet started. Note that methods such as `log`, `stderr` and `input` from [console](console.md) library will implicitly call `endCurses`. However, `moveTo`, `print` and `flush` will not change the output mode, and can be paired with curses methods, which can be useful in some circumstances.

The letters `x` and `y` stand for column and row respectively when calling `printAt`, where (0, 0) is the upper-left corner and the result of `getMax` is the just the first coordinate outside the lower-right corner.

> special characters on curses only work if a UTF-8 [locale](system.md) can be set

### makePair

You can import the [color](color.md) library to use color names and create a combination of foreground and background (pair). Pass `null` to apply the default color to the desired parameter.

### usePair

The input of this method should be a color pair created with `makePair` method. It leaves this pair enabled until you call it again with a different pair.

### readKey

This method is non-blocking and returns `null` if `stdin` is empty, otherwise it will return one character at a time.

Special keys may be detected and return keywords such as:

- arrow keys:
  - up
  - down
  - left
  - right
- edit keys:
  - delete
  - backspace
  - enter
  - space
  - tab
  - backTab (shift+tab)
- control keys:
  - pageUp
  - pageDown
  - home
  - end
  - insert
  - esc
- other:
  - resize (terminal window was resized)

> the correct detection of keys can depend on the context or platform

### readText

Enters text capture mode using an area demarcated by position and width of the text box. If the text is larger than the space, an automatic text scroll is performed. The full text is returned when `enter` or `tab` is pressed, however, if `esc` is pressed, `null` is returned.

## See also

- [Color library](color.md)
- [Console library](console.md)
