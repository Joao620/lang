{% if output.name != "ebook" %}

# Param

{% endif %}

Parameter presence and type verification

## Import

```
_ <- fat.Param
```

> [Text type](../type/text.md) and [Error type](../type/error.md) are automatically imported with this import

## Types

This library introduces the `Param` type and the `Using` utility for implicit parameter declaration.

## Constructors

Both `Param` and `Using` constructors take two arguments:

- **\_exp**: the parameter name to check in context.
- **\_typ**: the expected type of the evaluated value.

### Param

The `Param` type provides mechanisms for checking the presence and type of parameters in the execution context.

#### Prototype members

| Name | Signature | Brief                                          |
| ---- | --------- | ---------------------------------------------- |
| get  | (): Any   | Retrieves the parameter if it matches the type |

> the `get` method throws `KeyError` if the parameter is not defined, and `TypeError` if the type does not match

#### Example

```
_ <- fat.extra.Param

currentUser = Param('userId', 'Text')

...

# Assuming userId is defined in the context and is a text,
# safely retrieve it's value from the current namespace
userId = currentUser.get
```

### Using

Apply `Using` to suppress implicit parameter hints on method declarations for entries expected to be in scope.

> alternatively, to suppress warnings about implicit parameters, name the implicit entry starting with an underscore (`_`)

#### Example

```
_ <- fat.extra.Param

printUserIdFromContext = -> {
  Using('userId', 'Text')
  console.log(userId)
}
```

> if the implicit parameter is missing or mismatched, an error will be raised at runtime when the method is called

## See also

- [Extra package](index.md)
