{% if output.name != "ebook" %}

# extra package

{% endif %}

Additional types implemented in vanilla FatScript:

- [Date](date.md) - Calendar and date handling
- [Duration](duration.md) - Millisecond duration builder
- [HashMap](hmap.md) - Quick key-value store
- [Logger](logger.md) - Logging support
- [Memo](memo.md) - Generic memoization utility
- [Option](option.md) - Encapsulation of optional value
- [Param](param.md) - Parameter presence and type verification
- [Sound](sound.md) - Sound playback interface
- [Storable](storable.md) - Data store facilities

## Importing

If you want to make all of them available at once you can simply write:

```
_ <- fat.extra._
```

...or import one-by-one, as needed, e.g.:

```
_ <- fat.Date
```

## Developer note

Currently most of these utilities are not resource or performance optimized.

The intent here was more of providing simple features, as basic templates that can be pulled out via [readLib](../sdk.md), so any developer with particular requirements will have a starting point for their own implementations.
