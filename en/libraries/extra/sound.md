{% if output.name != "ebook" %}

# Sound

{% endif %}

Sound playback interface

Wrapper for command-line audio players using [fork and kill](../system.md).

## Import

```
_ <- fat.extra.Sound
```

## Constructor

The `Sound` constructor takes three arguments:

- **path**: the filepath of your audio file.
- **duration** (optional): the cool off time (in milliseconds) to accept to play again the file, usually you want to set this to the exact duration of your audio.
- **player** (optional): the default player used is `aplay` (common Linux audio utility, only supports wav files), but you could use `ffplay` to play mp3, for example, defining `ffplay = [ 'ffplay', '-nodisp', '-autoexit', '-loglevel', 'quiet' ]`, then providing it as argument for your sound instance. In this case the package `ffmpeg` needs to be installed on the system.

## Prototype members

| Name | Signature | Brief                                |
| ---- | --------- | ------------------------------------ |
| play | (): Void  | Start player, if not already playing |
| stop | (): Void  | Stop player, if still playing        |

> state of "still playing" is inferred from the duration parameter

### Example

```
_    <- fat.extra.Sound
time <- fat.time

applause = Sound('applause.wav', 5000);
applause.play
time.wait(5000)
```

> note that Sound spawns a child process to play the audio, so it is asynchronous

### Sound in Web Build

When using `fry` built with Emscripten (for example, when using FatScript Playground), this prototype uses embedded commands `$soundPlay` and `$soundStop`, which are only defined in the web build. Therefore, instead of utilizing a CLI audio player through process forking, there is audio support via SDL2/WebAudio.

## See also

- [Extra package](index.md)
