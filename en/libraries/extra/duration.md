{% if output.name != "ebook" %}

# Duration

{% endif %}

Millisecond duration builder

In FatScript time is natively expressed in milliseconds, and this type provides a simple way to express different time magnitudes effortlessly into Millis.

## Import

```
_ <- fat.extra.Duration
```

## Constructor

| Name     | Signature      | Brief                              |
| -------- | -------------- | ---------------------------------- |
| Duration | (val: Number ) | Create a Millis duration converter |

## Prototype members

| Name    | Signature  | Brief                              |
| ------- | ---------- | ---------------------------------- |
| nanos   | (): Millis | Interpret value as nanoseconds     |
| micros  | (): Millis | Interpret value as microseconds    |
| millis  | (): Millis | Interpret value as milliseconds    |
| seconds | (): Millis | Interpret value as seconds         |
| minutes | (): Millis | Interpret value as minutes         |
| days    | (): Millis | Interpret value as days            |
| weeks   | (): Millis | Interpret value as weeks           |
| months  | (): Millis | Interpret value as months (aprox.) |
| years   | (): Millis | Interpret value as years (aprox.)  |

### Example

```
_    <- fat.extra.Duration
time <- fat.time

fiveSeconds = Duration(5).seconds
time.wait(fiveSeconds)  # sleeps thread for 5 seconds
```
