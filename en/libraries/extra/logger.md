{% if output.name != "ebook" %}

# Logger

{% endif %}

Logging support

> from simple console logging to file-based logging

## Import

```
_ <- fat.extra.Logger
```

> [console library](../console.md), [color library](../color.md), [file library](../file.md), [time library](../time.md), [sdk library](../sdk.md), and [type library](../type/index.md) are automatically imported with this import

## Logger Type

`Logger` provides customizable logging capabilities with various levels and formats.

### Properties

- `level`: Text (default 'debug') - Logging level
- `showTime`: Boolean (default true) - Flag to display timestamps

> valid levels: 'debug', 'info', 'warn', 'error'

### Prototype members

| Name        | Signature                        | Brief                                    |
| ----------- | -------------------------------- | ---------------------------------------- |
| setLevel    | (level: Text)                    | Sets the logging level                   |
| setShowTime | (showTime: Boolean)              | Toggles timestamp display in logs        |
| asMessage   | (level: Text, args: Scope): Text | Formats log messages (can be overridden) |
| log         | (msg: Any, fg: Number)           | Outputs messages (can be overridden)     |

### Logging methods

- `debug(_1, _2, _3, _4, _5)`: Logs a debug message
- `info(_1, _2, _3, _4, _5)`: Logs an info message
- `warn(_1, _2, _3, _4, _5)`: Logs a warning message
- `error(_1, _2, _3, _4, _5)`: Logs an error message

## Subtypes

### BoringLogger

- Inherits from `Logger`
- Overrides `log` to output plain text without color

### FileLogger

- Inherits from `Logger`
- Additional Properties:
  - `logfile`: Text (default 'log.txt') - file for logging
- Overrides `log` to append messages to a file

## Usage Example

```
_ <- fat.extra.Logger

# Create an instance with custom settings
myLogger = Logger(level = 'info', showTime = false)

# Log an information message
myLogger.info('This is an informational message.')

# Create a FileLogger to log messages to a file
fileLogger = FileLogger('myLog.txt')
fileLogger.info('Logged to file.')
```
