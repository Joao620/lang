{% if output.name != "ebook" %}

# Memo

{% endif %}

Generic memoization utility
(can also create lazy values)

## Import

```
_ <- fat.extra.Memo
```

## Constructor

| Name | Signature        | Brief                               |
| ---- | ---------------- | ----------------------------------- |
| Memo | (method: Method) | Create a Memo instance for a method |

> the arity of the memoized method should be 1 or else 0 (for lazy values)

## Prototype members

| Name     | Signature       | Brief                                        |
| -------- | --------------- | -------------------------------------------- |
| asMethod | (): Method      | Return a memoized version of original method |
| call     | (arg: Any): Any | Memoized call; cache and return results      |

### Example

Memo is useful for optimizing functions by caching results. It stores the outcome of function calls and returns the cached result when the same inputs occur again.

```
_ <- fat.extra.Memo

fib = (n: Number) -> {
  n <= 2 => 1
  _      => quickFib(n - 1) + quickFib(n - 2)
}

quickFib = Memo(fib).asMethod

quickFib(50)  # 12586269025
```

You can now call `quickFib` as if you were calling `fib`, but with cached results for previously computed inputs.

> caveat: may cause memory allocation build-up
