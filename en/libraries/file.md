{% if output.name != "ebook" %}

# file

{% endif %}

File input and output operations

## Import

```
_ <- fat.file
```

## Type contributions

| Name     | Signature                    | Brief         |
| -------- | ---------------------------- | ------------- |
| FileInfo | (modTime: Epoch, size: Text) | File metadata |

## Methods

| Name     | Signature                   | Brief                                |
| -------- | --------------------------- | ------------------------------------ |
| basePath | (): Text                    | Extract path where app was called    |
| exists   | (path: Text): Boolean       | Check file exists on provided path   |
| read     | (path: Text): Text          | Read file from path (text mode)      |
| readBin  | (path: Text): Chunk         | Read file from path (binary mode)    |
| write    | (path: Text, src): Boolean  | Write src to file and return success |
| append   | (path: Text, src): Boolean  | Append to file and return success    |
| remove   | (path: Text): Boolean       | Remove file and return success       |
| isDir    | (path: Text): Boolean       | Check if path is a directory         |
| mkDir    | (path: Text, safe: Boolean) | Create a directory                   |
| lsDir    | (path: Text): List          | Get list of files in a directory     |
| stat     | (path: Text): FileInfo      | Get file metadata                    |

## Usage Notes

### read

On exception:

- logs error to `stderr`
- returns `null`

> `read` cannot see builtin "files", but `readLib` from [SDK lib](sdk.md) can

### write/append

Exceptions:

- logs error to `stderr`
- returns `false`

### mkDir

If `safe` is set to `true`, the directory gets 0700 permission instead of default 0755, which is less protected.

## See also

- [Recode library](recode.md)
