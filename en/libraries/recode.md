{% if output.name != "ebook" %}

# recode

{% endif %}

Data conversion between various formats

## Import

```
_ <- fat.recode
```

> [type package](type/index.md) is automatically imported with this import

## Constants

- numeric, regex definition used by `inferType` (DEPRECATED)

> the constant `numeric` is redundant and will be removed in version `3.x.x`

## Variables

These settings can be adjusted to configure the behavior of the processing functions:

- csvSeparator, default is `,` (comma)
- csvReplacement, default is empty (just removes commas from text)
- xmlWarnings, default is `true` - set to `false` to suppress XML warnings (DEPRECATED)

## Base64 functions

| Name       | Signature           | Brief                                 |
| ---------- | ------------------- | ------------------------------------- |
| toBase64   | (data: Chunk): Text | Encode binary chunk to base64 text    |
| fromBase64 | (b64: Text): Chunk  | Decode base64 text to original format |

## JSON functions

| Name     | Signature         | Brief                         |
| -------- | ----------------- | ----------------------------- |
| toJSON   | (\_: Any): Text   | Encode JSON from native types |
| fromJSON | (json: Text): Any | Decode JSON to native types   |

## URL functions

| Name         | Signature           | Brief                                      |
| ------------ | ------------------- | ------------------------------------------ |
| toURL        | (text: Text): Text  | Encode text to URL escaped text            |
| fromURL      | (url: Text): Text   | Decode URL escaped text to original format |
| toFormData   | (data: Scope): Text | Encode URL encoded Form Data from scope    |
| fromFormData | (data: Text): Scope | Decode URL encoded Form Data to scope      |

## CSV functions

| Name    | Signature                                   | Brief                |
| ------- | ------------------------------------------- | -------------------- |
| toCSV   | (header: List/Text, rows: List/Scope): Text | Encode CSV from rows |
| fromCSV | (csv: Text): List/Scope                     | Decode CSV into rows |

> `csvReplacement` is used by `toCSV` as replacement in case a `csvSeparator` is found within a text being encoded

## XML functions (DEPRECATED)

XML attributes and self-closing tags are not supported.

| Name    | Signature         | Brief                        |
| ------- | ----------------- | ---------------------------- |
| toXML   | (node: Any): Text | Encode XML from native types |
| fromXML | (text: Text): Any | Decode XML into native types |

> XML functions will be removed from FatScript standard libraries in version `3.x.x`, use [XMLoaf](https://gitlab.com/aprates/xmloaf)

## RLE functions

| Name    | Signature             | Brief                      |
| ------- | --------------------- | -------------------------- |
| toRLE   | (chunk: Chunk): Chunk | Compress to RLE schema     |
| fromRLE | (chunk: Chunk): Chunk | Decompress from RLE schema |

## Other functions

| Name      | Signature         | Brief                               |
| --------- | ----------------- | ----------------------------------- |
| inferType | (val: Text): Any  | Convert text to void/boolean/number |
| minify    | (src: Text): Text | Minifies FatScript source code      |

> `minify` will replace any `$break` statements (debugger breakpoint) with `()`

## Usage

### JSON

Since FatScript alternatively accepts [JSON-like syntax](../syntax/entries.md#json-like-syntax), `fromJSON` actually uses FatScript internal parser, which is blazing fast, but may or not yield exactly what one is expecting from a JSON parser.

For example, once the bellow fragment is parsed, since `null` in FatScript is absence of value, there would be no entry declarations for "prop":

```
"prop": null
```

Therefore, reading with `fromJSON` and writing back with `toJSON` is not necessarily an idempotent operation.

## See also

- [Type package](type/index.md)
- [SDK library](sdk.md)
