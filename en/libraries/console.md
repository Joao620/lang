{% if output.name != "ebook" %}

# console

{% endif %}

Console input and output operations

## Import

```
_ <- fat.console
```

## Methods

| Name         | Signature                                        | Brief                                   |
| ------------ | ------------------------------------------------ | --------------------------------------- |
| log          | (msg: Any, fg: Number = ø, bg: Number = ø): Void | Print msg to stdout, with newline       |
| print        | (msg: Any, fg: Number = ø, bg: Number = ø): Void | Print msg to stdout, without newline    |
| stderr       | (msg: Any, fg: Number = ø, bg: Number = ø): Void | Print msg to stderr, with newline       |
| input        | (msg: Any, mode: Text = ø): Text                 | Print msg and return input of stdin     |
| flush        | (): Void                                         | Flush stdout buffer                     |
| cls          | (): Void                                         | Clear stdout using ANSI escape codes    |
| moveTo       | (x: Number, y: Number): Void                     | Move cursor using ANSI escape codes     |
| isTTY        | (): Boolean                                      | Check if stdout a terminal device       |
| isTty        | (): Boolean                                      | DEPRECATED (will be removed in `3.x.x`) |
| showProgress | (label: Text, fraction: Number): Void            | Render progress bar, fraction 0 to 1    |

> the methods `log`, `stderr` and `input` ensure thread safety in asynchronous scenarios

## Usage Notes

### output

By default, `stdout` and `stderr` both print to the console. The foreground color (fg) and background color (bg) parameters are optional.

> colors are automatically suppressed if the output buffer is not a TTY

### input

The optional `mode` parameter accepts the following values:

- 'plain', plain input (no readline cursor, no history)
- 'quiet', like plain mode, but without feedback
- 'secret', special mode for password input
- `null` (default), with readline and input history

## See also

- [Color library](color.md)
- [Curses library](curses.md)
