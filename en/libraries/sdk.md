{% if output.name != "ebook" %}

# sdk

{% endif %}

Fry's software development kit utilities

> a special library that exposes some of the inner elements of fry interpreter

## Import

```
_ <- fat.sdk
```

## Methods

| Name       | Signature             | Brief                                  |
| ---------- | --------------------- | -------------------------------------- |
| ast        | (\_): Void            | Print abstract syntax tree of node     |
| stringify  | (\_): Text            | Converts node to json text             |
| eval       | (\_): Any             | Interprets text as FatScript program   |
| getVersion | (): Text              | Return fry version                     |
| printStack | (depth: Number): Void | Print execution context stack trace    |
| readLib    | (ref: Text): Text     | Return fry library source code         |
| typeOf     | (\_): Text            | Return type name of node               |
| getTypes   | (): List              | Return info about declared types       |
| getDef     | (name: Text): Any     | Return type definition by name         |
| getMeta    | (): Scope             | Return interpreter's metadata          |
| setKey     | (key: Text): Void     | Set key for obfuscated bundles         |
| setMem     | (n: Number): Void     | Set memory limit (node count)          |
| runGC      | (): Number            | Run GC, return elapsed in milliseconds |
| quickGC    | (): Number            | Run single GC iteration and return ms  |
| setAutoGC  | (n: Number): Void     | Set GC to run every n new nodes        |

## Usage notes

## readLib

```
_ <- fat.sdk
_ <- fat.console

print(readLib('fat.extra.Date'))  # prints the Date library implementation
```

> `readLib` cannot see external files, but `read` from [file lib](file.md) can

### setKey

Use preferably on `.fryrc` file like so:

```
_ <- fat.sdk
setKey('secret')  # will encode and decode bundles with this key
```

See more about [obfuscating](../general/bundling.md#obfuscating).

### setMem

Use preferably on `.fryrc` file like so:

```
_ <- fat.sdk
setMem(5000)  # ~2mb
```

### Choosing between full, quick and auto GC

Most simple scripts in FatScript won't need to worry about memory management, as the default settings are designed to provide developers with a reasonably large memory capacity and sensible automatic behavior right out of the box.

The `quickGC` method offers swift, less exhaustive cleanup, making it suitable for scenarios where some leeway in memory allocation is acceptable. On the other hand, `runGC` ensures deterministic and thorough garbage collection, albeit at the expense of longer execution times, depending on various factors such as the size and complexity of the memory graph. However, in certain scenarios, `quickGC` may lead to a buildup of unreclaimed memory and might not be the most effective option.

In addition to manually choosing between `quickGC` and `runGC`, there is also a heuristic-based automatic GC. It is disabled by default, but can be enabled by calling `setAutoGC` with a non-zero value, this heuristic applies quickGC when ample free memory is available, ensuring minimal disruption. In contrast, under high memory pressure, fullGC is executed for comprehensive cleanup. This strategy balances memory efficiency with application performance, dynamically adapting to the memory usage pattern.

See more about [memory management](../general/options.md#memory-management).
